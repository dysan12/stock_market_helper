.PHONY: %
.DEFAULT_GOAL := up

up:
	@cd images; docker-compose down; docker-compose up

down:
	@cd images; docker-compose down

build:
	@cd images; docker-compose build

tests:
	@make tapi_f file="tests"

tapi:
	docker exec -it node sh -c "yarn run test_api"

tapi_f:
	docker exec -it node sh -c "yarn run jest ${file}"

tunit:
	docker exec -it node sh -c "yarn run test_unit"