import { useCallback } from 'react';
import { SupportedLang } from '../Translations';
import { useDispatch } from 'react-redux';
import { changeLanguage as changeLanguageCreator } from '../Redux/Actions';

export const useLanguageChanger = (): [(lang: SupportedLang) => void] => {
  const dispatch = useDispatch();
  const changeLanguage = useCallback(
    (lang: SupportedLang) => {
      dispatch(changeLanguageCreator(lang));
    },
    [dispatch]
  );

  return [changeLanguage];
};
