import { CHANGE_LANGUAGE } from './Actions';
import { Action } from '../../../App/Redux/Action';
import { getAvailableLanguages, getDict, SupportedLang } from '../Translations';

const initialState: LocaleState = {
  language: 'pl',
  messages: getDict('pl'), // todo get default language from local storage
  availableLanguages: getAvailableLanguages(),
};

export interface LocaleState {
  language: SupportedLang;
  messages: any;
  availableLanguages: string[];
}

export const LocaleReducer = (state = initialState, action: Action): LocaleState => {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      localStorage.setItem('language', action.payload.langauge);
      return {
        ...state,
        language: action.payload.langauge,
      };

    default:
      return state;
  }
};
