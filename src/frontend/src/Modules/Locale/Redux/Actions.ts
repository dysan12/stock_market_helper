import { Action } from '../../../App/Redux/Action';
import { SupportedLang } from '../Translations';

export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

export const changeLanguage = (lang: SupportedLang): Action<{ language: SupportedLang }> => ({
  type: CHANGE_LANGUAGE,
  payload: {
    language: lang,
  },
});
