import Polyglot from 'node-polyglot';
import { shallowEqual, useSelector } from 'react-redux';
import Polish from '../../App/Translations/Polish';
import { State } from '../../App/Redux/MainReducer';

const dicts = {
  pl: Polish,
  // en: English,
};

export declare type SupportedLang = 'pl' | 'en';

export const getAvailableLanguages = () => Object.keys(dicts);

export const getDict = (lang: SupportedLang) => dicts[lang];

export function useTranslations() {
  const { locale } = useSelector(
    (state: State) => ({
      locale: state.locale,
    }),
    shallowEqual
  );

  const polyglot = new Polyglot({ locale: locale.language });
  polyglot.extend(locale.messages);

  return polyglot.t.bind(polyglot);
}

export function useDate() {
  const lang = useSelector((state: State) => state.locale.language);

  const options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  };
  let formater = new Intl.DateTimeFormat('pl-PL', options);
  if (lang === 'en') {
    formater = new Intl.DateTimeFormat('en-US', options);
  }
  return (value: number): string => {
    return formater.format(value);
  };
}
