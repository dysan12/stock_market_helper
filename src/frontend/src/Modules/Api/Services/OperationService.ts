import { isError } from '../Error';

export interface WithOperation {
  operationId: number;
}

export interface Operation {
  id: number;
  inProgress: boolean;
  result?: {
    isSuccessful: boolean;
    response: { [property: string]: string };
  };
}

class Service {
  private id = 1;

  public createNew(id: number): Operation {
    return {
      id,
      inProgress: true,
      result: undefined,
    };
  }

  public getNextId(): number {
    return ++this.id;
  }

  public finishSucceedOperation(operations: Operation[], operationId: number, response: any): Operation[] {
    return operations.map((operation: Operation) =>
      operation.id === operationId
        ? {
            ...operation,
            inProgress: false,
            result: { isSuccessful: true, response },
          }
        : operation
    );
  }

  public finishFailedOperation(operations: Operation[], operationId: number, response: any): Operation[] {
    return operations.map((operation: Operation) =>
      operation.id === operationId
        ? {
            ...operation,
            inProgress: false,
            result: { isSuccessful: false, response },
          }
        : operation
    );
  }

  public hasCode(operation: Operation, code: string): boolean {
    const { result } = operation;
    if (result) {
      if (result.isSuccessful === true) {
        return false;
      }
      if (Array.isArray(result.response)) {
        for (const error of result.response) {
          if (isError(error) && error.code === code) {
            return true;
          }
        }
      }
    }
    return false;
  }
}

export const OperationService = new Service();
