import { State } from '../../../App/Redux/MainReducer';
import { Operation, OperationService, WithOperation } from '../Services/OperationService';
import { useCallback, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { OperationAction } from '../Redux/Action';

export type OperationSelector = (operationId: number | undefined) => (state: State) => Operation | undefined;

type callbackFn = (operation: Operation) => void;
export interface PromiseHookHandler {
  then: (
    resolveFn: callbackFn
  ) => {
    catch: (rejectFn: callbackFn) => { finally: (finallyFn: callbackFn) => void };
  };
}

// todo separate somehow promise-like logic out of the hook
export const useOperation = <Data>(
  actionCreator: (payload: Data & WithOperation) => OperationAction,
  selector: OperationSelector
): [Operation | undefined, (data: Data) => PromiseHookHandler] => {
  const [operationId, setOperationId] = useState<number>();

  const thenCallback = useRef<callbackFn | undefined>(undefined);
  const catchCallback = useRef<callbackFn | undefined>(undefined);
  const finallyCallback = useRef<callbackFn | undefined>(undefined);
  const getPromiseLikeBuilder: () => PromiseHookHandler = useCallback(
    () => ({
      then: (resolve: callbackFn) => {
        thenCallback.current = resolve;
        return {
          catch: (reject: callbackFn) => {
            catchCallback.current = reject;
            return {
              finally: (finallyFn: callbackFn) => {
                finallyCallback.current = finallyFn;
              },
            };
          },
        };
      },
    }),
    []
  );
  const removeCallbacks = useCallback(() => {
    thenCallback.current = undefined;
    catchCallback.current = undefined;
    finallyCallback.current = undefined;
  }, []);

  const dispatch = useDispatch();
  const callback = useCallback(
    (data: Data) => {
      const id = OperationService.getNextId();
      dispatch(actionCreator({ ...data, operationId: id }));
      setOperationId(id);
      return getPromiseLikeBuilder();
    },
    [dispatch, getPromiseLikeBuilder, actionCreator]
  );

  const memoizedSelector = useCallback(selector, [operationId]);
  const operation = useSelector(memoizedSelector(operationId));

  if (operation && operationId === operation.id) {
    if (operation.inProgress === false) {
      const { result } = operation;
      if (result) {
        if (thenCallback.current !== undefined && result.isSuccessful === true) {
          thenCallback.current(operation);
        } else if (catchCallback.current !== undefined) {
          catchCallback.current(operation);
        }
        if (finallyCallback.current !== undefined) {
          finallyCallback.current(operation);
        }
        removeCallbacks();
      }
    }
  }

  return [operation, callback];
};
