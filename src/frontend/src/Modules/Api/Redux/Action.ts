import { Action } from '../../../App/Redux/Action';
import { WithOperation } from '../Services/OperationService';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface OperationAction<Payload = any, T = string> extends Action<Payload & WithOperation, T> {}
