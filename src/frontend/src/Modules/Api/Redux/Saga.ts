import { FailedResult } from '../Api';

export function* handleNetworkError(result: FailedResult) {
  console.log('handleNetworkError', result);
  yield 1; // todo redirect to the network error page
}
