export const ErrorCodes = {
  USER_OLD_PASSWORD_NOT_MATCH: '001',
  USER_LOGIN_OCCUPIED: '002',
  USER_NOT_FOUND: '003',
  USER_PASSWORD_NOT_MATCH: '004',
  INDEX_COLLECTION_FIND_ERROR: '021',
  ARCHIVE_COLLECTION_FIND_ERROR: '041',
  ARCHIVE_COLLECTION_LENGTH_ERROR: '042',
  INDICATOR_NULL_VALUE_ERROR: '061',
  INDICATOR_OPERATION_ERROR: '062',
  CREATE_OPERATION_VALIDATION_FAILURE: '080',
  INVESTMENT_NOT_FOUND: '081',
  OPERATION_VIOLATES_STATE: '082',
  UNKNOWN: '666',

  // validators mapper
  validator: {
    length: '101',
    presence: '102',
  },
};

export type BadRequestResponse = Error[];

interface Error {
  field?: string;
  message: string;
  code: string;
}

export function isError(obj: any): obj is Error {
  return obj.message && obj.code;
}

export interface ApiFailure {
  status: number;
  errors: BadRequestResponse;
}
