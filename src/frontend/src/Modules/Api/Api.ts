import { BadRequestResponse } from './Error';
import { API_DOMAIN } from '../../App/Config';
import { Cookies } from '../../App/Services/Cookies/Cookies';

export const NETWORK_ERROR_STATUS = 0;

export interface Result<T = any> {
  status: number;
  body: T;
}
export interface FailedResult {
  status: number;
  body: BadRequestResponse;
}

export interface ApiResponse<T = any> {
  data: T;
  status: number;
}

export async function callApi<T>(path: string, settings?: RequestInit): Promise<Result<T>> {
  const token = Cookies.get('token');
  return new Promise(async (resolve, reject) => {
    try {
      const response = await fetch(`http://${API_DOMAIN}${path}`, {
        ...{
          headers: {
            'Content-type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        },
        ...settings,
      });
      if (response.status === 204) {
        resolve({
          body: {} as T,
          status: response.status,
        });
      } else {
        const body = await response.json();
        if (response.ok) {
          resolve({
            body,
            status: response.status,
          });
        } else {
          reject({
            body,
            status: response.status,
          });
        }
      }
    } catch (e) {
      reject({
        body: [],
        status: NETWORK_ERROR_STATUS,
      });
    }
  });
}
