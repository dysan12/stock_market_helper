import { BadRequestResponse } from '../Api/Error';

export interface Resource<T> {
  response: ErrorResource | SuccessResource<T> | undefined;
  isFetching: boolean;
}

interface ErrorResource {
  status: number;
  errors: BadRequestResponse;
}

interface SuccessResource<T> {
  data: T;
  status: number;
  errors: false;
}

export function createResource<T>(): Resource<T> {
  return { isFetching: false, response: undefined };
}
