import { ApiOperationRoutine, ApiRoutine } from '../../App/Redux/Routine';
import { callApi, FailedResult, NETWORK_ERROR_STATUS, Result } from '../Api/Api';
import { call, put } from 'redux-saga/effects';
import { handleNetworkError } from '../Api/Redux/Saga';

interface FetchParams<Routine = ApiRoutine> {
  routine: Routine;
  callPath: string;
  callOptions?: RequestInit;
}

export function* fetchResource({ routine, callPath, callOptions }: FetchParams) {
  try {
    const response: Result = yield call(callApi, callPath, callOptions);
    yield put(routine.success({ status: response.status, data: response.body }));
  } catch (e) {
    const result: FailedResult = e;
    if (result.status !== NETWORK_ERROR_STATUS) {
      yield put(routine.failure({ errors: result.body, status: result.status }));
    }
    yield handleNetworkError(result);
  }
  yield put(routine.fulfill({}));
}
// separate method for a different error handling
export function* mutateResource(
  operationId: number,
  { routine, callPath, callOptions }: FetchParams<ApiOperationRoutine>
) {
  let isSuccessful;
  try {
    const response: Result = yield call(callApi, callPath, callOptions);
    yield put(routine.success({ status: response.status, data: response.body, operationId }));
    isSuccessful = true;
  } catch (e) {
    isSuccessful = false;
    const result: FailedResult = e;
    if (result.status !== NETWORK_ERROR_STATUS) {
      yield put(routine.failure({ errors: result.body, status: result.status, operationId }));
    }
    yield handleNetworkError(result);
  }
  yield put(routine.fulfill({ operationId }));

  return isSuccessful;
}
