import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../App/Redux/MainReducer';
import { useEffect } from 'react';
import { Routine } from '../../../App/Redux/Routine';

export function useResource<Res, Args>(
  selector: (state: State) => Res,
  routine: Routine<Args, any, any, any, any, any, any, any, any, any>,
  routineArgs: Args
) {
  const dispatch = useDispatch();
  const resource = useSelector(selector);
  useEffect(() => {
    dispatch(routine.trigger(routineArgs));
  }, [...Object.values(routineArgs)]);

  return resource;
}
