import { Action } from '../../../App/Redux/Action';
import { ApiOperationRoutine, ApiRoutine } from '../../../App/Redux/Routine';
import { Operation, OperationService } from '../../Api/Services/OperationService';

type Reducer<State> = (state: State, action: Action) => State;
export function CombinedReducer<State>(initialState: State, reducers: Reducer<State>[]): Reducer<State> {
  return (state: State = initialState, action: Action) => {
    return reducers.reduce((reducedState, reducer) => {
      return reducer(reducedState, action);
    }, state);
  };
}

export function ResourceReducer<State>(routine: ApiRoutine, stateKey: keyof State): Reducer<State> {
  return (state: State, action: Action) => {
    switch (action.type) {
      case routine.TRIGGER: {
        return { ...state, [stateKey]: { ...state[stateKey], isFetching: true } };
      }
      case routine.SUCCESS: {
        const { payload } = action;
        return { ...state, [stateKey]: { ...state[stateKey], response: { ...payload, errors: false } } };
      }
      case routine.FAILURE: {
        const { payload } = action;
        return { ...state, [stateKey]: { ...state[stateKey], response: { ...payload } } };
      }
      case routine.FULFILL: {
        return { ...state, [stateKey]: { ...state[stateKey], isFetching: false } };
      }
    }
    return state;
  };
}

export function OperationReducer<State extends { operations: Operation[] }>(
  routine: ApiOperationRoutine
): Reducer<State> {
  return (state: State, action: Action) => {
    switch (action.type) {
      case routine.TRIGGER: {
        const { payload } = action;
        return { ...state, operations: [...state.operations, OperationService.createNew(payload.operationId)] };
      }
      case routine.SUCCESS: {
        const { payload } = action;
        return {
          ...state,
          operations: OperationService.finishSucceedOperation(state.operations, payload.operationId, payload),
        };
      }
      case routine.FAILURE: {
        const { payload } = action;
        return {
          ...state,
          operations: OperationService.finishFailedOperation(state.operations, payload.operationId, payload.errors),
        };
      }
    }
    return state;
  };
}
