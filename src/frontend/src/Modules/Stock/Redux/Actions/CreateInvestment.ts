import { createOperationRoutine } from '../../../../App/Redux/Routine';
import { CreateInvestmentRequest, CreateInvestmentResponse } from '../../Api/Contract';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';
import { OperationAction } from '../../../Api/Redux/Action';

export namespace CREATE_INVESTMENT {
  export const name = 'CREATE_INVESTMENT';
  export const trigger = 'CREATE_INVESTMENT/trigger';
  export const failure = 'CREATE_INVESTMENT/failure';
  export const success = 'CREATE_INVESTMENT/success';
  export const fulfill = 'CREATE_INVESTMENT/fulfill';
}

export const CreateInvestment = createOperationRoutine<
  CreateInvestmentRequest,
  typeof CREATE_INVESTMENT.trigger,
  any,
  any,
  ApiResponse<CreateInvestmentResponse>,
  typeof CREATE_INVESTMENT.success,
  ApiFailure,
  typeof CREATE_INVESTMENT.failure,
  any,
  typeof CREATE_INVESTMENT.fulfill
>(CREATE_INVESTMENT.name);

export type CreateInvestmentAction =
  | OperationAction<CreateInvestmentRequest, typeof CREATE_INVESTMENT.trigger>
  | OperationAction<ApiResponse<CreateInvestmentResponse>, typeof CREATE_INVESTMENT.success>
  | OperationAction<ApiFailure, typeof CREATE_INVESTMENT.failure>
  | OperationAction<any, typeof CREATE_INVESTMENT.fulfill>;
