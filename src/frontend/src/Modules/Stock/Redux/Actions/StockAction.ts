import { FetchIndexAction } from './FetchIndex';
import { FetchIndexesAction } from './FetchIndexes';
import { FetchCompanyAction } from './FetchCompany';
import { FetchCompanyIndicatorsAction } from './FetchCompanyIndicators';
import { FetchWalletAction } from './FetchWallet';
import { CreateInvestmentAction } from './CreateInvestment';
import { DeleteInvestmentOperationAction } from './DeleteInvestmentOperation';
import { AddInvestmentOperationAction } from './AddInvestmentOperation';
import { ViewAction } from './View';

export type StockAction =
  | FetchIndexAction
  | FetchIndexesAction
  | FetchCompanyAction
  | FetchCompanyIndicatorsAction
  | FetchWalletAction
  | DeleteInvestmentOperationAction
  | AddInvestmentOperationAction
  | CreateInvestmentAction
  | ViewAction;
