import { createOperationRoutine } from '../../../../App/Redux/Routine';
import { AddInvestmentOperationRequest, AddInvestmentOperationResponse } from '../../Api/Contract';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';
import { OperationAction } from '../../../Api/Redux/Action';

export namespace ADD_INVESTMENT_OPERATION {
  export const name = 'ADD_INVESTMENT_OPERATION';
  export const trigger = 'ADD_INVESTMENT_OPERATION/trigger';
  export const failure = 'ADD_INVESTMENT_OPERATION/failure';
  export const success = 'ADD_INVESTMENT_OPERATION/success';
  export const fulfill = 'ADD_INVESTMENT_OPERATION/fulfill';
}

export const AddInvestmentOperation = createOperationRoutine<
  AddInvestmentOperationRequest,
  typeof ADD_INVESTMENT_OPERATION.trigger,
  any,
  any,
  ApiResponse<AddInvestmentOperationResponse>,
  typeof ADD_INVESTMENT_OPERATION.success,
  ApiFailure,
  typeof ADD_INVESTMENT_OPERATION.failure,
  any,
  typeof ADD_INVESTMENT_OPERATION.fulfill
>(ADD_INVESTMENT_OPERATION.name);

export type AddInvestmentOperationAction =
  | OperationAction<AddInvestmentOperationRequest, typeof ADD_INVESTMENT_OPERATION.trigger>
  | OperationAction<ApiResponse<AddInvestmentOperationResponse>, typeof ADD_INVESTMENT_OPERATION.success>
  | OperationAction<ApiFailure, typeof ADD_INVESTMENT_OPERATION.failure>
  | OperationAction<any, typeof ADD_INVESTMENT_OPERATION.fulfill>;
