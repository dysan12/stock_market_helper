import { createOperationRoutine } from '../../../../App/Redux/Routine';
import { DeleteInvestmentOperationRequest, DeleteInvestmentOperationResponse } from '../../Api/Contract';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';
import { OperationAction } from '../../../Api/Redux/Action';

export namespace DELETE_INVESTMENT_OPERATION {
  export const name = 'DELETE_INVESTMENT_OPERATION';
  export const trigger = 'DELETE_INVESTMENT_OPERATION/trigger';
  export const failure = 'DELETE_INVESTMENT_OPERATION/failure';
  export const success = 'DELETE_INVESTMENT_OPERATION/success';
  export const fulfill = 'DELETE_INVESTMENT_OPERATION/fulfill';
}

export const DeleteInvestmentOperation = createOperationRoutine<
  DeleteInvestmentOperationRequest,
  typeof DELETE_INVESTMENT_OPERATION.trigger,
  any,
  any,
  ApiResponse<DeleteInvestmentOperationResponse>,
  typeof DELETE_INVESTMENT_OPERATION.success,
  ApiFailure,
  typeof DELETE_INVESTMENT_OPERATION.failure,
  any,
  typeof DELETE_INVESTMENT_OPERATION.fulfill
>(DELETE_INVESTMENT_OPERATION.name);

export type DeleteInvestmentOperationAction =
  | OperationAction<DeleteInvestmentOperationRequest, typeof DELETE_INVESTMENT_OPERATION.trigger>
  | OperationAction<ApiResponse<DeleteInvestmentOperationResponse>, typeof DELETE_INVESTMENT_OPERATION.success>
  | OperationAction<ApiFailure, typeof DELETE_INVESTMENT_OPERATION.failure>
  | OperationAction<any, typeof DELETE_INVESTMENT_OPERATION.fulfill>;
