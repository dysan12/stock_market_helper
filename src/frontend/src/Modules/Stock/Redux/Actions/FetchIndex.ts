import { createRoutine } from '../../../../App/Redux/Routine';
import { IndexRequest, IndexResponse } from '../../Api/Contract';
import { Action } from '../../../../App/Redux/Action';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';

export namespace FETCH_INDEX {
  export const name = 'FETCH_INDEX';
  export const trigger = 'FETCH_INDEX/trigger';
  export const failure = 'FETCH_INDEX/failure';
  export const success = 'FETCH_INDEX/success';
  export const fulfill = 'FETCH_INDEX/fulfill';
}

export const FetchIndex = createRoutine<
  IndexRequest,
  typeof FETCH_INDEX.trigger,
  any,
  any,
  ApiResponse<IndexResponse>,
  typeof FETCH_INDEX.success,
  ApiFailure,
  typeof FETCH_INDEX.failure,
  any,
  typeof FETCH_INDEX.fulfill
>(FETCH_INDEX.name);

export type FetchIndexAction =
  | Action<IndexRequest, typeof FETCH_INDEX.trigger>
  | Action<ApiResponse<IndexResponse>, typeof FETCH_INDEX.success>
  | Action<ApiFailure, typeof FETCH_INDEX.failure>
  | Action<any, typeof FETCH_INDEX.fulfill>;
