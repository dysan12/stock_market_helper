import { createRoutine } from '../../../../App/Redux/Routine';
import { CompanyRequest, CompanyResponse } from '../../Api/Contract';
import { Action } from '../../../../App/Redux/Action';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';

export namespace FETCH_COMPANY {
  export const name = 'FETCH_COMPANY';
  export const trigger = 'FETCH_COMPANY/trigger';
  export const failure = 'FETCH_COMPANY/failure';
  export const success = 'FETCH_COMPANY/success';
  export const fulfill = 'FETCH_COMPANY/fulfill';
}

export const FetchCompany = createRoutine<
  CompanyRequest,
  typeof FETCH_COMPANY.trigger,
  any,
  any,
  ApiResponse<CompanyResponse>,
  typeof FETCH_COMPANY.success,
  ApiFailure,
  typeof FETCH_COMPANY.failure,
  any,
  typeof FETCH_COMPANY.fulfill
>(FETCH_COMPANY.name);

export type FetchCompanyAction =
  | Action<CompanyRequest, typeof FETCH_COMPANY.trigger>
  | Action<ApiResponse<CompanyResponse>, typeof FETCH_COMPANY.success>
  | Action<ApiFailure, typeof FETCH_COMPANY.failure>
  | Action<any, typeof FETCH_COMPANY.fulfill>;
