import { StockAction } from './StockAction';
import { Action } from '../../../../App/Redux/Action';

export const EXPAND_INVESTMENT_PANEL = 'EXPAND_INVESTMENT_PANEL';

export function expandInvestmentPanel(investmentId: string): StockAction {
  return {
    type: EXPAND_INVESTMENT_PANEL,
    payload: {
      investmentId,
    },
  };
}

export type ViewAction = Action<{ investmentId: string }, typeof EXPAND_INVESTMENT_PANEL>;
