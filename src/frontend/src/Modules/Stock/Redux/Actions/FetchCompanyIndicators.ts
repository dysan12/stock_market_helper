import { createRoutine } from '../../../../App/Redux/Routine';
import { CompanyIndicatorsRequest, CompanyIndicatorsResponse } from '../../Api/Contract';
import { Action } from '../../../../App/Redux/Action';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';

export namespace FETCH_COMPANY_INDICATORS {
  export const name = 'FETCH_COMPANY_INDICATORS';
  export const trigger = 'FETCH_COMPANY_INDICATORS/trigger';
  export const failure = 'FETCH_COMPANY_INDICATORS/failure';
  export const success = 'FETCH_COMPANY_INDICATORS/success';
  export const fulfill = 'FETCH_COMPANY_INDICATORS/fulfill';
}

export const FetchCompanyIndicators = createRoutine<
  CompanyIndicatorsRequest,
  typeof FETCH_COMPANY_INDICATORS.trigger,
  any,
  any,
  ApiResponse<CompanyIndicatorsResponse>,
  typeof FETCH_COMPANY_INDICATORS.success,
  ApiFailure,
  typeof FETCH_COMPANY_INDICATORS.failure,
  any,
  typeof FETCH_COMPANY_INDICATORS.fulfill
>(FETCH_COMPANY_INDICATORS.name);

export type FetchCompanyIndicatorsAction =
  | Action<CompanyIndicatorsRequest, typeof FETCH_COMPANY_INDICATORS.trigger>
  | Action<ApiResponse<CompanyIndicatorsResponse>, typeof FETCH_COMPANY_INDICATORS.success>
  | Action<ApiFailure, typeof FETCH_COMPANY_INDICATORS.failure>
  | Action<any, typeof FETCH_COMPANY_INDICATORS.fulfill>;
