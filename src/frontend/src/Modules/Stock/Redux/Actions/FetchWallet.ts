import { createRoutine } from '../../../../App/Redux/Routine';
import { WalletRequest, WalletResponse } from '../../Api/Contract';
import { Action } from '../../../../App/Redux/Action';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';

export namespace FETCH_WALLET {
  export const name = 'FETCH_WALLET';
  export const trigger = 'FETCH_WALLET/trigger';
  export const failure = 'FETCH_WALLET/failure';
  export const success = 'FETCH_WALLET/success';
  export const fulfill = 'FETCH_WALLET/fulfill';
}

export const FetchWallet = createRoutine<
  WalletRequest,
  typeof FETCH_WALLET.trigger,
  any,
  any,
  ApiResponse<WalletResponse>,
  typeof FETCH_WALLET.success,
  ApiFailure,
  typeof FETCH_WALLET.failure,
  any,
  typeof FETCH_WALLET.fulfill
>(FETCH_WALLET.name);

export type FetchWalletAction =
  | Action<WalletRequest, typeof FETCH_WALLET.trigger>
  | Action<ApiResponse<WalletResponse>, typeof FETCH_WALLET.success>
  | Action<ApiFailure, typeof FETCH_WALLET.failure>
  | Action<any, typeof FETCH_WALLET.fulfill>;
