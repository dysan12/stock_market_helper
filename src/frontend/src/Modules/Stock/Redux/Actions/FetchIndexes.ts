import { createRoutine } from '../../../../App/Redux/Routine';
import { IndexesRequest, IndexesResponse } from '../../Api/Contract';
import { Action } from '../../../../App/Redux/Action';
import { ApiResponse } from '../../../Api/Api';
import { ApiFailure } from '../../../Api/Error';

export namespace FETCH_INDEXES {
  export const name = 'FETCH_INDEXES';
  export const trigger = 'FETCH_INDEXES/trigger';
  export const failure = 'FETCH_INDEXES/failure';
  export const success = 'FETCH_INDEXES/success';
  export const fulfill = 'FETCH_INDEXES/fulfill';
}

export const FetchIndexes = createRoutine<
  IndexesRequest,
  typeof FETCH_INDEXES.trigger,
  any,
  any,
  ApiResponse<IndexesResponse>,
  typeof FETCH_INDEXES.success,
  ApiFailure,
  typeof FETCH_INDEXES.failure,
  any,
  typeof FETCH_INDEXES.fulfill
>(FETCH_INDEXES.name);

export type FetchIndexesAction =
  | Action<any, typeof FETCH_INDEXES.trigger>
  | Action<ApiResponse<IndexesResponse>, typeof FETCH_INDEXES.success>
  | Action<ApiFailure, typeof FETCH_INDEXES.failure>
  | Action<any, typeof FETCH_INDEXES.fulfill>;
