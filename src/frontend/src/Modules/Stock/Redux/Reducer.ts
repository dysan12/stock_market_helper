import { FetchIndexes } from './Actions/FetchIndexes';
import { createResource, Resource } from '../../Resource/Resource';
import {
  CompanyIndicatorsResponse,
  CompanyResponse,
  IndexesResponse,
  IndexResponse,
  WalletResponse,
} from '../Api/Contract';
import { CombinedReducer, OperationReducer, ResourceReducer } from '../../Resource/Redux/Reducer';
import { FetchIndex } from './Actions/FetchIndex';
import { FetchCompany } from './Actions/FetchCompany';
import { FetchCompanyIndicators } from './Actions/FetchCompanyIndicators';
import { FetchWallet } from './Actions/FetchWallet';
import { Operation } from '../../Api/Services/OperationService';
import { CreateInvestment } from './Actions/CreateInvestment';
import { AddInvestmentOperation } from './Actions/AddInvestmentOperation';
import { DeleteInvestmentOperation } from './Actions/DeleteInvestmentOperation';
import { StockAction } from './Actions/StockAction';
import { EXPAND_INVESTMENT_PANEL } from './Actions/View';

const initialState: StockState = {
  indexes: createResource(),
  index: createResource(),
  company: createResource(),
  companyIndicators: createResource(),
  wallet: createResource(),
  operations: [],
  view: {
    walletInvestments: {
      expanded: undefined,
    },
  },
};

export interface StockState {
  indexes: Resource<IndexesResponse>;
  index: Resource<IndexResponse>;
  company: Resource<CompanyResponse>;
  companyIndicators: Resource<CompanyIndicatorsResponse>;
  wallet: Resource<WalletResponse>;
  operations: Operation[];
  view: {
    walletInvestments: {
      expanded: string | undefined;
    };
  };
}

export const StockReducer = CombinedReducer<StockState>(initialState, [
  ResourceReducer<StockState>(FetchIndexes, 'indexes'),
  ResourceReducer<StockState>(FetchIndex, 'index'),
  ResourceReducer<StockState>(FetchCompany, 'company'),
  ResourceReducer<StockState>(FetchCompanyIndicators, 'companyIndicators'),
  ResourceReducer<StockState>(FetchWallet, 'wallet'),
  OperationReducer<StockState>(CreateInvestment),
  OperationReducer<StockState>(AddInvestmentOperation),
  OperationReducer<StockState>(DeleteInvestmentOperation),
  WalletInvestmentsReducer,
]);

function WalletInvestmentsReducer(state: StockState, action: StockAction): StockState {
  if (action.type === EXPAND_INVESTMENT_PANEL) {
    const { payload } = action;
    return {
      ...state,
      view: {
        ...state.view,
        walletInvestments: {
          expanded: payload.investmentId,
        },
      },
    };
  }
  return state;
}
