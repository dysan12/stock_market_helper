import { all, put, takeLatest } from '@redux-saga/core/effects';
import { FETCH_INDEXES, FetchIndexes } from './Actions/FetchIndexes';
import { FETCH_INDEX, FetchIndex } from './Actions/FetchIndex';
import { fetchResource, mutateResource } from '../../Resource/Fetch';
import { StockAction } from './Actions/StockAction';
import { FETCH_COMPANY, FetchCompany } from './Actions/FetchCompany';
import { FETCH_COMPANY_INDICATORS, FetchCompanyIndicators } from './Actions/FetchCompanyIndicators';
import { FETCH_WALLET, FetchWallet } from './Actions/FetchWallet';
import { CREATE_INVESTMENT, CreateInvestment } from './Actions/CreateInvestment';
import { ADD_INVESTMENT_OPERATION, AddInvestmentOperation } from './Actions/AddInvestmentOperation';
import { DELETE_INVESTMENT_OPERATION, DeleteInvestmentOperation } from './Actions/DeleteInvestmentOperation';

function* fetchIndexes(action: StockAction) {
  if (action.type === FETCH_INDEXES.trigger) {
    yield fetchResource({ routine: FetchIndexes, callPath: '/indexes' });
  }
}

function* fetchIndex(action: StockAction) {
  if (action.type === FETCH_INDEX.trigger) {
    const path = `/indexes/${action.payload.id}`;
    yield fetchResource({ routine: FetchIndex, callPath: path });
  }
}

function* fetchCompany(action: StockAction) {
  if (action.type === FETCH_COMPANY.trigger) {
    const path = `/companies/${action.payload.companyId}`;
    yield fetchResource({ routine: FetchCompany, callPath: path });
  }
}

function* fetchCompanyIndicators(action: StockAction) {
  if (action.type === FETCH_COMPANY_INDICATORS.trigger) {
    const path = `/companies/${action.payload.companyId}/indicators`;
    yield fetchResource({ routine: FetchCompanyIndicators, callPath: path });
  }
}

function* fetchWallet(action: StockAction) {
  if (action.type === FETCH_WALLET.trigger) {
    const path = `/wallets/${action.payload.companyId}`;
    yield fetchResource({ routine: FetchWallet, callPath: path });
  }
}

function* createInvestment(action: StockAction) {
  if (action.type === CREATE_INVESTMENT.trigger) {
    const { payload } = action;
    const path = `/wallets/${payload.companyId}/investments`;
    yield mutateResource(payload.operationId, {
      routine: CreateInvestment,
      callPath: path,
      callOptions: { method: 'POST', body: JSON.stringify(payload.body) },
    });
    yield put(FetchWallet.trigger({ companyId: payload.companyId }));
  }
}

function* addOperation(action: StockAction) {
  if (action.type === ADD_INVESTMENT_OPERATION.trigger) {
    const { payload } = action;
    const path = `/investments/${payload.investmentId}/operations`;
    const isSuccessful = yield mutateResource(payload.operationId, {
      routine: AddInvestmentOperation,
      callPath: path,
      callOptions: { method: 'POST', body: JSON.stringify(payload.body) },
    });
    if (isSuccessful === true) {
      yield put(FetchWallet.trigger({ companyId: payload.companyId }));
    }
  }
}

function* deleteOperation(action: StockAction) {
  if (action.type === DELETE_INVESTMENT_OPERATION.trigger) {
    const { payload } = action;
    const path = `/investments/${payload.investmentId}/operations/${payload.investmentOperationId}`;
    const isSuccessful = yield mutateResource(payload.operationId, {
      routine: DeleteInvestmentOperation,
      callPath: path,
      callOptions: { method: 'DELETE' },
    });

    if (isSuccessful === true) {
      yield put(FetchWallet.trigger({ companyId: payload.companyId }));
    }
  }
}

export function* stockSagas() {
  yield all([
    takeLatest(FETCH_INDEXES.trigger, fetchIndexes),
    takeLatest(FETCH_INDEX.trigger, fetchIndex),
    takeLatest(FETCH_COMPANY.trigger, fetchCompany),
    takeLatest(FETCH_COMPANY_INDICATORS.trigger, fetchCompanyIndicators),
    takeLatest(FETCH_WALLET.trigger, fetchWallet),
    takeLatest(CREATE_INVESTMENT.trigger, createInvestment),
    takeLatest(ADD_INVESTMENT_OPERATION.trigger, addOperation),
    takeLatest(DELETE_INVESTMENT_OPERATION.trigger, deleteOperation),
  ]);
}
