import { FetchIndexes } from '../Redux/Actions/FetchIndexes';
import { State } from '../../../App/Redux/MainReducer';
import { FetchIndex } from '../Redux/Actions/FetchIndex';
import { useResource } from '../../Resource/Hooks/Resource';

export function useIndexes() {
  return useResource((state: State) => state.stock.indexes, FetchIndexes, {});
}

export function useIndex(id: string) {
  return useResource((state: State) => state.stock.index, FetchIndex, { id });
}
