import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../../App/Redux/MainReducer';
import { expandInvestmentPanel } from '../Redux/Actions/View';

export function useInvestmentExpand() {
  const dispatch = useDispatch();
  const expanded = useSelector((state: State) => state.stock.view.walletInvestments.expanded);

  return {
    expanded,
    changeExpanded: (expanded: string) => dispatch(expandInvestmentPanel(expanded)),
  };
}
