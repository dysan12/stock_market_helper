import { State } from '../../../App/Redux/MainReducer';
import { useResource } from '../../Resource/Hooks/Resource';
import { FetchCompany } from '../Redux/Actions/FetchCompany';
import { FetchCompanyIndicators } from '../Redux/Actions/FetchCompanyIndicators';

export function useCompany(companyId: string) {
  return useResource((state: State) => state.stock.company, FetchCompany, { companyId });
}

export function useCompanyIndicators(companyId: string) {
  return useResource((state: State) => state.stock.companyIndicators, FetchCompanyIndicators, { companyId });
}
