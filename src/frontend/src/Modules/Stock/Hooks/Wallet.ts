import { State } from '../../../App/Redux/MainReducer';
import { useResource } from '../../Resource/Hooks/Resource';
import { FetchWallet } from '../Redux/Actions/FetchWallet';
import { useOperation } from '../../Api/Hooks/Operation';
import {
  AddInvestmentOperationRequest,
  CreateInvestmentRequest,
  DeleteInvestmentOperationRequest,
} from '../Api/Contract';
import { CreateInvestment } from '../Redux/Actions/CreateInvestment';
import { AddInvestmentOperation } from '../Redux/Actions/AddInvestmentOperation';
import { DeleteInvestmentOperation } from '../Redux/Actions/DeleteInvestmentOperation';

export function useWallet(companyId: string) {
  return useResource((state: State) => state.stock.wallet, FetchWallet, { companyId });
}

export const useCreateInvestment = () => {
  const [operation, handler] = useOperation<CreateInvestmentRequest>(
    payload => CreateInvestment.trigger(payload),
    (operationId: number | undefined) => (state: State) => state.stock.operations.find(o => o.id === operationId)
  );
  return {
    operation,
    createInvestment: handler,
  };
};

export const useAddOperation = () => {
  const [operation, handler] = useOperation<AddInvestmentOperationRequest>(
    payload => AddInvestmentOperation.trigger(payload),
    (operationId: number | undefined) => (state: State) => state.stock.operations.find(o => o.id === operationId)
  );
  return {
    operation,
    addOperation: handler,
  };
};

export const useDeleteOperation = () => {
  const [operation, handler] = useOperation<DeleteInvestmentOperationRequest>(
    payload => DeleteInvestmentOperation.trigger(payload),
    (operationId: number | undefined) => (state: State) => state.stock.operations.find(o => o.id === operationId)
  );
  return {
    operation,
    deleteOperation: handler,
  };
};
