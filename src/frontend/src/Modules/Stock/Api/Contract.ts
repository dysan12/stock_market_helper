export interface IndexesRequest {}
export interface IndexesResponse {
  data: Index[];
}

export interface IndexRequest {
  id: string;
}
export interface IndexResponse extends Index {}

interface Index {
  id: string;
  name: string;
  uid: string;
  rate: number;
  rateChange: number;
  openingRate: number;
  maxRate: number;
  minRate: number;
  closingRate: number;
  timestamp: number;
  companies: IndexCompany[];
}

export interface IndexCompany {
  name: string;
  uid: string;
  packet: number;
  packetPln: number;
  walletSharePercentage: number;
  sessionStockExchangeSharePercentage: number;
  averageSessionSpread: number;
}

export interface CompanyRequest {
  companyId: string;
}
export interface CompanyResponse extends Company {}
export interface CompanyIndicatorsRequest {
  companyId: string;
}
export interface CompanyIndicatorsResponse extends CompanyIndicators {}

export interface Company {
  name: string;
  uid: string;
  rateChange: number;
  buyOffer: number;
  sellOffer: number;
  minRate: number;
  maxRate: number;
  exchangeVolume: number;
  exchangeValue: number;
  timestamp: number;
}

export interface CompanyIndicators {
  rsi: number | null;
  slowSts: number | null;
  fastSts: number | null;
  macd: number | null;
  trix: number | null;
  sma5: number | null;
  sma15: number | null;
  sma30: number | null;
  sma60: number | null;
  ema5: number | null;
  ema15: number | null;
  ema30: number | null;
  ema60: number | null;
}

export interface WalletRequest {
  companyId: string;
}

export interface WalletResponse extends Wallet {}

export interface Wallet {
  companyId: string;
  investments: Investment[];
}

export interface Investment {
  id: string;
  name: string;
  totalValue: number;
  totalUnits: number;
  profitableFrom: number;
  operations: Array<{
    id: string;
    type: string;
    unitPrice: number;
    quantity: number;
    madeAt: number;
  }>;
}

export interface CreateInvestmentRequest {
  companyId: string;
  body: {
    name: string;
  };
}

export interface CreateInvestmentResponse {
  id: string;
}

export interface AddInvestmentOperationRequest {
  companyId: string;
  investmentId: string;
  body: {
    type: 'buy' | 'sell';
    unitPrice: number;
    quantity: number;
  };
}

export interface AddInvestmentOperationResponse {
  id: string;
}

export interface DeleteInvestmentOperationRequest {
  companyId: string;
  investmentId: string;
  investmentOperationId: string;
}

export interface DeleteInvestmentOperationResponse {}
