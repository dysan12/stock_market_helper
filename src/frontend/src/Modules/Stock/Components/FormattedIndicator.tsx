import { useTranslations } from '../../Locale/Translations';
import { INDICATOR_THRESHOLDS } from '../../../App/Config';
import React from 'react';
import { makeStyles } from '@material-ui/core';
import clsx from 'clsx';

export function FormattedIndicator({ value, name }: { name: string; value: any }) {
  const t = useTranslations();
  const classes = useStyles({});
  const settings = INDICATOR_THRESHOLDS;
  if (value === null) {
    return <>{t('common.noData')}</>;
  }
  if (typeof settings[name] !== 'undefined') {
    const setting = settings[name];
    if (setting.buyFrom < value) {
      return (
        <span>
          <span className={clsx([classes.label, classes.buy])}>{t('stock.labels.indicators.buy')}</span>({value})
        </span>
      );
    }
    if (setting.sellTo > value) {
      return (
        <span>
          <span className={clsx([classes.label, classes.sell])}>{t('stock.labels.indicators.sell')}</span>({value})
        </span>
      );
    }
  }
  return <span>{value}</span>;
}

const useStyles = makeStyles({
  label: {
    fontWeight: 'bold',
  },
  buy: {
    color: 'green',
  },
  sell: {
    color: 'red',
  },
});
