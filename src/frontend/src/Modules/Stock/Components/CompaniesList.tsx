import React from 'react';
import { makeStyles, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import { IndexCompany } from '../Api/Contract';
import { useTranslations } from '../../Locale/Translations';
import TablePagination from '@material-ui/core/TablePagination';
import { useRouter } from 'next/router';

export function CompaniesList({ companies }: { companies: IndexCompany[] }) {
  const t = useTranslations();
  const classes = useStyles({});
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(8);
  const router = useRouter();

  const companiesToShow = companies.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);
  return (
    <>
      <Table className={classes.table}>
        <TableHead>
          <TableRow className={classes.row}>
            <TableCell align="center">{t('stock.labels.company.name')}</TableCell>
            <TableCell align="center">{t('stock.labels.company.packet')}</TableCell>
            <TableCell align="center">{t('stock.labels.company.packetPln')}</TableCell>
            <TableCell align="center">{t('stock.labels.company.walletSharePercentage')}</TableCell>
            <TableCell align="center" size="small">
              {t('stock.labels.company.sessionStockExchangeSharePercentage')}
            </TableCell>
            <TableCell align="center">{t('stock.labels.company.averageSessionSpread')}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {companiesToShow.length === 0 ? (
            <TableRow className={classes.row}>
              <TableCell colSpan={6} align="center">
                {t('stock.messages.noCompanies')}
              </TableCell>
            </TableRow>
          ) : (
            companiesToShow.map(c => {
              return (
                <TableRow
                  hover
                  key={c.uid}
                  onClick={() => router.push(`/stock/companies/[id]`, `/stock/companies/${c.uid}`)}
                  className={classes.row}
                >
                  <TableCell align="right">{c.name}</TableCell>
                  <TableCell align="right">{c.packet}</TableCell>
                  <TableCell align="right">{c.packetPln}</TableCell>
                  <TableCell align="right">{c.walletSharePercentage}</TableCell>
                  <TableCell align="right">{c.sessionStockExchangeSharePercentage}</TableCell>
                  <TableCell align="right">{c.averageSessionSpread}</TableCell>
                </TableRow>
              );
            })
          )}
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[8, 20, 30]}
        component="div"
        count={companies.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={(event: unknown, newPage: number) => {
          setPage(newPage);
        }}
        onChangeRowsPerPage={(event: React.ChangeEvent<HTMLInputElement>) => {
          setRowsPerPage(parseInt(event.target.value, 10));
          setPage(0);
        }}
      />
    </>
  );
}

const useStyles = makeStyles({
  container: {},
  row: {
    cursor: 'pointer',
    '& > td': {
      maxWidth: '180px',
    },
  },
  table: {},
});
