import { WalletResponse } from '../../Api/Contract';
import { Grid, IconButton, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { WalletSummary } from './WalletSummary';
import { WalletInvestments } from './WalletInvestments';
import { useTranslations } from '../../../Locale/Translations';
import { Add } from '@material-ui/icons';
import { CreateInvestmentModal } from './CreateInvestmentModal';

export function CompanyWallet({ wallet }: { wallet: WalletResponse }) {
  const t = useTranslations();
  const [modalState, setModalState] = useState(false);

  return (
    <section>
      <CreateInvestmentModal companyId={wallet.companyId} isOpen={modalState} close={() => setModalState(false)} />
      <Grid container>
        <Grid item xs={12}>
          <Grid item xs={1} />
          <Grid item xs={2}>
            <Typography variant={'h6'}>
              {t('stock.titles.investments')}
              <IconButton onClick={() => setModalState(true)}>
                <Add />
              </IconButton>
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={10}>
          <WalletInvestments wallet={wallet} />
        </Grid>
        <Grid item xs={1} />
        <Grid item xs={12}>
          <WalletSummary wallet={wallet} />
        </Grid>
      </Grid>
    </section>
  );
}
