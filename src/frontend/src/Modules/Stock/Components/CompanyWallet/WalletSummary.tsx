import { WalletResponse } from '../../Api/Contract';
import { useTranslations } from '../../../Locale/Translations';
import React from 'react';
import { Grid, makeStyles, Typography } from '@material-ui/core';
import { toPLN } from '../../../../App/Formats/Currencies';

export function WalletSummary({ wallet }: { wallet: WalletResponse }) {
  const t = useTranslations();
  const classes = useStyles({});
  const totalValue = wallet.investments.reduce((p, c) => c.totalValue + p, 0);
  const totalUnits = wallet.investments.reduce((p, c) => c.totalUnits + p, 0);
  return (
    <div className={classes.container}>
      <Grid item xs={1} />
      <Grid item xs={2}>
        <Typography variant={'subtitle1'}>{t('stock.titles.summary')}</Typography>
      </Grid>
      <Grid container>
        <Grid item xs={1} />
        <Grid item xs={3}>
          <Typography variant="body1">
            {t('stock.labels.wallet.investmentsNo')}: {wallet.investments.length}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="body1">
            {t('stock.labels.wallet.totalValue')}: {toPLN(totalValue / 100)}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="body1">
            {t('stock.labels.wallet.totalUnits')}: {totalUnits}
          </Typography>
        </Grid>
        <Grid item xs={1} />
      </Grid>
    </div>
  );
}

const useStyles = makeStyles({
  container: {
    paddingTop: '15px',
    paddingBottom: '15px',
  },
});
