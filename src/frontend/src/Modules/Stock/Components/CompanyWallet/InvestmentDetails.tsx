import { Investment } from '../../Api/Contract';
import { AddOperationModal } from './AddOperationModal';
import {
  Divider,
  Grid,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { Add, Delete } from '@material-ui/icons';
import React, { useState } from 'react';
import { useDate, useTranslations } from '../../../Locale/Translations';
import { toPLN } from '../../../../App/Formats/Currencies';
import { useDeleteOperation } from '../../Hooks/Wallet';
import { useNotifications } from '../../../Notification/Hooks/Notifications';
import { OperationService } from '../../../Api/Services/OperationService';
import { ErrorCodes } from '../../../Api/Error';

export function InvestmentDetails({ companyId, investment }: { companyId: string; investment: Investment }) {
  const t = useTranslations();
  const toDate = useDate();
  const { deleteOperation } = useDeleteOperation();
  const [modalState, setModalState] = useState(false);
  const { addNotification } = useNotifications();

  return (
    <>
      <AddOperationModal
        companyId={companyId}
        investmentId={investment.id}
        isOpen={modalState}
        close={() => setModalState(false)}
      />
      <Grid container>
        <Divider variant={'fullWidth'} />
        <Grid item xs={1} />
        <Grid item xs={2}>
          <Typography variant={'subtitle2'}>
            {t('stock.titles.operations')}
            <IconButton onClick={() => setModalState(true)}>
              <Add />
            </IconButton>
          </Typography>
        </Grid>
        <Grid item xs={9} />
        <Grid item xs={12}>
          {investment.operations.length === 0 ? (
            <Typography variant={'body2'}>{t('stock.messages.noOperations')}</Typography>
          ) : (
            <Table>
              <TableHead>
                <TableCell>{t('stock.labels.operation.unitPrice')}</TableCell>
                <TableCell>{t('stock.labels.operation.quantity')}</TableCell>
                <TableCell>{t('stock.labels.operation.type')}</TableCell>
                <TableCell>{t('stock.labels.operation.madeAt')}</TableCell>
                <TableCell>{t('common.actions')}</TableCell>
              </TableHead>
              <TableBody>
                {investment.operations.map(o => (
                  <TableRow>
                    <TableCell>{toPLN(o.unitPrice / 100)}</TableCell>
                    <TableCell>{o.quantity}</TableCell>
                    <TableCell>{t(`stock.labels.operation.${o.type}`)}</TableCell>
                    <TableCell>{toDate(o.madeAt * 1000)}</TableCell>
                    <TableCell>
                      <IconButton
                        onClick={() => {
                          deleteOperation({
                            investmentOperationId: o.id,
                            companyId,
                            investmentId: investment.id,
                          })
                            .then(() => {
                              addNotification({
                                type: 'success',
                                duration: 5000,
                                msg: t('stock.messages.operationDeletedSuccessfully'),
                              });
                            })
                            .catch(operation => {
                              if (OperationService.hasCode(operation, ErrorCodes.OPERATION_VIOLATES_STATE)) {
                                addNotification({
                                  type: 'error',
                                  duration: 5000,
                                  msg: t('stock.messages.deletedOperationViolatesState'),
                                });
                              }
                            });
                        }}
                      >
                        <Delete />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          )}
        </Grid>
      </Grid>
    </>
  );
}
