import { useTranslations } from '../../../Locale/Translations';
import React from 'react';
import { useNotifications } from '../../../Notification/Hooks/Notifications';
import { Button, Dialog, DialogContent, DialogTitle, Grid, MenuItem, Typography } from '@material-ui/core';
import { Field, Form } from 'react-final-form';
import { integerPositive, number, required, validators } from '../../../../App/Form/Validation/Validation';
import { Select } from '../../../../App/Form/Select';
import { useAddOperation } from '../../Hooks/Wallet';
import { TextField } from '../../../../App/Form/TextField';
import { ErrorCodes } from '../../../Api/Error';
import { FORM_ERROR } from 'final-form';
import { mapOperationError } from '../../../../App/Form/ErrorsMapper';

export function AddOperationModal({
  companyId,
  investmentId,
  isOpen,
  close,
}: {
  companyId: string;
  investmentId: string;
  isOpen: boolean;
  close: () => void;
}) {
  const t = useTranslations();
  const { addNotification } = useNotifications();
  const { addOperation } = useAddOperation();

  const submit = async values =>
    new Promise(resolve => {
      addOperation({
        companyId,
        investmentId,
        body: {
          type: values.type,
          unitPrice: Math.round(parseFloat(values.unitPrice) * 100),
          quantity: parseInt(values.quantity, 10),
        },
      })
        .then(operation => {
          addNotification({
            type: 'success',
            msg: 'stock.messages.operationAddedSuccessfully',
            duration: 2000,
          });
          close();
        })
        .catch(operation => {
          resolve({
            [FORM_ERROR]: mapOperationError(operation, {
              [ErrorCodes.OPERATION_VIOLATES_STATE]: 'stock.messages.addedOperationViolatesState',
            }),
          });
          console.log('catch', operation);
        })
        .finally(() => {});
    });

  return (
    <Dialog open={isOpen} onClose={close} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{t('stock.titles.addOperation')}</DialogTitle>
      <DialogContent>
        <Form
          onSubmit={async values => submit(values)}
          render={({ handleSubmit, submitting, pristine, hasValidationErrors, submitError }) => (
            <form onSubmit={handleSubmit}>
              <Grid container>
                <Grid item xs={12}>
                  <Field
                    validate={validators(required())}
                    component={Select}
                    type="text"
                    label={t('stock.labels.operation.type')}
                    name="type"
                    variant="outlined"
                    margin="normal"
                    id="type"
                    defaultValue={'buy'}
                  >
                    <MenuItem value={'buy'}>{t('stock.labels.operation.buy')}</MenuItem>
                    <MenuItem value={'sell'}>{t('stock.labels.operation.sell')}</MenuItem>
                  </Field>
                </Grid>
                <Grid item xs={12}>
                  <Field
                    validate={validators(required(), number())}
                    component={TextField}
                    type="text"
                    label={t('stock.labels.operation.unitPrice')}
                    name="unitPrice"
                    variant="outlined"
                    margin="normal"
                    id="unitPrice"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    validate={validators(required(), integerPositive())}
                    component={TextField}
                    type="text"
                    label={t('stock.labels.operation.quantity')}
                    name="quantity"
                    variant="outlined"
                    margin="normal"
                    id="quantity"
                  />
                </Grid>
                <Grid item xs={12}>
                  {Array.isArray(submitError) && submitError.map(e => <Typography key={e}>{t(e)}</Typography>)}
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                    disabled={submitting || pristine || hasValidationErrors}
                  >
                    {t('common.buttons.create')}
                  </Button>
                </Grid>
              </Grid>
            </form>
          )}
        />
      </DialogContent>
    </Dialog>
  );
}
