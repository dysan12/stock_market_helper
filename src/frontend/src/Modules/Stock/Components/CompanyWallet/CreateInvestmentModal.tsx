import { useTranslations } from '../../../Locale/Translations';
import React from 'react';
import { useNotifications } from '../../../Notification/Hooks/Notifications';
import { Button, Dialog, DialogContent, DialogTitle, Typography } from '@material-ui/core';
import { Field, Form } from 'react-final-form';
import { minLength, required, validators } from '../../../../App/Form/Validation/Validation';
import { TextField } from '../../../../App/Form/TextField';
import { useCreateInvestment } from '../../Hooks/Wallet';

export function CreateInvestmentModal({
  companyId,
  isOpen,
  close,
}: {
  companyId: string;
  isOpen: boolean;
  close: () => void;
}) {
  const t = useTranslations();
  const { addNotification } = useNotifications();
  const { createInvestment } = useCreateInvestment();

  const submit = async values =>
    new Promise(resolve => {
      createInvestment({
        companyId,
        body: {
          name: values.name,
        },
      })
        .then(operation => {
          addNotification({
            type: 'success',
            msg: 'stock.messages.investmentCreatedSuccessfully',
            duration: 2000,
          });
        })
        .catch(operation => {
          addNotification({
            type: 'error',
            msg: 'common.messages.sthWentWrongOperation',
            duration: 2000,
          });
          console.log('catch', operation);
        })
        .finally(() => {
          close();
        });
    });

  return (
    <Dialog open={isOpen} onClose={close} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{t('stock.titles.createInvestment')}</DialogTitle>
      <DialogContent>
        <Form
          onSubmit={async values => submit(values)}
          render={({ handleSubmit, submitting, pristine, hasValidationErrors, submitError }) => (
            <form onSubmit={handleSubmit}>
              <Field
                validate={validators(required(), minLength(3))}
                component={TextField}
                type="text"
                label={t('stock.labels.wallet.name')}
                name="name"
                variant="outlined"
                margin="normal"
                id="name"
              />
              {Array.isArray(submitError) && submitError.map(e => <Typography key={e}>{t(e)}</Typography>)}
              <Button
                variant="contained"
                color="primary"
                type="submit"
                fullWidth
                disabled={submitting || pristine || hasValidationErrors}
              >
                {t('common.buttons.create')}
              </Button>
            </form>
          )}
        />
      </DialogContent>
    </Dialog>
  );
}
