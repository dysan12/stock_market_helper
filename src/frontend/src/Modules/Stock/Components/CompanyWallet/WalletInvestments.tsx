import { WalletResponse } from '../../Api/Contract';
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core';
import { useTranslations } from '../../../Locale/Translations';
import React from 'react';
import { InvestmentDetails } from './InvestmentDetails';
import { toPLN } from '../../../../App/Formats/Currencies';
import { useInvestmentExpand } from '../../Hooks/View';

export function WalletInvestments({ wallet }: { wallet: WalletResponse }) {
  const t = useTranslations();
  const { expanded, changeExpanded } = useInvestmentExpand();
  const classes = useStyles({});

  const handleChange = (panel: string) => (event: React.ChangeEvent<{}>, newExpanded: boolean) => {
    changeExpanded(newExpanded ? panel : undefined);
  };

  return (
    <div>
      {wallet.investments.length === 0 ? (
        <Typography>{t('stock.messages.noInvestmentsCreateOne')}</Typography>
      ) : (
        wallet.investments.map(i => (
          <ExpansionPanel square expanded={expanded === i.id} onChange={handleChange(i.id)} className={classes.panel}>
            <ExpansionPanelSummary aria-controls="panel1d-content" id="panel1d-header">
              <Grid item xs={3}>
                <Typography variant="body2">
                  {t('stock.labels.wallet.name')}: {i.name}
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography variant="body2">
                  {t('stock.labels.wallet.totalValue')}: {toPLN(i.totalValue / 100)}
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography variant="body2">
                  {t('stock.labels.wallet.totalUnits')}: {i.totalUnits}
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography variant="body2">
                  {i.profitableFrom !== null
                    ? `${t('stock.labels.wallet.profitableFrom')}:  ${toPLN(i.profitableFrom / 100)}`
                    : t('stock.messages.noUnits')}
                </Typography>
              </Grid>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <InvestmentDetails companyId={wallet.companyId} investment={i} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))
      )}
    </div>
  );
}

const useStyles = makeStyles({
  panel: {
    boxShadow: 'none',
    border: '1px solid',
    marginBottom: '0 !important',
    marginTop: '0 !important',
    borderColor: '#808080',
    borderBottomWidth: '0',
    '&:last-child': {
      borderBottomWidth: '1px',
    },
  },
});
