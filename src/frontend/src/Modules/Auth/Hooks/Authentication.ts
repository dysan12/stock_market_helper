import { LogIn } from '../Redux/Action';
import { State } from '../../../App/Redux/MainReducer';
import { useOperation } from '../../Api/Hooks/Operation';
import { LogInRequest } from '../Api/Contract';

export const useAuthentication = () => {
  const [operation, handler] = useOperation<LogInRequest>(
    payload => LogIn.trigger(payload),
    (operationId: number | undefined) => (state: State) => state.auth.operations.find(o => o.id === operationId)
  );
  return {
    operation,
    authenticate: handler,
  };
};
