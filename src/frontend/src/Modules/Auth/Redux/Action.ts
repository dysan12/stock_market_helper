import { createOperationRoutine } from '../../../App/Redux/Routine';
import { OperationAction } from '../../Api/Redux/Action';
import { DecodedToken, LogInRequest, LogInResponse } from '../Api/Contract';
import { Action } from '../../../App/Redux/Action';
import { ApiFailure } from '../../Api/Error';

export namespace LOG_IN {
  export const name = 'LOG_IN';
  export const init = 'LOG_IN/init';
  export const trigger = 'LOG_IN/trigger';
  export const failure = 'LOG_IN/failure';
  export const success = 'LOG_IN/success';
}

export const LogIn = createOperationRoutine<
  LogInRequest,
  typeof LOG_IN.trigger,
  {},
  any,
  LogInResponse,
  typeof LOG_IN.success,
  ApiFailure,
  typeof LOG_IN.failure
>(LOG_IN.name);

export function logInInit(token: string, decoded: DecodedToken): AuthAction {
  return { type: LOG_IN.init, payload: { token, decoded } };
}

export const LOG_OUT = 'LOG_OUT';

export function logOut(): AuthAction {
  return { type: LOG_OUT, payload: undefined };
}

export type AuthAction =
  | OperationAction<LogInRequest, typeof LOG_IN.trigger>
  | OperationAction<LogInResponse, typeof LOG_IN.success>
  | OperationAction<ApiFailure, typeof LOG_IN.failure>
  | Action<{ token: string; decoded: DecodedToken }, typeof LOG_IN.init>
  | Action<undefined, typeof LOG_OUT>;
