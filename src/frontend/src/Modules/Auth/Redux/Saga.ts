import { all, call, put, takeLatest } from 'redux-saga/effects';
import { LOG_IN, LOG_OUT, LogIn, logInInit } from './Action';
import { callApi, FailedResult, NETWORK_ERROR_STATUS, Result } from '../../Api/Api';
import { AuthService } from '../Services/Auth';
import { DecodedToken, LogInRequest, LogInResponse } from '../Api/Contract';
import { OperationAction } from '../../Api/Redux/Action';
import Router from 'next/router';
import jwtDecode from 'jwt-decode';
import { handleNetworkError } from '../../Api/Redux/Saga';

function* logIn(action: OperationAction<LogInRequest, typeof LOG_IN.trigger>) {
  const { payload } = action;
  try {
    const response: Result<LogInResponse> = yield call(callApi, '/tokens', {
      method: 'POST',
      body: JSON.stringify(payload),
    });
    const { token } = response.body;
    yield put(LogIn.success({ ...response.body, operationId: payload.operationId }));
    AuthService.saveToken(token);
  } catch (e) {
    const result: FailedResult = e;
    if (result.status !== NETWORK_ERROR_STATUS) {
      yield put(LogIn.failure({ errors: result.body, operationId: payload.operationId, status: result.status }));
    }
    yield handleNetworkError(result);
  }
}

function* logOut() {
  AuthService.removeToken();
  yield call(Router.push, '/auth/login');
}

function* initAuth() {
  const token = AuthService.getToken();
  if (token) {
    try {
      const decoded = jwtDecode<DecodedToken>(token);
      yield put(logInInit(token, decoded));
    } catch (e) {
      console.warn('PERSISTED INVALID TOKEN');
    }
  }
}

export function* authSagas() {
  yield all([initAuth(), takeLatest(LOG_IN.trigger, logIn), takeLatest(LOG_OUT, logOut)]);
}
