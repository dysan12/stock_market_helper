import { AuthAction, LOG_IN, LOG_OUT } from './Action';
import { Operation, OperationService } from '../../Api/Services/OperationService';
import { DecodedToken } from '../Api/Contract';
import jwtDecode from 'jwt-decode';

const initialState: AuthState = {
  isAuthenticated: false,
  operations: [],
};

export type AuthState = (
  | {
      isAuthenticated: true;
      token: {
        token: string;
        data: DecodedToken;
      };
      user?: { name: string; login: string };
    }
  | { isAuthenticated: false }
) & { operations: Operation[] };

export function AuthReducer(state: AuthState = initialState, action: AuthAction): AuthState {
  switch (action.type) {
    case LOG_IN.init: {
      const { token, decoded } = action.payload;
      return {
        ...state,
        isAuthenticated: true,
        token: {
          token,
          data: decoded,
        },
      };
    }
    case LOG_IN.trigger: {
      const { payload } = action;
      return {
        ...state,
        operations: [...state.operations, OperationService.createNew(payload.operationId)],
      };
    }
    case LOG_IN.success: {
      const { payload } = action;
      return {
        ...state,
        isAuthenticated: true,
        token: {
          token: payload.token,
          data: jwtDecode<DecodedToken>(payload.token), // todo move decoding to the saga
        },
        operations: OperationService.finishSucceedOperation(state.operations, payload.operationId, payload),
      };
    }
    case LOG_IN.failure: {
      const { payload } = action;
      return {
        ...state,
        operations: OperationService.finishFailedOperation(state.operations, payload.operationId, payload.errors),
      };
    }
    case LOG_OUT: {
      return {
        operations: state.operations,
        isAuthenticated: false,
      };
    }
  }
  return state;
}
