export interface LogInRequest {
  login: string;
  password: string;
}

export interface LogInResponse {
  token: string;
}

export interface DecodedToken {
  user: {
    id: string;
    login: string;
  };
}

export interface RegisterRequest {
  name: string;
  login: string;
  password: string;
}

export interface RegisterResponse {
  id: string;
}
