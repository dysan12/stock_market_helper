import { RegisterRequest, RegisterResponse } from './Contract';
import { callApi } from '../../Api/Api';

export function register(data: RegisterRequest) {
  return callApi<RegisterResponse>('/users', {
    method: 'POST',
    body: JSON.stringify(data),
  });
}
