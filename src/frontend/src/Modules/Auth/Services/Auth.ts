import { Cookies } from '../../../App/Services/Cookies/Cookies';

class Service {
  public saveToken(token: string): void {
    Cookies.set('token', token);
  }

  public removeToken(): void {
    Cookies.remove('token');
  }

  public getToken(): string | null {
    return Cookies.get('token');
  }
}

export const AuthService = new Service();
