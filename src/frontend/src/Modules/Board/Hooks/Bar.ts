import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { changeBoardTitle } from '../Redux/Actions';

export const useTitle = (title: string): void => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(changeBoardTitle(title));
  }, [title, dispatch]);
};
