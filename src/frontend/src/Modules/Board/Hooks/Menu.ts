import { useDispatch, useSelector } from 'react-redux';
import { useCallback } from 'react';
import { addToMenu, removeFromMenu, toggleCompactMenu } from '../Redux/Actions';
import { MenuItem } from '../Components/Menu';
import { State } from '../../../App/Redux/MainReducer';

export const useMenuManager = (): { toggleMenu: (state: boolean) => void } => {
  const dispatch = useDispatch();
  const toggleMenu = useCallback(
    (state: boolean) => {
      dispatch(toggleCompactMenu(state));
    },
    [dispatch]
  );

  return { toggleMenu };
};

export const useMenu = () => {
  const dispatch = useDispatch();
  return {
    addMenuItem: (item: MenuItem) => {
      dispatch(addToMenu(item));
    },
    removeMenuItem: (asPath: string) => {
      dispatch(removeFromMenu(asPath));
    },
    hasMenuItem: (asPath: string) => {
      const items = useSelector((state: State) => state.board.menu.userItems.filter(i => i.as === asPath));
      return items.length > 0;
    },
  };
};
