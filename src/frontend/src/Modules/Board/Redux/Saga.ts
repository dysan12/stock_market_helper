import { all, put, takeLatest } from 'redux-saga/effects';
import { ADD_TO_MENU, BoardAction, REMOVE_FROM_MENU, setMenuItems } from './Actions';
import { LocalStorage } from '../../../App/Services/LocalStorage';
import { MenuItem } from '../Components/Menu';

function* addToMenu(action: BoardAction) {
  if (action.type === ADD_TO_MENU) {
    const { payload } = action;
    let menu = LocalStorage.getItem('menu');
    if (Array.isArray(menu) === false) {
      menu = [];
    }
    const menuItems = menu.filter(i => isMenuItem(i) && i.as !== payload.item.as);
    menuItems.push({ as: payload.item.as, href: payload.item.href, label: payload.item.label });
    LocalStorage.setItem('menu', menuItems);
    yield put(setMenuItems(menuItems));
  }
}

function* removeFromMenu(action: BoardAction) {
  if (action.type === REMOVE_FROM_MENU) {
    const { payload } = action;
    let menu = LocalStorage.getItem('menu');
    if (Array.isArray(menu) === false) {
      menu = [];
    }
    const menuItems = menu.filter(i => isMenuItem(i) && i.as !== payload.path);
    yield put(setMenuItems(menuItems));
  }
}

function* init() {
  let menu = LocalStorage.getItem('menu');
  if (Array.isArray(menu) === false) {
    menu = [];
  }
  const menuItems = menu.filter(i => isMenuItem(i));
  yield put(setMenuItems(menuItems));
}

function isMenuItem(obj: any): obj is MenuItem {
  return typeof obj.as === 'string' && typeof obj.href === 'string' && typeof obj.label === 'string';
}

export function* boardSagas() {
  yield all([init(), takeLatest(ADD_TO_MENU, addToMenu), takeLatest(REMOVE_FROM_MENU, removeFromMenu)]);
}
