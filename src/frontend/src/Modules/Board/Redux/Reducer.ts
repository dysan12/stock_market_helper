import { BoardAction, CHANGE_BOARD_TITLE, SET_MENU, TOGGLE_COMPACT_MENU } from './Actions';
import { MenuItem } from '../Components/Menu';

export interface BoardState {
  menu: {
    isCompact: boolean;
    userItems: MenuItem[];
  };
  title: string;
}

const initial: BoardState = {
  menu: {
    isCompact: false,
    userItems: [],
  },
  title: '',
};

export const BoardReducer = (state = initial, action: BoardAction) => {
  switch (action.type) {
    case TOGGLE_COMPACT_MENU: {
      return { ...state, menu: { isCompact: action.payload.isCompact } };
    }
    case CHANGE_BOARD_TITLE: {
      return { ...state, title: action.payload.title };
    }
    case SET_MENU: {
      return { ...state, menu: { ...state.menu, userItems: action.payload.items } };
    }
  }
  return state;
};
