import { Action } from '../../../App/Redux/Action';
import { MenuItem } from '../Components/Menu';

export const TOGGLE_COMPACT_MENU = 'TOGGLE_COMPACT_MENU';
export const CHANGE_BOARD_TITLE = 'CHANGE_BOARD_TITLE';
export const ADD_TO_MENU = 'ADD_TO_MENU';
export const REMOVE_FROM_MENU = 'REMOVE_FROM_MENU';
export const SET_MENU = 'SET_MENU';

export type BoardAction =
  | Action<{ isCompact: boolean }, typeof TOGGLE_COMPACT_MENU>
  | Action<{ item: MenuItem }, typeof ADD_TO_MENU>
  | Action<{ path: string }, typeof REMOVE_FROM_MENU>
  | Action<{ items: MenuItem[] }, typeof SET_MENU>
  | Action<{ title: string }, typeof CHANGE_BOARD_TITLE>;

export function toggleCompactMenu(isCompact: boolean): BoardAction {
  return {
    type: TOGGLE_COMPACT_MENU,
    payload: {
      isCompact,
    },
  };
}

export function changeBoardTitle(title: string): BoardAction {
  return {
    type: CHANGE_BOARD_TITLE,
    payload: { title },
  };
}

export function addToMenu(item: MenuItem): BoardAction {
  return {
    type: ADD_TO_MENU,
    payload: { item },
  };
}
export function removeFromMenu(path: string): BoardAction {
  return {
    type: REMOVE_FROM_MENU,
    payload: { path },
  };
}

export function setMenuItems(items: MenuItem[]): BoardAction {
  return {
    type: SET_MENU,
    payload: { items },
  };
}
