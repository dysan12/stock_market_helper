import { Box, Container, makeStyles } from '@material-ui/core';
import React from 'react';
import { TopBar } from './TopBar';
import { Menu } from './Menu';

const useStyles = makeStyles({
  secondBox: {
    display: 'flex',
  },
  contentBox: {
    flexGrow: 1,
  },
  boxContainer: {
    padding: 0,
    '& > *': {
      paddingTop: '15px',
    },
  },
});

export const Board = ({ children }) => {
  const styles = useStyles({});
  return (
    <Container maxWidth="xl">
      <Box>
        <TopBar />
      </Box>
      <Box className={styles.secondBox}>
        <Menu />
        <Box className={styles.contentBox}>
          <Container maxWidth="xl" className={styles.boxContainer}>
            {children}
          </Container>
        </Box>
      </Box>
    </Container>
  );
};
