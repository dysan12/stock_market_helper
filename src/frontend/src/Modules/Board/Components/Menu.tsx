import React from 'react';
import { Drawer } from '@material-ui/core';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { Business, Star } from '@material-ui/icons';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { useSelector } from 'react-redux';
import { useTranslations } from '../../Locale/Translations';
import { State } from '../../../App/Redux/MainReducer';
import { DefaultTheme } from '@material-ui/styles';
import { useRouter } from 'next/router';

const useStyles = makeStyles<DefaultTheme, { isMenuCompact: boolean }>({
  drawerPaper: props => ({
    top: 'initial',
    transition: 'width 0.3s',
    width: props.isMenuCompact ? '60px' : '200px',
  }),
  drawer: props => ({
    width: props.isMenuCompact ? '60px' : '200px',
    transition: 'width 0.3s',
  }),
});

export interface MenuItem {
  label: string;
  href: string;
  as: string;
}

export const Menu = () => {
  const t = useTranslations();
  const isMenuCompact = useSelector((state: State) => state.board.menu.isCompact);
  const classes = useStyles({ isMenuCompact });
  const router = useRouter();
  const userMenuItems = useSelector((state: State) => state.board.menu.userItems);

  return (
    <Drawer variant="permanent" open className={classes.drawer} classes={{ paper: classes.drawerPaper }}>
      <Divider />
      <List>
        <ListItem
          button
          onClick={async () => {
            await router.push('/stock/indexes');
          }}
        >
          <ListItemIcon>
            <Business />
          </ListItemIcon>
          <ListItemText primary={t('board.menu.stockIndexes')} />
        </ListItem>
        {userMenuItems.map(i => (
          <ListItem
            key={i.as}
            button
            onClick={async () => {
              await router.push(i.href, i.as);
            }}
          >
            <ListItemIcon>
              <Star />
            </ListItemIcon>
            <ListItemText primary={i.label} />
          </ListItem>
        ))}
        {/*<ListItem*/}
        {/*  button*/}
        {/*  onClick={async () => {*/}
        {/*    await router.push('/statistics');*/}
        {/*  }}*/}
        {/*>*/}
        {/*  <ListItemIcon>*/}
        {/*    <Timeline />*/}
        {/*  </ListItemIcon>*/}
        {/*  <ListItemText primary={t('board.menu.statistics')} />*/}
        {/*</ListItem>*/}
        {/*<ListItem*/}
        {/*  button*/}
        {/*  onClick={async () => {*/}
        {/*    await router.push('/wallet');*/}
        {/*  }}*/}
        {/*>*/}
        {/*  <ListItemIcon>*/}
        {/*    <AccountBalanceWallet />*/}
        {/*  </ListItemIcon>*/}
        {/*  <ListItemText primary={t('board.menu.wallet')} />*/}
        {/*</ListItem>*/}
        {/*<ListItem*/}
        {/*  button*/}
        {/*  onClick={async () => {*/}
        {/*    await router.push('/settings');*/}
        {/*  }}*/}
        {/*>*/}
        {/*  <ListItemIcon>*/}
        {/*    <Build />*/}
        {/*  </ListItemIcon>*/}
        {/*  <ListItemText primary={t('board.menu.settings')} />*/}
        {/*</ListItem>*/}
      </List>
    </Drawer>
  );
};
