import React, { ChangeEvent } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { AppBar, Button, IconButton, makeStyles, Toolbar, Typography } from '@material-ui/core';
import { Language, Menu } from '@material-ui/icons';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { SupportedLang, useTranslations } from '../../Locale/Translations';
// import { useLanguageChanger } from '../../Locale/Hooks/Language';
import { State } from '../../../App/Redux/MainReducer';
import { useMenuManager } from '../Hooks/Menu';
import { logOut } from '../../Auth/Redux/Action';

const useStyles = makeStyles({
  title: {
    marginLeft: '20px',
    flexGrow: 1,
    fontWeight: 500,
  },
  barContainer: {
    height: '64px',
  },
  loader: {
    position: 'absolute',
    right: '50%',
  },
  language: {
    color: 'white',
    '& > div': {
      paddingRight: '30px !important',
      paddingTop: '8px',
    },
    '& > *': {
      border: 'none !important',
      '&:focus': {
        backgroundColor: 'initial',
      },
    },
    '&::before': {
      border: 'none !important',
    },
    '&::after': {
      border: 'none !important',
    },
  },
  languageIcon: {
    color: 'white',
  },
  logout: {
    color: 'white',
    marginLeft: '20px',
  },
});

export const TopBar = () => {
  const classes = useStyles({});
  const { currentLang, availableLangs, isMenuCompact } = useSelector(
    (state: State) => ({
      currentLang: state.locale.language,
      availableLangs: state.locale.availableLanguages,
      isMenuCompact: state.board.menu.isCompact,
    }),
    shallowEqual
  );
  const { toggleMenu } = useMenuManager();
  const t = useTranslations();
  // const [changeLanguage] = useLanguageChanger();
  const title = useSelector((state: State) => state.board.title);
  const dispatch = useDispatch();

  return (
    <div className={classes.barContainer}>
      <AppBar position="fixed" color="primary">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="Open drawer">
            <Menu />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {title !== '' ? t(title) : title}
          </Typography>
          <Select
            value={currentLang}
            onChange={(e: ChangeEvent<{ name?: string; value: unknown }>) => {
              // changeLanguage(e.target.value as SupportedLang)
              console.warn('Language switching is not supported');
            }}
            IconComponent={Language}
            color="primary"
            className={classes.language}
            classes={{ icon: classes.languageIcon }}
          >
            {availableLangs.map(lang => (
              <MenuItem key={lang} value={lang}>
                {t(`locale.languages.${lang}`)}
              </MenuItem>
            ))}
          </Select>
          <Button
            onClick={() => {
              dispatch(logOut());
            }}
            className={classes.logout}
          >
            {t('board.buttons.logout')}
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};
