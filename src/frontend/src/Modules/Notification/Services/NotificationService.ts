export interface Notification extends NotificationData {
  id: number;
  isHidden: boolean;
}

export interface NotificationActions {
  close?: boolean;
}

export interface NotificationData {
  type: NotificationType;
  msg: string; // a component will try to translate a given message
  actions?: NotificationActions;
  duration?: number; // milliseconds
}

export type NotificationType = 'success' | 'warning' | 'error' | 'info';

class Service {
  private id = 1;

  public getNextId(): number {
    return ++this.id;
  }

  public create(data: NotificationData & { id: number }): Notification {
    return {
      ...data,
      isHidden: true,
    };
  }
}

export const NotificationService = new Service();
