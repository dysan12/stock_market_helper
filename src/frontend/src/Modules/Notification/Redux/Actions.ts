import { NotificationData } from '../Services/NotificationService';
import { Action } from '../../../App/Redux/Action';

export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION';
export const EDIT_NOTIFICATION = 'EDIT_NOTIFICATION';
export const CLOSE_NOTIFICATION = 'CLOSE_NOTIFICATION';
export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';

export type NotificationAction =
  | Action<NotificationData & { id: number }, typeof ADD_NOTIFICATION>
  | Action<NotificationData & { id: number }, typeof EDIT_NOTIFICATION>
  | Action<{ id: number }, typeof CLOSE_NOTIFICATION>
  | Action<{ id: number }, typeof SHOW_NOTIFICATION>;

export function addNotification(id: number, data: NotificationData): NotificationAction {
  return {
    type: ADD_NOTIFICATION,
    payload: { id, ...data },
  };
}

/**
 * Do NOT dispatch this action! The saga controls the process of showing.
 */
export function showNotification(id: number): NotificationAction {
  return {
    type: SHOW_NOTIFICATION,
    payload: { id },
  };
}

export function editNotification(id: number, data: NotificationData): NotificationAction {
  return {
    type: EDIT_NOTIFICATION,
    payload: { id, ...data },
  };
}

export function closeNotification(id: number): NotificationAction {
  return {
    type: CLOSE_NOTIFICATION,
    payload: { id },
  };
}
