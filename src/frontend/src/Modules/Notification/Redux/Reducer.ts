import {
  ADD_NOTIFICATION,
  CLOSE_NOTIFICATION,
  EDIT_NOTIFICATION,
  NotificationAction,
  SHOW_NOTIFICATION,
} from './Actions';
import { Notification, NotificationService } from '../Services/NotificationService';

export interface NotificationState {
  notifications: Notification[];
}

const initState: NotificationState = {
  notifications: [],
};

export const NotificationReducer = (
  state: NotificationState = initState,
  action: NotificationAction
): NotificationState => {
  switch (action.type) {
    case SHOW_NOTIFICATION: {
      const { payload } = action;
      const notifications = state.notifications.map(
        (n): Notification => (n.id === payload.id ? { ...n, isHidden: false } : n)
      );
      return { ...state, notifications };
    }
    case EDIT_NOTIFICATION: {
      const { payload } = action;
      const notifications = state.notifications.map(notification =>
        notification.id === payload.id ? { ...notification, ...payload } : notification
      );
      return { ...state, notifications };
    }
    case CLOSE_NOTIFICATION: {
      const { payload } = action;
      const notifications = state.notifications.filter(notification => notification.id !== payload.id);
      return { ...state, notifications };
    }
    case ADD_NOTIFICATION: {
      const { payload } = action;
      const notifications = [...state.notifications];
      const notification = NotificationService.create(payload);
      notifications.push(notification);

      return { ...state, notifications };
    }
  }
  return state;
};
