import { actionChannel, call, cancel, fork, put, select, take } from 'redux-saga/effects';
import {
  ADD_NOTIFICATION,
  CLOSE_NOTIFICATION,
  closeNotification,
  EDIT_NOTIFICATION,
  NotificationAction,
  SHOW_NOTIFICATION,
  showNotification,
} from './Actions';
import { pause } from '../../../App/Redux/RootSaga';
import { State } from '../../../App/Redux/MainReducer';
import { Notification } from '../Services/NotificationService';

const selectors = {
  notifications: (state: State) => state.notifications.notifications,
  notification: (id: number) => (state: State) => state.notifications.notifications.find(n => n.id === id),
};

export function* notificationSaga() {
  let currentlyShowedId;
  let delayedCloseProcess;
  const channel = yield actionChannel([SHOW_NOTIFICATION, EDIT_NOTIFICATION, CLOSE_NOTIFICATION, ADD_NOTIFICATION]);
  while (true) {
    const action: NotificationAction = yield take(channel);
    switch (action.type) {
      case SHOW_NOTIFICATION: {
        const { payload } = action;
        currentlyShowedId = payload.id;
        const notification = yield select(selectors.notification(payload.id));
        if (notification && notification.duration) {
          delayedCloseProcess = yield fork(
            function* delayedClose(notificationId: number, duration: number) {
              yield call(pause, duration);
              yield put(closeNotification(notificationId));
            },
            notification.id,
            notification.duration
          );
        }
        break;
      }
      case EDIT_NOTIFICATION: {
        const { payload } = action;
        if (payload.id === currentlyShowedId && delayedCloseProcess) {
          yield cancel(delayedCloseProcess);
          delayedCloseProcess = undefined;
          yield put(showNotification(payload.id));
        }
        break;
      }
      case ADD_NOTIFICATION: {
        const { payload } = action;
        if (currentlyShowedId === undefined) {
          yield put(showNotification(payload.id));
        }
        break;
      }
      // Reducer should get rid of the closed element from the list of notifications
      case CLOSE_NOTIFICATION: {
        const { payload } = action;
        // if the closed notification is shown, cancel its automatic close
        if (payload.id === currentlyShowedId) {
          if (delayedCloseProcess) {
            yield cancel(delayedCloseProcess);
            delayedCloseProcess = undefined;
          }
          currentlyShowedId = undefined;

          // take an another notification from the stack
          const notifications: Notification[] = yield select(selectors.notifications);
          if (notifications.length > 0) {
            yield put(showNotification(notifications[0].id));
          }
        }
        break;
      }
    }
  }
}
