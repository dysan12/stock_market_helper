import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { NotificationData, NotificationService } from '../Services/NotificationService';
import { addNotification, closeNotification, editNotification } from '../Redux/Actions';

export function useNotifications(): {
  addNotification: (data: NotificationData) => number;
  editNotification: (notificationId: number, data: NotificationData) => void;
  closeNotification: (notificationId: number) => void;
} {
  const dispatch = useDispatch();
  const add = useCallback(
    (data: NotificationData) => {
      const notificationId = NotificationService.getNextId();
      dispatch(addNotification(notificationId, data));
      return notificationId;
    },
    [dispatch]
  );

  const edit = useCallback(
    (notificationId: number, data: NotificationData) => {
      dispatch(editNotification(notificationId, data));
    },
    [dispatch]
  );

  const close = useCallback(
    (notificationId: number) => {
      dispatch(closeNotification(notificationId));
    },
    [dispatch]
  );

  return {
    addNotification: add,
    editNotification: edit,
    closeNotification: close,
  };
}
