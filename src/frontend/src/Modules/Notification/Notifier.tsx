import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../App/Redux/MainReducer';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { Colors } from '../../App/Assets/Themes/Colors';
import { Button, makeStyles, Snackbar, Theme } from '@material-ui/core';
import clsx from 'clsx';
import { Notification, NotificationType } from './Services/NotificationService';
import { closeNotification } from './Redux/Actions';
import { useTranslations } from '../Locale/Translations';
import { Close, Done, Error, Info, Warning } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) => ({
  success: {
    backgroundColor: Colors.green,
    color: Colors.white1,
  },
  error: {
    backgroundColor: Colors.red,
    color: Colors.white1,
  },
  info: {
    backgroundColor: Colors.blue,
    color: Colors.white1,
  },
  warning: {
    backgroundColor: Colors.yellow,
    color: Colors.black,
  },
  snackbarWrapper: {
    fontSize: 13,
    borderRadius: 0,
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}));

export interface Props {
  className?: string;
  onClose?: () => void;
  notification: Notification;
}

export const NotificationVariantIcon: { [key in NotificationType]: any } = {
  success: Done,
  warning: Warning,
  error: Error,
  info: Info,
};

function SnackbarContentWrapper(props: Props) {
  const classes = useStyles({});
  const { className, notification, onClose, ...other } = props;
  const { actions, msg, id, type } = notification;
  const Icon = NotificationVariantIcon[notification.type];
  const dispatch = useDispatch();
  const t = useTranslations();

  let closeBtn;
  if (actions) {
    const { close } = actions;
    if (close === true) {
      closeBtn = (
        <Button key="close" onClick={() => dispatch(closeNotification(id))} className={classes[type]}>
          <Close />
        </Button>
      );
    }
  }

  return (
    <SnackbarContent
      className={clsx(classes.snackbarWrapper, classes[type], className)}
      aria-describedby="client-snackbar"
      message={
        <span className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)} />
          {t(msg)}
          {/* todo silence when translation not found */}
        </span>
      }
      action={[closeBtn]}
      {...other}
    />
  );
}

const Notifier = () => {
  const notifications = useSelector((state: State) => state.notifications.notifications);

  return (
    <>
      {notifications.map(n => (
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={n.isHidden === false}
          key={n.id}
        >
          <SnackbarContentWrapper notification={n} />
        </Snackbar>
      ))}
    </>
  );
};

export default Notifier;
