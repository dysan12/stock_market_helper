import { Typography } from '@material-ui/core';
import React from 'react';
import { Reload } from './Reload';
import { useTranslations } from '../../Modules/Locale/Translations';

export function ErrorReload() {
  const t = useTranslations();
  return (
    <Typography>
      {t('common.messages.sthWentWrong')}
      <Reload />
    </Typography>
  );
}
