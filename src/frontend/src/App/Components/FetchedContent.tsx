import React, { ReactChild } from 'react';
import { CircularProgress } from '@material-ui/core';
import { ErrorReload } from './ErrorReload';
import { Resource } from '../../Modules/Resource/Resource';

export function FetchedContent<T>({ data, generator }: { data: Resource<T>; generator: (data: T) => ReactChild }) {
  let content = null;
  if (data.isFetching === true) {
    content = <CircularProgress size={34} />;
  } else {
    const { response } = data;
    if (response !== undefined && response.errors === false) {
      const { data } = response;
      content = generator(data);
    } else {
      content = <ErrorReload />;
    }
  }
  return content;
}
