import { Button } from '@material-ui/core';
import { useTranslations } from '../../Modules/Locale/Translations';
import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useRouter } from 'next/router';

export function Reload() {
  const t = useTranslations();
  const classes = useStyles({});
  const router = useRouter();
  return (
    <Button
      variant="text"
      className={classes.button}
      onClick={() => {
        router.reload();
      }}
    >
      {t('common.buttons.reload')}
    </Button>
  );
}

const useStyles = makeStyles({
  button: {
    '&:hover': {
      backgroundColor: 'initial',
    },
    '& > span': {
      color: 'blue',
      textDecoration: 'underline',
    },
  },
});
