import React, { Component } from 'react';
import { Cookies } from './Cookies';

function withCookies(BaseComponent) {
  class WrappedComponent extends Component {
    static displayName = `withCookies(${BaseComponent.displayName || BaseComponent.name || 'BaseComponent'})`;

    static async getInitialProps(props) {
      const { req } = props.ctx;
      if (req !== undefined) {
        Cookies.cookies = parseCookies(req.headers.cookie || '');
      }
      let pageProps = {};
      if (BaseComponent.getInitialProps) {
        pageProps = await BaseComponent.getInitialProps(props);
      }
      return pageProps;
    }

    render() {
      return <BaseComponent {...this.props} />;
    }
  }

  return WrappedComponent;
}

function parseCookies(cookie: string) {
  const chunked = cookie.split(';').map(e => e.trim());
  const parsed = chunked.map(cookie => cookie.split('='));
  return Object.fromEntries(parsed);
}

export default withCookies;
