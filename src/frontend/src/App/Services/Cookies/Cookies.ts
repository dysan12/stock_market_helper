import { Cookies as ReactCookies } from 'react-cookie';

class Service {
  private _cookies = {};

  get cookies() {
    // @ts-ignore
    return Object.keys(this._cookies).length === 0 ? new ReactCookies().getAll() : this._cookies;
  }

  /**
   *  use only for hydration
   */
  set cookies(value: { [key: string]: any }) {
    this._cookies = value;
  }

  public set(name: string, value: any): void {
    this._cookies[name] = value;
    new ReactCookies().set(name, value, { path: '/' });
  }

  public remove(name: string): void {
    this._cookies[name] = undefined;
    new ReactCookies().remove(name);
  }

  public get(name: string): any | undefined {
    return this._cookies[name] || new ReactCookies().get(name);
  }
}

export const Cookies = new Service();
