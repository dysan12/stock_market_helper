export default {
  common: {
    yes: 'Yes',
    no: 'No',
    cancel: 'Cancel',
    close: 'Close',
  },
};
