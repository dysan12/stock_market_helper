import fetch from 'isomorphic-unfetch';
import { API_DOMAIN } from './Config';
import { State } from './Redux/MainReducer';
import Router from 'next/router';
import { Cookies } from './Services/Cookies/Cookies';
import { Store } from 'redux';

export type InitCallback = (context: {
  store: Store;
  err: any;
  res: any;
  query: any;
  AppTree: any;
  isServer: any;
  req: any;
  pathname: any;
  asPath: any;
}) => { [key: string]: any };

export function onlyPublicRoute(callback: InitCallback = () => ({})) {
  return async context => {
    const { res } = context;
    const props = {};
    if (typeof window === 'undefined') {
      const token = Cookies.get('token');
      if (token !== undefined) {
        // todo move it to the API module
        const resp = await fetch(`http://${API_DOMAIN}/tokens`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (resp.ok) {
          res.writeHead(302, {
            Location: '/',
          });
          res.end();
        }
      }
    } else {
      const state: State = context.store.getState();
      if (state.auth.isAuthenticated === true) {
        await Router.push('/');
      }
    }
    return { ...props, ...callback(context) };
  };
}

export function protectedRoute(callback: InitCallback = () => ({})) {
  return async context => {
    const { res, store } = context;
    const props = {};
    if (typeof window === 'undefined') {
      const token = Cookies.get('token');
      if (token !== undefined) {
        const resp = await fetch(`http://${API_DOMAIN}/tokens`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (resp.ok === false) {
          res.writeHead(302, {
            Location: '/auth/login',
          });
          res.end();
        }
      } else {
        res.writeHead(302, {
          Location: '/auth/login',
        });
        res.end();
      }
    } else {
      const state: State = store.getState();
      if (state.auth.isAuthenticated === false) {
        await Router.push('/auth/login');
      }
    }
    return { ...props, ...callback(context) };
  };
}
