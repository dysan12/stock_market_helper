export const Button = {
  MuiButton: {
    root: {
      borderRadius: '2px',
      fontFamily: '"averta-semibold", sans-serif',
      fontSize: '16px',
      letterSpacing: '-0.46px',
      lineHeight: '20px',
      textTransform: undefined,
    },
    contained: {
      boxShadow: '0 14px 13px 0 rgba(0,0,0,0.1), 0 2px 30px 0 rgba(0,0,0,0.13)',
    },
    containedPrimary: {
      color: '#F9F9F9',
    },
  },
};
