export const Form = {
  MuiTextField: {
    root: {
      paddingBottom: '20px',
    },
  },
  MuiFormHelperText: {
    root: {
      position: 'absolute' as 'absolute',
      bottom: '0',
    },
  },
};
