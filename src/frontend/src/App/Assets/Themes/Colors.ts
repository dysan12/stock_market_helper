export const Colors = {
  white1: '#E5E5E5',
  gray: '#979797',
  lightGray: '#D8D8D8',
  darkGray: '#565966',
  blue: '#1e6ee8',
  text1: '#43454A',
  red: '#EC4C36',
  green: '#8BB505',
  darkGreen: '#7A9C0C',
  paleBlue: '#6490D1',
  yellow: '#F5D738',
  black: 'black',
};
