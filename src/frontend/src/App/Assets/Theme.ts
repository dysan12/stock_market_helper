import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import { Button } from './Themes/Button';
import { Colors } from './Themes/Colors';
import { Form } from './Themes/Form';

/* basic definitions */
const baseTheme = createMuiTheme({
  palette: {
    background: {
      default: '#f9f9f9',
    },
    primary: {
      main: Colors.blue,
      contrastText: '#fff',
    },
    text: {
      primary: Colors.text1,
    },
  },
});

/* overrides */
export const mainTheme = createMuiTheme({
  ...baseTheme,
  overrides: {
    ...Button,
    ...Form,
  },
});
