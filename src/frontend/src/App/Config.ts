export const { API_DOMAIN } = process.env;
export const INDICATOR_THRESHOLDS = {
  rsi: { buyFrom: 10, sellTo: 10 },
  slowSts: { buyFrom: 10, sellTo: 10 },
  fastSts: { buyFrom: 10, sellTo: 10 },
  macd: { buyFrom: 10, sellTo: 10 },
  trix: { buyFrom: 10, sellTo: 10 },
  sma5: { buyFrom: 10, sellTo: 10 },
  sma15: { buyFrom: 10, sellTo: 10 },
  sma30: { buyFrom: 10, sellTo: 10 },
  sma60: { buyFrom: 10, sellTo: 10 },
  ema5: { buyFrom: 10, sellTo: 10 },
  ema15: { buyFrom: 10, sellTo: 10 },
  ema30: { buyFrom: 10, sellTo: 10 },
  ema60: { buyFrom: 10, sellTo: 10 },
};
