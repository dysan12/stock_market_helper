import { Action } from './Action';
import { WithOperation } from '../../Modules/Api/Services/OperationService';
import { ApiFailure } from '../../Modules/Api/Error';
import { ApiResponse } from '../../Modules/Api/Api';

export interface Routine<T, TType, R, RType, S, SType, Fa, FaType, Fu, FuType> {
  trigger: (payload: T) => Action<T, TType>;
  TRIGGER: string;
  success: (payload: S) => Action<S, SType>;
  SUCCESS: string;
  request: (payload: R) => Action<R, RType>;
  REQUEST: string;
  failure: (payload: Fa) => Action<Fa, FaType>;
  FAILURE: string;
  fulfill: (payload: Fu) => Action<Fu, FuType>;
  FULFILL: string;
}
export type ApiRoutine = Routine<any, any, any, any, ApiResponse, any, ApiFailure, any, any, any>;
export type ApiOperationRoutine = Routine<
  {} & WithOperation,
  any,
  {} & WithOperation,
  any,
  ApiResponse & WithOperation,
  any,
  ApiFailure & WithOperation,
  any,
  {} & WithOperation,
  any
>;

export function createRoutine<
  T = any,
  TType = any,
  R = any,
  RType = any,
  S = any,
  SType = any,
  Fa = any,
  FaType = any,
  Fu = any,
  FuType = any
>(name: string): Routine<T, TType, R, RType, S, SType, Fa, FaType, Fu, FuType> {
  const names = {
    TRIGGER: `${name}/trigger`,
    SUCCESS: `${name}/success`,
    REQUEST: `${name}/request`,
    FAILURE: `${name}/failure`,
    FULFILL: `${name}/fulfill`,
  };
  return {
    // @ts-ignore
    trigger: (payload: T) => ({ type: names.TRIGGER, payload }),
    // @ts-ignore
    request: (payload: R) => ({ type: names.REQUEST, payload }),
    // @ts-ignore
    success: (payload: S) => ({ type: names.SUCCESS, payload }),
    // @ts-ignore
    failure: (payload: Fa) => ({ type: names.FAILURE, payload }),
    // @ts-ignore
    fulfill: (payload: Fu) => ({ type: names.FULFILL, payload }),
    ...names,
  };
}

export function createOperationRoutine<
  T = {},
  TType = any,
  R = {},
  RType = any,
  S = {},
  SType = any,
  Fa = {},
  FaType = any,
  Fu = {},
  FuType = any
>(name: string) {
  return createRoutine<
    T & WithOperation,
    TType,
    R & WithOperation,
    RType,
    S & WithOperation,
    SType,
    Fa & WithOperation,
    FaType,
    Fu & WithOperation,
    FuType
  >(name);
}
