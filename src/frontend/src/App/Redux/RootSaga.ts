import { all } from 'redux-saga/effects';
import { authSagas } from '../../Modules/Auth/Redux/Saga';
import { notificationSaga } from '../../Modules/Notification/Redux/Saga';
import { stockSagas } from '../../Modules/Stock/Redux/Saga';
import { boardSagas } from '../../Modules/Board/Redux/Saga';

export default function* rootSaga() {
  yield all([authSagas(), notificationSaga(), stockSagas(), boardSagas()]);
}

export function pause(sec) {
  return new Promise(resolve => {
    setTimeout(_ => {
      resolve();
    }, sec);
  });
}
