export interface Action<Payload = any, T = string> {
  type: T;
  payload: Payload;
}
