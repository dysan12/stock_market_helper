import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { createRootReducer } from './MainReducer';
import rootSaga from './RootSaga';

export const initializeStore = (preloadedState: any, { isServer, req = null }) => {
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(createRootReducer(), preloadedState, composeWithDevTools(applyMiddleware(sagaMiddleware)));

  if (req || !isServer) {
    // @ts-ignore
    store.sagaTask = sagaMiddleware.run(rootSaga);
  }

  return store;
};
