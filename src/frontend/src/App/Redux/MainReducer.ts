import { combineReducers } from 'redux';
import { LocaleReducer, LocaleState } from '../../Modules/Locale/Redux/Reducer';
import { AuthReducer, AuthState } from '../../Modules/Auth/Redux/Reducer';
import { NotificationReducer, NotificationState } from '../../Modules/Notification/Redux/Reducer';
import { BoardReducer, BoardState } from '../../Modules/Board/Redux/Reducer';
import { StockReducer, StockState } from '../../Modules/Stock/Redux/Reducer';

export function createRootReducer() {
  return combineReducers({
    locale: LocaleReducer,
    auth: AuthReducer,
    notifications: NotificationReducer,
    board: BoardReducer,
    stock: StockReducer,
  });
}

export interface State {
  locale: LocaleState;
  router: any;
  auth: AuthState;
  notifications: NotificationState;
  board: BoardState;
  stock: StockState;
}
