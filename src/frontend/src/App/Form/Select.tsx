/* eslint-disable react/prop-types */
import React from 'react';
import { FormControl, InputLabel, Select as MaterialSelect } from '@material-ui/core';

export const Select = ({ input: { name, onChange, value, ...restInput }, meta, ...rest }) => {
  const showError = ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched;
  const labelId = rest.label + rest.id;
  return (
    <FormControl>
      <InputLabel id={labelId}>{rest.label}</InputLabel>
      <MaterialSelect
        {...rest}
        name={name}
        error={showError}
        inputProps={restInput}
        onChange={onChange}
        value={value}
        labelId={labelId}
      />
    </FormControl>
  );
};
