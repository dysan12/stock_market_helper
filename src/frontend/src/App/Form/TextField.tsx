/* eslint-disable react/prop-types */
import React from 'react';
import { TextField as MaterialTextField } from '@material-ui/core';

export const TextField = ({ input: { name, onChange, value, ...restInput }, meta, ...rest }) => {
  const showError = ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) && meta.touched;

  return (
    <MaterialTextField
      {...rest}
      name={name}
      helperText={showError ? meta.error || meta.submitError : undefined}
      error={showError}
      inputProps={restInput}
      onChange={onChange}
      value={value}
    />
  );
};
