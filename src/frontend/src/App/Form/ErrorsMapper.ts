import { ErrorCodes } from '../../Modules/Api/Error';
import { Operation } from '../../Modules/Api/Services/OperationService';

export function mapError(error: any, map: { [key: string]: string } = {}): string[] {
  if (Array.isArray(error.body)) {
    return error.body.map(e => {
      if (map[e.code] !== undefined) {
        return map[e.code];
      }
      if (e.code === ErrorCodes.USER_NOT_FOUND) {
        return 'common.messages.loginInvalid';
      }
      if (e.code === ErrorCodes.USER_LOGIN_OCCUPIED) {
        return 'common.messages.loginOccupied';
      }
      if (e.code === ErrorCodes.USER_PASSWORD_NOT_MATCH) {
        return 'common.messages.passwordInvalid';
      }
      console.warn('Error', e);
      return 'common.messages.unknown';
    });
  }
  if (typeof error === 'string') {
    return [error];
  }
  return [JSON.stringify(error)];
}

export function mapOperationError(operation: Operation, map: { [key: string]: string } = {}): string[] {
  if (operation.result !== undefined) {
    const { response } = operation.result;
    return mapError({ body: response }, map);
  }

  return [];
}
