import * as React from 'react';
import { useTranslations } from '../../../Modules/Locale/Translations';

interface Props {
  msg: string;
  args?: any;
}

export const ValidationMessage = (props: Props) => {
  const t = useTranslations();
  return <>{t(props.msg, props.args || {})}</>;
};
