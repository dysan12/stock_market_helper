import * as React from 'react';
import { ValidationMessage } from './ValidationMessage';

const isEmpty = (value: any) => typeof value === 'undefined' || value === null || value === '';

export const integer = (message = 'common.validation.integer') => value =>
  !isEmpty(value) && Number.isInteger(Number(value)) === false ? <ValidationMessage msg={message} /> : undefined;

export const integerPositive = (message = 'common.validation.integerPositive') => value =>
  !isEmpty(value) && (parseInt(value, 10) != value || Number(value) <= 0) ? (
    <ValidationMessage msg={message} />
  ) : (
    undefined
  );

export const number = (message = 'common.validation.number') => value =>
  !isEmpty(value) && parseFloat(value) != value ? <ValidationMessage msg={message} /> : undefined;

export const required = (message = 'common.validation.required') => value =>
  value ? undefined : <ValidationMessage msg={message} />;

export const minLength = (min: number, message = 'common.validation.minLength') => value =>
  value.length >= min ? undefined : <ValidationMessage msg={message} args={{ smart_count: min }} />;

export const maxLength = (max: number, message = 'common.validation.maxLength') => value =>
  value.length <= max ? undefined : <ValidationMessage msg={message} args={{ smart_count: max }} />;

export const validators = (...validators) => value =>
  validators.reduce((error, validator) => error || validator(value), undefined);
