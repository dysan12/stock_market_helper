/* eslint-disable react/prop-types */
import React from 'react';
import { Checkbox as MaterialCheckbox, FormControlLabel } from '@material-ui/core';

export const Checkbox = ({ input: { name, onChange, ...restInput }, label, meta, ...rest }) => (
  <FormControlLabel
    control={<MaterialCheckbox {...rest} name={name} inputProps={restInput} onChange={onChange} />}
    label={label}
  />
);
