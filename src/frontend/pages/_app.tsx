import React from 'react';
import App from 'next/app';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import { mainTheme } from '../src/App/Assets/Theme';
import Notifier from '../src/Modules/Notification/Notifier';
import withRedux from 'next-redux-wrapper';
import nextReduxSaga from 'next-redux-saga';
import { Provider } from 'react-redux';
import { initializeStore } from '../src/App/Redux/Store';
import withCookies from '../src/App/Services/Cookies/WithCookies';

class CoreApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    // @ts-ignore
    const { Component, pageProps, store } = this.props;
    const Comp = nextReduxSaga(Component);
    return (
      <ThemeProvider theme={mainTheme}>
        <Provider store={store}>
          <Notifier />
          <Comp {...pageProps} />
        </Provider>
      </ThemeProvider>
    );
  }
}

export default withRedux(initializeStore)(withCookies(nextReduxSaga(CoreApp)));
