import React from 'react';
import { protectedRoute } from '../src/App/Routes';
import { Board } from '../src/Modules/Board/Components/Board';
import { useTitle } from '../src/Modules/Board/Hooks/Bar';
import Head from 'next/head';
import { useTranslations } from '../src/Modules/Locale/Translations';

const Home = () => {
  const t = useTranslations();
  useTitle('board.titles.dashboard');
  return (
    <Board>
      <Head>
        <title>{t('board.titles.dashboard')}</title>
      </Head>
    </Board>
  );
};

Home.getInitialProps = protectedRoute();

export default Home;
