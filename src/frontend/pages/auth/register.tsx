import React, { useState } from 'react';
import { Avatar, Button, CircularProgress, Container, Grid, makeStyles, Typography } from '@material-ui/core';
import { Assignment } from '@material-ui/icons';
import { Field, Form } from 'react-final-form';
import { minLength, required, validators } from '../../src/App/Form/Validation/Validation';
import { useTranslations } from '../../src/Modules/Locale/Translations';
import { TextField } from '../../src/App/Form/TextField';
import Link from 'next/link';
import { FormButton } from '../../src/App/Assets/Components/FormButton';
import { useRouter } from 'next/router';
import { FORM_ERROR } from 'final-form';
import { mapError } from '../../src/App/Form/ErrorsMapper';
import { register } from '../../src/Modules/Auth/Api/Register';
import { useNotifications } from '../../src/Modules/Notification/Hooks/Notifications';
import { onlyPublicRoute } from '../../src/App/Routes';
import Head from 'next/head';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    ...FormButton,
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonProgress: {
    color: 'white',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

export const RegisterPage = () => {
  const t = useTranslations();
  // useProtectedRoute();
  const classes = useStyles({});
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const { addNotification } = useNotifications();

  async function onSubmit(values) {
    setIsLoading(true);
    try {
      await register({
        login: values.login,
        password: values.password,
        name: values.name,
      });
      await router.push('/auth/login');
      addNotification({
        type: 'success',
        msg: 'auth.messages.registrationCompleted',
        duration: 7000,
      });
    } catch (e) {
      return { [FORM_ERROR]: mapError(e) };
    } finally {
      setIsLoading(false);
    }
  }

  // todo redirect if authenticated to the home page
  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <Head>
        <title>{t('auth.titles.signin')}</title>
      </Head>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          {isLoading ? <CircularProgress size={24} className={classes.buttonProgress} /> : <Assignment />}
        </Avatar>
        <Typography component="h1" variant="h5">
          {t('auth.headers.registration')}
        </Typography>
        <Form
          onSubmit={async values => {
            if (values.passwordConfirmation !== values.password) {
              return {
                passwordConfirmation: t('auth.fields.errors.notIdenticalConfirmationPwd'),
              };
            }
            return await onSubmit(values);
          }}
          render={({ handleSubmit, submitting, pristine, hasValidationErrors, submitError }) => (
            <form onSubmit={handleSubmit} className={classes.form}>
              <Field
                validate={validators(required(), minLength(3))}
                component={TextField}
                type="text"
                label={t('auth.fields.login')}
                name="login"
                variant="outlined"
                margin="normal"
                fullWidth
                id="login"
                autoComplete="login"
              />
              <Field
                validate={validators(required(), minLength(3))}
                component={TextField}
                type="text"
                label={t('auth.fields.name')}
                name="name"
                variant="outlined"
                margin="normal"
                fullWidth
                id="login"
                autoComplete="name"
              />
              <Field
                validate={validators(required(), minLength(3))}
                component={TextField}
                type="password"
                label={t('auth.fields.password')}
                name="password"
                variant="outlined"
                margin="normal"
                fullWidth
                id="password"
              />
              <Field
                validate={validators(required(), minLength(3))}
                component={TextField}
                type="password"
                label={t('auth.fields.passwordConfirmation')}
                name="passwordConfirmation"
                variant="outlined"
                margin="normal"
                fullWidth
                id="password"
              />
              {Array.isArray(submitError) && submitError.map(e => <Typography key={e}>{t(e)}</Typography>)}
              <Button
                variant="contained"
                className={classes.submit}
                color="primary"
                type="submit"
                fullWidth
                disabled={submitting || pristine || hasValidationErrors}
              >
                {t('auth.buttons.register')}
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="/auth/login">
                    <a>{t('auth.links.goToLogin')}</a>
                  </Link>
                </Grid>
              </Grid>
            </form>
          )}
        />
      </div>
    </Container>
  );
};

RegisterPage.getInitialProps = onlyPublicRoute((context: any) => {
  console.log('inital props');
  return {};
});

export default RegisterPage;
