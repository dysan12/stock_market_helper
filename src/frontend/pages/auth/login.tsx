import { Avatar, Button, CircularProgress, Container, Grid, makeStyles, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { Field, Form } from 'react-final-form';
import { LockOutlined } from '@material-ui/icons';
import { useTranslations } from '../../src/Modules/Locale/Translations';
import { useRouter } from 'next/router';
import { minLength, required, validators } from '../../src/App/Form/Validation/Validation';
import { TextField } from '../../src/App/Form/TextField';
import { FormButton } from '../../src/App/Assets/Components/FormButton';
import Link from 'next/link';
import { FORM_ERROR } from 'final-form';
import { mapOperationError } from '../../src/App/Form/ErrorsMapper';
import { useAuthentication } from '../../src/Modules/Auth/Hooks/Authentication';
import { useNotifications } from '../../src/Modules/Notification/Hooks/Notifications'; // todo abstract duplicated code
import { onlyPublicRoute } from '../../src/App/Routes';
import Head from 'next/head';
import { Board } from '../../src/Modules/Board/Components/Board';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    ...FormButton,
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonProgress: {
    color: 'white',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

export const LoginPage = () => {
  const t = useTranslations();
  const classes = useStyles({});
  const { authenticate } = useAuthentication();
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const { addNotification } = useNotifications();

  const submit = async values =>
    new Promise(resolve => {
      setIsLoading(true);
      authenticate({ login: values.login, password: values.password })
        .then(operation => {
          router.push('/');
          addNotification({
            type: 'success',
            msg: 'auth.messages.loggedSuccessfully',
            duration: 4000,
          });
        })
        .catch(operation => {
          console.log('catch', operation);
          resolve({ [FORM_ERROR]: mapOperationError(operation) });
        })
        .finally(() => {
          setIsLoading(false);
        });
    });

  // todo redirect if authenticated to the home page
  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <Head>
        <title>{t('auth.titles.login')}</title>
      </Head>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          {isLoading ? <CircularProgress size={24} className={classes.buttonProgress} /> : <LockOutlined />}
        </Avatar>
        <Typography component="h1" variant="h5">
          {t('auth.headers.login')}
        </Typography>
        <Form
          onSubmit={async values => await submit(values)}
          render={({ handleSubmit, submitting, pristine, hasValidationErrors, submitError }) => (
            <form onSubmit={handleSubmit} className={classes.form}>
              <Field
                validate={validators(required(), minLength(3))}
                component={TextField}
                type="text"
                label={t('auth.fields.login')}
                name="login"
                variant="outlined"
                margin="normal"
                fullWidth
                id="login"
                autoComplete="login"
              />
              <Field
                validate={validators(required(), minLength(3))}
                component={TextField}
                type="password"
                label={t('auth.fields.password')}
                name="password"
                variant="outlined"
                margin="normal"
                fullWidth
                id="password"
              />
              {Array.isArray(submitError) && submitError.map(e => <Typography key={e}>{t(e)}</Typography>)}
              <Button
                variant="contained"
                className={classes.submit}
                color="primary"
                type="submit"
                fullWidth
                disabled={submitting || pristine || hasValidationErrors || isLoading}
              >
                {t('auth.buttons.login')}
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="/auth/register">
                    <a>{t('auth.links.goToRegister')}</a>
                  </Link>
                </Grid>
              </Grid>
            </form>
          )}
        />
      </div>
    </Container>
  );
};

LoginPage.getInitialProps = onlyPublicRoute();

export default LoginPage;
