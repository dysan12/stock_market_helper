import { useTitle } from '../../../src/Modules/Board/Hooks/Bar';
import { useTranslations } from '../../../src/Modules/Locale/Translations';
import { useRouter } from 'next/router';
import { Board } from '../../../src/Modules/Board/Components/Board';
import {
  CircularProgress,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Paper,
  Typography,
} from '@material-ui/core';
import { protectedRoute } from '../../../src/App/Routes';
import React from 'react';
import { FetchIndex } from '../../../src/Modules/Stock/Redux/Actions/FetchIndex';
import { useIndex } from '../../../src/Modules/Stock/Hooks/Index';
import { ErrorReload } from '../../../src/App/Components/ErrorReload';
import { CompaniesList } from '../../../src/Modules/Stock/Components/CompaniesList';
import { toPLN } from '../../../src/App/Formats/Currencies';
import Head from 'next/head';

function IndexPage() {
  useTitle('stock.titles.index');
  const router = useRouter();
  const { query } = router;
  const index = useIndex(query.id as string);
  const t = useTranslations();
  const classes = useStyles({});

  let content = null;
  if (index.isFetching === true) {
    content = <CircularProgress size={34} />;
  } else {
    const { response } = index;
    if (response !== undefined && response.errors === false) {
      const { data } = response;
      content = (
        <Grid container>
          <Grid item xs={12}>
            <Grid container wrap={'nowrap'}>
              <Grid item xs={1} />
              <Typography variant="h4">{`${data.name}(${data.uid})`}</Typography>
            </Grid>
          </Grid>
          <Divider variant="middle" />

          <Grid item xs>
            <List>
              <ListItem>
                <ListItemText>{`${t('stock.labels.index.rate')}: ${toPLN(data.rate)}`}</ListItemText>
              </ListItem>
              <Divider component="li" />
              <ListItem>
                <ListItemText>{`${t('stock.labels.index.rateChange')}: ${data.rateChange}`}</ListItemText>
              </ListItem>
              <Divider component="li" />
            </List>
          </Grid>
          <Grid item xs>
            <List>
              <ListItem>
                <ListItemText>{`${t('stock.labels.index.openingRate')}: ${toPLN(data.openingRate)}`}</ListItemText>
              </ListItem>
              <Divider component="li" />
              <ListItem>
                <ListItemText>{`${t('stock.labels.index.closingRate')}: ${toPLN(data.closingRate)}`}</ListItemText>
              </ListItem>
              <Divider component="li" />
            </List>
          </Grid>
          <Grid item xs>
            <List>
              <ListItem>
                <ListItemText>{`${t('stock.labels.index.maxRate')}: ${toPLN(data.maxRate)}`}</ListItemText>
              </ListItem>
              <Divider component="li" />
              <ListItem>
                <ListItemText>{`${t('stock.labels.index.minRate')}: ${toPLN(data.minRate)}`}</ListItemText>
              </ListItem>
              <Divider component="li" />
            </List>
          </Grid>
          <Grid item xs={12}>
            <Grid item xs={1}>
              <Typography variant="h5">{t('stock.titles.companies')}</Typography>
            </Grid>
            <Grid xs={12}>
              <Divider variant="middle" />
            </Grid>
          </Grid>
          <Grid container justify="center">
            <Grid xs={11}>
              <CompaniesList companies={data.companies} />
            </Grid>
          </Grid>
          <Grid item xs={1} />
        </Grid>
      );
    } else {
      content = <ErrorReload />;
    }
  }

  return (
    <Board>
      <Head>
        <title>{t('stock.titles.index')}</title>
      </Head>
      <Paper className={classes.paper}>{content}</Paper>
    </Board>
  );
}

IndexPage.getInitialProps = protectedRoute(({ store, query }) => {
  store.dispatch(FetchIndex.trigger({ id: query.id }));
  return {};
});

const useStyles = makeStyles({
  paper: {
    textAlign: 'center',
  },
});

export default IndexPage;
