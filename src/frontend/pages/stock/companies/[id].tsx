import { useTitle } from '../../../src/Modules/Board/Hooks/Bar';
import { useTranslations } from '../../../src/Modules/Locale/Translations';
import { useRouter } from 'next/router';
import { Board } from '../../../src/Modules/Board/Components/Board';
import {
  Divider,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Paper,
  Typography,
} from '@material-ui/core';
import React from 'react';
import { useCompany, useCompanyIndicators } from '../../../src/Modules/Stock/Hooks/Company';
import { CompanyIndicatorsResponse, CompanyResponse, WalletResponse } from '../../../src/Modules/Stock/Api/Contract';
import { useWallet } from '../../../src/Modules/Stock/Hooks/Wallet';
import { CompanyWallet } from '../../../src/Modules/Stock/Components/CompanyWallet/ComapnyWallet';
import { FetchedContent } from '../../../src/App/Components/FetchedContent';
import { protectedRoute } from '../../../src/App/Routes';
import { FetchCompany } from '../../../src/Modules/Stock/Redux/Actions/FetchCompany';
import { FetchCompanyIndicators } from '../../../src/Modules/Stock/Redux/Actions/FetchCompanyIndicators';
import { FetchWallet } from '../../../src/Modules/Stock/Redux/Actions/FetchWallet';
import { Star, StarBorder } from '@material-ui/icons';
import { useMenu } from '../../../src/Modules/Board/Hooks/Menu';
import Head from 'next/head';
import { FormattedIndicator } from '../../../src/Modules/Stock/Components/FormattedIndicator';

function CompanyPage() {
  useTitle('stock.titles.company');
  const router = useRouter();
  const { query } = router;
  const companyId = query.id as string;
  const company = useCompany(companyId);
  const companyIndicators = useCompanyIndicators(companyId);
  const wallet = useWallet(companyId);
  const t = useTranslations();
  const classes = useStyles({});
  const { addMenuItem, removeMenuItem, hasMenuItem } = useMenu();
  const { asPath, pathname } = router;
  return (
    <Board>
      <Head>
        <title>{t('stock.titles.company')}</title>
      </Head>
      <Paper className={classes.paper}>
        <Grid container spacing={1}>
          <FetchedContent
            data={company}
            generator={(data: CompanyResponse) => {
              return (
                <>
                  <Grid item xs={12}>
                    <Grid container wrap={'nowrap'}>
                      <Grid item xs={1} />
                      <Typography variant="h4">{`${data.name}(${data.uid})`}</Typography>
                      {hasMenuItem(asPath) === true ? (
                        <IconButton
                          onClick={() => {
                            removeMenuItem(asPath);
                          }}
                        >
                          <Star />
                        </IconButton>
                      ) : (
                        <IconButton onClick={() => addMenuItem({ href: pathname, as: asPath, label: data.name })}>
                          <StarBorder />
                        </IconButton>
                      )}
                    </Grid>
                  </Grid>
                  <Grid container>
                    <Grid item xs={6}>
                      <List>
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.company.rateChange')}: ${data.rateChange}`}</ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.company.buyOffer')}: ${data.buyOffer}`}</ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.company.exchangeValue')}: ${
                            data.exchangeValue
                          }`}</ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.company.exchangeVolume')}: ${
                            data.exchangeVolume
                          }`}</ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                      </List>
                    </Grid>
                    <Grid item xs={6}>
                      <List>
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.company.sellOffer')}: ${data.sellOffer}`}</ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.company.maxRate')}: ${data.maxRate}`}</ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.company.minRate')}: ${data.minRate}`}</ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                      </List>
                    </Grid>
                  </Grid>
                </>
              );
            }}
          />
          <Grid item xs={12}>
            <FetchedContent
              data={wallet}
              generator={(data: WalletResponse) => {
                return <CompanyWallet wallet={data} />;
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <FetchedContent
              data={companyIndicators}
              generator={(data: CompanyIndicatorsResponse) => {
                return (
                  <Grid container>
                    <Grid item xs={6}>
                      <List>
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.rsi')}: `}
                            <FormattedIndicator name={'rsi'} value={data.rsi} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.slowSts')}: `}
                            <FormattedIndicator name={'slowSts'} value={data.slowSts} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.fastSts')}: `}
                            <FormattedIndicator name={'fastSts'} value={data.fastSts} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                      </List>
                    </Grid>
                    <Grid item xs={6}>
                      <List>
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.macd')}: `}
                            <FormattedIndicator name={'macd'} value={data.macd} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.trix')}: `}
                            <FormattedIndicator name={'trix'} value={data.trix} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.sma5')}: `}
                            <FormattedIndicator name={'sma5'} value={data.sma5} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                      </List>
                    </Grid>
                    <Grid item xs={6}>
                      <List>
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.sma15')}: `}
                            <FormattedIndicator name={'sma15'} value={data.sma15} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.sma30')}: `}
                            <FormattedIndicator name={'sma30'} value={data.sma30} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.sma60')}: `}
                            <FormattedIndicator name={'sma60'} value={data.sma60} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                      </List>
                    </Grid>
                    <Grid item xs={6}>
                      <List>
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.ema5')}: `}
                            <FormattedIndicator name={'ema5'} value={data.ema5} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.ema15')}: `}
                            <FormattedIndicator name={'ema15'} value={data.ema15} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                        <ListItem>
                          <ListItemText>{`${t('stock.labels.indicators.ema30')}: `}
                            <FormattedIndicator name={'ema30'} value={data.ema30} />
                          </ListItemText>
                        </ListItem>
                        <Divider variant={'middle'} component="li" />
                      </List>
                    </Grid>
                  </Grid>
                );
              }}
            />
          </Grid>
        </Grid>
      </Paper>
    </Board>
  );
}


CompanyPage.getInitialProps = protectedRoute(({ store, query }) => {
  store.dispatch(FetchCompany.trigger({ companyId: query.id }));
  store.dispatch(FetchCompanyIndicators.trigger({ companyId: query.id }));
  store.dispatch(FetchWallet.trigger({ companyId: query.id }));
  return {};
});

const useStyles = makeStyles({
  paper: {
    textAlign: 'center',
  },
  infoRow: {
    marginLeft: '5px',
  },
});

export default CompanyPage;
