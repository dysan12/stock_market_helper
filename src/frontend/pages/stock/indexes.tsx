import { protectedRoute } from '../../src/App/Routes';
import { Board } from '../../src/Modules/Board/Components/Board';
import { useTitle } from '../../src/Modules/Board/Hooks/Bar';
import { CircularProgress, Paper, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import { useIndexes } from '../../src/Modules/Stock/Hooks/Index';
import { FetchIndexes } from '../../src/Modules/Stock/Redux/Actions/FetchIndexes';
import React from 'react';
import { useTranslations } from '../../src/Modules/Locale/Translations';
import { ErrorReload } from '../../src/App/Components/ErrorReload';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/styles';
import { toPLN } from '../../src/App/Formats/Currencies';
import Head from 'next/head';

function IndexesPage() {
  useTitle('stock.titles.indexes');
  const indexes = useIndexes();
  const t = useTranslations();
  const router = useRouter();
  const classes = useStyles({});

  let tableContent = null;
  if (indexes.isFetching === true) {
    tableContent = <CircularProgress size={34} />;
  } else {
    const { response } = indexes;
    if (response !== undefined && response.errors === false) {
      tableContent = (
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>{t('stock.labels.index.name')}</TableCell>
              <TableCell>{t('stock.labels.index.rate')}</TableCell>
              <TableCell>{t('stock.labels.index.rateChange')}</TableCell>
              <TableCell>{t('stock.labels.index.openingRate')}</TableCell>
              <TableCell>{t('stock.labels.index.closingRate')}</TableCell>
              <TableCell>{t('stock.labels.index.maxRate')}</TableCell>
              <TableCell>{t('stock.labels.index.minRate')}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {response.data.data.map(index => {
              return (
                <TableRow
                  className={classes.tableRow}
                  key={index.uid}
                  onClick={() => router.push('/stock/indexes/[id]', `/stock/indexes/${index.uid}`)}
                >
                  <TableCell>{`${index.name}(${index.uid})`}</TableCell>
                  <TableCell>{toPLN(index.rate)}</TableCell>
                  <TableCell>{index.rateChange}</TableCell>
                  <TableCell>{toPLN(index.openingRate)}</TableCell>
                  <TableCell>{toPLN(index.closingRate)}</TableCell>
                  <TableCell>{toPLN(index.maxRate)}</TableCell>
                  <TableCell>{toPLN(index.minRate)}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      );
    } else {
      tableContent = <ErrorReload />;
    }
  }

  return (
    <Board>
      <Head>
        <title>{t('stock.titles.indexes')}</title>
      </Head>
      <Paper className={classes.paper}>{tableContent}</Paper>
    </Board>
  );
}

IndexesPage.getInitialProps = protectedRoute(({ store }) => {
  store.dispatch(FetchIndexes.trigger({}));
  return {};
});

const useStyles = makeStyles({
  paper: {
    textAlign: 'center',
  },
  tableRow: {
    '&:hover': {
      cursor: 'pointer',
      background: '#e6e6e659',
    },
  },
});

export default IndexesPage;
