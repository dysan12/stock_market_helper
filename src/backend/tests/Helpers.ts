import { Response } from 'supertest';
import { request } from './Request';
import { CreateInvestmentData } from '../src/Modules/Wallet/Domain/Data/CreateInvestment';

export async function getToken(login: string, password: string): Promise<string> {
  const res2: Response = await request.post('/tokens').send({ login, password });
  expect(res2.status).toEqual(201); // eg. means that provided password is OK
  expect(res2.body).toHaveProperty('token');
  const { token } = res2.body;

  return token;
}

export async function logIn(login: string, password: string): Promise<void> {
  const token = await getToken(login, password);
  request.setToken(token);
}

export async function authorizeAsNewUser(): Promise<void> {
  const user = await createUser();
  const token = await getToken(user.login, user.password);
  request.setToken(token);
}

export async function createUser(
  userData: any = {},
): Promise<{ id: string; name: string; login: string; password: string }> {
  const user = {
    name: `John${Math.random()}`,
    login: Math.random().toString(),
    password: 'qweasdzxc',
  };
  Object.assign(user, userData);
  const res: Response = await request.post('/users').send(user);
  expect(res.status).toEqual(201);
  expect(res.body).toHaveProperty('id');
  return { id: res.body.id, ...user };
}

export async function createInvestment(
  companyId: string,
  createData: CreateInvestmentData.Interface,
): Promise<{ id: string }> {
  const res: Response = await request.post(`/wallets/${companyId}/investments`).send(createData);
  expect(res.status).toEqual(201);
  expect(res.body).toHaveProperty('id');
  return { id: res.body.id };
}
