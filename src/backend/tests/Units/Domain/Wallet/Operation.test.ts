import { Operation } from '../../../../src/Modules/Wallet/Domain/Operation';
import { PositiveInteger } from '../../../../src/Modules/Wallet/Domain/VO/PositiveInteger';

describe('Operation', () => {
  test('GetRealValue in SELL operation is positive', () => {
    const unitPrice = new PositiveInteger(5);
    const quantity = new PositiveInteger(10);
    const operation = new Operation('sell', unitPrice, quantity);

    expect(operation.getRealValue()).toBeGreaterThan(0);
    expect(operation.getRealValue()).toBe(unitPrice.number * quantity.number);
  });
  test('GetRealValue in BUY operation is negative', () => {
    const unitPrice = new PositiveInteger(5);
    const quantity = new PositiveInteger(10);
    const operation = new Operation('buy', unitPrice, quantity);

    expect(operation.getRealValue()).toBeLessThan(0);
    expect(operation.getRealValue()).toBe(unitPrice.number * quantity.number * -1);
  });
  test('GetRealQuantity in SELL operation is positive', () => {
    const unitPrice = new PositiveInteger(5);
    const quantity = new PositiveInteger(10);
    const operation = new Operation('sell', unitPrice, quantity);

    expect(operation.getRealQuantity()).toBeLessThan(0);
    expect(operation.getRealQuantity()).toBe(quantity.number * -1);
  });
  test('GetRealQuantity in BUY operation is negative', () => {
    const unitPrice = new PositiveInteger(5);
    const quantity = new PositiveInteger(10);
    const operation = new Operation('buy', unitPrice, quantity);

    expect(operation.getRealQuantity()).toBeGreaterThan(0);
    expect(operation.getRealQuantity()).toBe(quantity.number);
  });
});
