import { Investment } from '../../../../src/Modules/Wallet/Domain/Investment';
import { Operation } from '../../../../src/Modules/Wallet/Domain/Operation';
import { PositiveInteger } from '../../../../src/Modules/Wallet/Domain/VO/PositiveInteger';
import { SellOverState } from '../../../../src/Modules/Wallet/Domain/Exceptions';

describe('Investment', () => {
  test('Created investment has no operations', () => {
    const investment = new Investment('An investment');
    expect(investment.operations).toHaveLength(0);
    expect(investment.totalUnits).toBe(0);
    expect(investment.totalValue).toBe(0);
  });
  test('addOperation adds BUY operation and updates summary', () => {
    const investment = new Investment('An investment');
    const unitsQuantity = 15;
    const valuePerUnit = 1023;
    const operation = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));

    investment.addOperation(operation);

    expect(investment.operations).toContain(operation);
    expect(investment.totalValue).toBe(-15345);
    expect(investment.totalUnits).toBe(15);
  });
  test('addOperation assigns ID to the operation', () => {
    const investment = new Investment('An investment');
    const unitsQuantity = 15;
    const valuePerUnit = 1023;
    const operation1 = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));
    expect(operation1.id).toBe(undefined);
    investment.addOperation(operation1);
    expect(operation1.id).toBe(1);

    const operation2 = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));
    expect(operation2.id).toBe(undefined);
    investment.addOperation(operation2);
    expect(operation2.id).toBe(2);

    const operation3 = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));
    expect(operation3.id).toBe(undefined);
    investment.addOperation(operation3);
    expect(operation3.id).toBe(3);
  });
  test('addOperation assigns ID to the operation after removing', () => {
    const investment = new Investment('An investment');
    const unitsQuantity = 15;
    const valuePerUnit = 1023;
    const operation1 = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));
    expect(operation1.id).toBe(undefined);
    investment.addOperation(operation1);
    expect(operation1.id).toBe(1);

    const operation2 = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));
    expect(operation2.id).toBe(undefined);
    investment.addOperation(operation2);
    expect(operation2.id).toBe(2);

    const operation3 = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));
    expect(operation3.id).toBe(undefined);
    investment.addOperation(operation3);
    expect(operation3.id).toBe(3);

    investment.removeOperation(2);
    const operation4 = new Operation('buy', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));
    expect(operation4.id).toBe(undefined);
    investment.addOperation(operation4);
    expect(operation4.id).toBe(4);
  });
  test('addOperation adds SELL operation and throws exception because of sell over a current state', () => {
    const investment = new Investment('An investment');
    const unitsQuantity = 15;
    const valuePerUnit = 1023;
    const operation = new Operation('sell', new PositiveInteger(valuePerUnit), new PositiveInteger(unitsQuantity));

    expect(() => investment.addOperation(operation)).toThrow(SellOverState);
  });
  test('addOperation adds BUY/SELL operations', () => {
    const investment = new Investment('An investment');
    const operation1 = new Operation('buy', new PositiveInteger(1025), new PositiveInteger(100)); // -102500
    const operation2 = new Operation('buy', new PositiveInteger(1050), new PositiveInteger(50)); // -52500
    const operation3 = new Operation('sell', new PositiveInteger(1000), new PositiveInteger(75)); // 75000
    const operation4 = new Operation('sell', new PositiveInteger(1100), new PositiveInteger(70)); // 77000

    investment.addOperation(operation1);
    investment.addOperation(operation2);
    investment.addOperation(operation3);
    investment.addOperation(operation4);

    expect(investment.totalValue).toBe(-3000);
    expect(investment.totalUnits).toBe(5);
    expect(investment.getProfitableFrom()).toBe(600);
  });
  test('addOperation adds BUY/SELL operations and calculates correct getProfitableFrom #2', () => {
    const investment = new Investment('An investment');
    const operation1 = new Operation('buy', new PositiveInteger(1025), new PositiveInteger(100)); // -102500
    const operation2 = new Operation('buy', new PositiveInteger(1050), new PositiveInteger(50)); // -52500
    const operation3 = new Operation('sell', new PositiveInteger(1000), new PositiveInteger(75)); // 75000

    investment.addOperation(operation1);
    investment.addOperation(operation2);
    investment.addOperation(operation3);

    expect(investment.totalValue).toBe(-80000);
    expect(investment.totalUnits).toBe(75);
    expect(investment.getProfitableFrom()).toBe(1066.7);
  });
  test('addOperation adds SELL operation and throws exception because of sell over a current state #2', () => {
    const investment = new Investment('An investment');
    const operation1 = new Operation('buy', new PositiveInteger(1025), new PositiveInteger(100)); // -1025
    const operation2 = new Operation('buy', new PositiveInteger(1050), new PositiveInteger(50)); // -525
    const operation3 = new Operation('sell', new PositiveInteger(1000), new PositiveInteger(151));

    investment.addOperation(operation1);
    investment.addOperation(operation2);
    expect(() => investment.addOperation(operation3)).toThrow(SellOverState);
  });
  test('getProfitableFrom with no free units and with a loss returns Infinity', () => {
    const investment = new Investment('An investment');
    const operation1 = new Operation('buy', new PositiveInteger(1025), new PositiveInteger(100));
    const operation2 = new Operation('sell', new PositiveInteger(1020), new PositiveInteger(100));

    investment.addOperation(operation1);
    investment.addOperation(operation2);

    expect(investment.totalValue).toBe(-500);
    expect(investment.totalUnits).toBe(0);
    expect(investment.getProfitableFrom()).toBe(Infinity);
  });
  test('getProfitableFrom with a profit returns 0', () => {
    const investment = new Investment('An investment');
    const operation1 = new Operation('buy', new PositiveInteger(1025), new PositiveInteger(100));
    const operation2 = new Operation('sell', new PositiveInteger(2051), new PositiveInteger(50));

    investment.addOperation(operation1);
    investment.addOperation(operation2);

    expect(investment.totalValue).toBe(50);
    expect(investment.totalUnits).toBe(50);
    expect(investment.getProfitableFrom()).toBe(0);
  });
  test('removeOperation recalculates summary and getProfitableFrom', () => {
    const investment = new Investment('An investment');
    const operation1 = new Operation('buy', new PositiveInteger(1025), new PositiveInteger(100));
    const operation2 = new Operation('sell', new PositiveInteger(1020), new PositiveInteger(100));

    investment.addOperation(operation1);
    investment.addOperation(operation2);

    expect(investment.totalValue).toBe(-500);
    expect(investment.totalUnits).toBe(0);
    expect(investment.getProfitableFrom()).toBe(Infinity);

    expect(operation1.id).toBe(1);
    expect(operation2.id).toBe(2);
    investment.removeOperation(2);

    expect(investment.totalValue).toBe(-102500);
    expect(investment.totalUnits).toBe(100);
    expect(investment.getProfitableFrom()).toBe(1025);
  });
});
