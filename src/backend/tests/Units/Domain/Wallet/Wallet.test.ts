import { Wallet } from '../../../../src/Modules/Wallet/Domain/Wallet';

describe('Wallet', () => {
  test('Created wallet has no investments', () => {
    const wallet = new Wallet('1', '1');
    expect(wallet.investments).toHaveLength(0);
  });
});
