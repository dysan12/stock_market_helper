test('try', () => {
  expect(3).toBe(3);
  expect(t(5)).toBe(6);
});

function t(number: number): number {
  return number + 1;
}
