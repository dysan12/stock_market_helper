import { Response } from 'supertest';
import { request } from '../Request';

describe('Get single Company data', () => {
  it('Get single Company data', async () => {
    const companyUid = 'PLAGORA00067';
    const res: Response = await request.get(`/companies/${companyUid}`);
    expect(res.body).toHaveProperty('uid');
    expect(res.body).toHaveProperty('name');
    expect(res.body).toHaveProperty('rateChange');
    expect(res.body).toHaveProperty('buyOffer');
    expect(res.body).toHaveProperty('sellOffer');
    expect(res.body).toHaveProperty('minRate');
    expect(res.body).toHaveProperty('maxRate');
    expect(res.body).toHaveProperty('exchangeVolume');
    expect(res.body).toHaveProperty('exchangeValue');
    expect(res.body).toHaveProperty('timestamp');
    expect(res.status).toEqual(200);
  });
  it('of not existing Company fails', async () => {
    const companyUid = 'nonExistentCompanyUid';
    const res: Response = await request.get(`/companies/${companyUid}`);
    expect(res.status).toEqual(404);
  });
});

describe('Get single Company indicators', () => {
  it('Get single Company indicators', async () => {
    const companyUid = 'PLAGORA00067';
    const res: Response = await request.get(`/companies/${companyUid}/indicators`);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('rsi');
    expect(res.body.data).toHaveProperty('slowSts');
    expect(res.body.data).toHaveProperty('fastSts');
    expect(res.body.data).toHaveProperty('macd');
    expect(res.body.data).toHaveProperty('trix');
    expect(res.body.data).toHaveProperty('sma5');
    expect(res.body.data).toHaveProperty('sma15');
    expect(res.body.data).toHaveProperty('sma30');
    expect(res.body.data).toHaveProperty('sma60');
    expect(res.body.data).toHaveProperty('ema5');
    expect(res.body.data).toHaveProperty('ema15');
    expect(res.body.data).toHaveProperty('ema30');
    expect(res.body.data).toHaveProperty('ema60');
    expect(res.status).toEqual(200);
  });
  it('of not existing Company fails', async () => {
    const companyUid = 'nonExistentCompanyUid';
    const res: Response = await request.get(`/companies/${companyUid}/indicators`);
    expect(res.status).toEqual(400);
  });
});
