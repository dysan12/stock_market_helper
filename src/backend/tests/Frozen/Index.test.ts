import { Response } from 'supertest';
import { forEach } from 'async';
import { request } from '../Request';

describe('Get all Indexes data', () => {
  it('Get all Indexes data', async () => {
    const res: Response = await request.get('/indexes');
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toHaveProperty('wig');
    expect(res.body.data).toHaveProperty('wig20');
    expect(res.body.data).toHaveProperty('wig30');
    const { wig, wig20, wig30 } = res.body.data;
    expect(wig).toHaveProperty('uid');
    expect(wig).toHaveProperty('name');
    expect(wig).toHaveProperty('rateChange');
    expect(wig).toHaveProperty('rate');
    expect(wig).toHaveProperty('openingRate');
    expect(wig).toHaveProperty('minRate');
    expect(wig).toHaveProperty('maxRate');
    expect(wig).toHaveProperty('closingRate');
    expect(wig).toHaveProperty('timestamp');
    expect(wig).toHaveProperty('companies');
    const wigCompanies = wig.companies;
    expect(wigCompanies.length).toBeGreaterThanOrEqual(300);
    forEach(wigCompanies, (c: any) => {
      expect(c).toHaveProperty('uid');
      expect(c).toHaveProperty('name');
      expect(c).toHaveProperty('packet');
      expect(c).toHaveProperty('packetPln');
      expect(c).toHaveProperty('walletSharePercentage');
      expect(c).toHaveProperty('sessionStockExchangeSharePercentage');
      expect(c).toHaveProperty('averageSessionSpread');
      expect(c).toHaveProperty('timestamp');
    });
    expect(wig20).toHaveProperty('uid');
    expect(wig20).toHaveProperty('name');
    expect(wig20).toHaveProperty('rateChange');
    expect(wig20).toHaveProperty('rate');
    expect(wig20).toHaveProperty('openingRate');
    expect(wig20).toHaveProperty('minRate');
    expect(wig20).toHaveProperty('maxRate');
    expect(wig20).toHaveProperty('closingRate');
    expect(wig20).toHaveProperty('timestamp');
    expect(wig20).toHaveProperty('companies');
    const wig20Companies = wig20.companies;
    expect(wig20Companies).toHaveLength(20);
    forEach(wig20Companies, (c: any) => {
      expect(c).toHaveProperty('uid');
      expect(c).toHaveProperty('name');
      expect(c).toHaveProperty('packet');
      expect(c).toHaveProperty('packetPln');
      expect(c).toHaveProperty('walletSharePercentage');
      expect(c).toHaveProperty('sessionStockExchangeSharePercentage');
      expect(c).toHaveProperty('averageSessionSpread');
      expect(c).toHaveProperty('timestamp');
    });
    expect(wig30).toHaveProperty('uid');
    expect(wig30).toHaveProperty('name');
    expect(wig30).toHaveProperty('rateChange');
    expect(wig30).toHaveProperty('rate');
    expect(wig30).toHaveProperty('openingRate');
    expect(wig30).toHaveProperty('minRate');
    expect(wig30).toHaveProperty('maxRate');
    expect(wig30).toHaveProperty('closingRate');
    expect(wig30).toHaveProperty('timestamp');
    expect(wig30).toHaveProperty('companies');
    const wig30Companies = wig30.companies;
    expect(wig30Companies).toHaveLength(30);
    forEach(wig30Companies, (c: any) => {
      expect(c).toHaveProperty('uid');
      expect(c).toHaveProperty('name');
      expect(c).toHaveProperty('packet');
      expect(c).toHaveProperty('packetPln');
      expect(c).toHaveProperty('walletSharePercentage');
      expect(c).toHaveProperty('sessionStockExchangeSharePercentage');
      expect(c).toHaveProperty('averageSessionSpread');
      expect(c).toHaveProperty('timestamp');
    });
    expect(res.status).toEqual(200);
  });
});

describe('Get single Index data', () => {
  it('Get single Index data', async () => {
    const indexUid = 'PL9999999995';
    const res: Response = await request.get(`/indexes/${indexUid}`);
    expect(res.body).toHaveProperty('uid');
    expect(res.body).toHaveProperty('name');
    expect(res.body).toHaveProperty('rateChange');
    expect(res.body).toHaveProperty('rate');
    expect(res.body).toHaveProperty('openingRate');
    expect(res.body).toHaveProperty('minRate');
    expect(res.body).toHaveProperty('maxRate');
    expect(res.body).toHaveProperty('closingRate');
    expect(res.body).toHaveProperty('timestamp');
    expect(res.body).toHaveProperty('companies');
    const { companies } = res.body;
    expect(companies.length).toBeGreaterThanOrEqual(300);
    forEach(companies, (c: any) => {
      expect(c).toHaveProperty('uid');
      expect(c).toHaveProperty('name');
      expect(c).toHaveProperty('packet');
      expect(c).toHaveProperty('packetPln');
      expect(c).toHaveProperty('walletSharePercentage');
      expect(c).toHaveProperty('sessionStockExchangeSharePercentage');
      expect(c).toHaveProperty('averageSessionSpread');
      expect(c).toHaveProperty('timestamp');
    });
    expect(res.status).toEqual(200);
  });
  it('of not existing Index fails', async () => {
    const indexUid = 'nonExistentIndexUid';
    const res: Response = await request.get(`/indexes/${indexUid}`);
    expect(res.status).toEqual(404);
  });
});
