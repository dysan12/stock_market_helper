import { Response } from 'supertest';
import { forEach } from 'async';
import { request } from '../Request';
import { authorizeAsNewUser, createInvestment } from '../Helpers';
import { CreateOperation } from '../../src/Modules/Wallet/Domain/Data/CreateOperation';

describe('Create Investment', () => {
  it('Create Investment', async () => {
    await authorizeAsNewUser();
    const res: Response = await request.post('/wallets/3/investments').send({ name: 'My first investment' });
    expect(res.body).toHaveProperty('id');
    expect(res.status).toEqual(201);
  });
  it('Create 2 Investments for one company', async () => {
    await authorizeAsNewUser();
    const res: Response = await request.post('/wallets/3/investments').send({ name: 'My first investment' });
    expect(res.body).toHaveProperty('id');
    expect(res.status).toEqual(201);
    const res2: Response = await request.post('/wallets/3/investments').send({ name: 'My first investment 2' });
    expect(res2.body).toHaveProperty('id');
    expect(res2.status).toEqual(201);
  });
  it('Get all user wallets', async () => {
    await authorizeAsNewUser();
    const compId1 = '1';
    const inv1 = await createInvestment(compId1, { name: 'My investment #1' });
    const compId2 = '2';
    const inv2 = await createInvestment(compId2, { name: 'My investment #2' });
    const inv3 = await createInvestment(compId2, { name: 'My investment #3' });
    const res: Response = await request.get('/wallets');
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('data');
    expect(res.body.data).toContainEqual({
      companyId: compId1,
      investments: [inv1.id],
    });
    expect(res.body.data).toContainEqual({
      companyId: compId2,
      investments: [inv2.id, inv3.id],
    });
  });
  it('Get user wallet for a company', async () => {
    await authorizeAsNewUser();
    const compId1 = '1';
    await createInvestment(compId1, { name: 'My investment #1' });
    await createInvestment(compId1, { name: 'My investment #2' });

    const res: Response = await request.get(`/wallets/${compId1}`);
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('companyId');
    expect(res.body).toHaveProperty('investments');
    const { investments } = res.body;
    expect(investments).toHaveLength(2);
    forEach(investments, (i: any) => {
      expect(i).toHaveProperty('id');
      expect(i).toHaveProperty('name');
      expect(i).toHaveProperty('totalValue');
      expect(i).toHaveProperty('totalUnits');
      expect(i).toHaveProperty('operations');
      forEach(i.operations, (o: any) => {
        expect(o).toHaveProperty('id');
        expect(o).toHaveProperty('type');
        expect(o).toHaveProperty('unitPrice');
        expect(o).toHaveProperty('quantity');
        expect(o).toHaveProperty('madeAt');
      });
    });
  });
  it('Get user wallet for a company without investments', async () => {
    await authorizeAsNewUser();
    const compId1 = '1';

    const res: Response = await request.get(`/wallets/${compId1}`);
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('companyId');
    expect(res.body).toHaveProperty('investments');
    expect(res.body.investments).toHaveLength(0);
  });
  it('Add operation to the investment', async () => {
    await authorizeAsNewUser();
    const compId1 = '1';
    const inv1 = await createInvestment(compId1, { name: 'My investment #1' });
    const operationData1: CreateOperation.Interface = { type: 'buy', quantity: 10, unitPrice: 1000 };
    const res: Response = await request.post(`/investments/${inv1.id}/operations`).send(operationData1);
    expect(res.status).toEqual(201);
    expect(res.body).toHaveProperty('id');
  });
  it('Add operation to a not existing investment respond with 404', async () => {
    await authorizeAsNewUser();
    const operationData1: CreateOperation.Interface = { type: 'buy', quantity: 10, unitPrice: 1000 };
    const res: Response = await request.post('/investments/qqqq-wwww-eeee-rrrr/operations').send(operationData1);
    expect(res.status).toEqual(404);
  });
  it('Add operation without required data respond with 400', async () => {
    await authorizeAsNewUser();
    const compId1 = '1';
    const inv1 = await createInvestment(compId1, { name: 'My investment #1' });
    const res: Response = await request.post(`/investments/${inv1.id}/operations`).send({ type: 'kaszanka' });
    expect(res.status).toEqual(400);
  });
  it('Add sell operation which would reduce state below 0 respond with 400', async () => {
    await authorizeAsNewUser();
    const compId1 = '1';
    const inv1 = await createInvestment(compId1, { name: 'My investment #1' });
    const res: Response = await request
      .post(`/investments/${inv1.id}/operations`)
      .send({ type: 'buy', quantity: 10, unitPrice: 1000 });
    expect(res.status).toEqual(201);
    const res2: Response = await request
      .post(`/investments/${inv1.id}/operations`)
      .send({ type: 'sell', quantity: 11, unitPrice: 1000 });
    expect(res2.status).toEqual(400);
    expect(res2.body).toContainEqual(expect.objectContaining({ code: '082' }));
  });
  it("Add operation to other user's investment respond with 403", async () => {
    await authorizeAsNewUser();
    const compId1 = '1';
    const inv1 = await createInvestment(compId1, { name: 'My investment #1' });
    const operationData1: CreateOperation.Interface = { type: 'buy', quantity: 10, unitPrice: 1000 };
    const res: Response = await request.post(`/investments/${inv1.id}/operations`).send(operationData1);
    expect(res.status).toEqual(201);

    await authorizeAsNewUser();
    const res2: Response = await request.post(`/investments/${inv1.id}/operations`).send(operationData1);
    expect(res2.status).toEqual(403);
  });
  it('Add operations to the investment and get user wallet for a company', async () => {
    await authorizeAsNewUser();
    const compId1 = '1';
    const inv1 = await createInvestment(compId1, { name: 'My investment #1' });
    const operationData1: CreateOperation.Interface = { type: 'buy', quantity: 10, unitPrice: 1000 };
    const res: Response = await request.post(`/investments/${inv1.id}/operations`).send(operationData1);
    expect(res.status).toEqual(201);
    const opId1 = res.body.id;
    const operationData2: CreateOperation.Interface = { type: 'sell', quantity: 5, unitPrice: 1000 };
    const res2: Response = await request.post(`/investments/${inv1.id}/operations`).send(operationData2);
    expect(res2.status).toEqual(201);
    const opId2 = res2.body.id;

    const res3: Response = await request.get(`/wallets/${compId1}`);
    expect(res3.status).toEqual(200);
    const { investments } = res3.body;
    expect(investments).toHaveLength(1);

    const inv = investments.filter((i: any) => i.id === inv1.id);
    expect(inv).toHaveLength(1);

    expect(inv[0].operations).toContainEqual(expect.objectContaining({ id: opId1, ...operationData1 }));
    expect(inv[0].operations).toContainEqual(expect.objectContaining({ id: opId2, ...operationData2 }));
  });
  it('Remove operation from the investment', async () => {
    await authorizeAsNewUser();
    const campId1 = '1';
    const inv1 = await createInvestment(campId1, { name: 'Investment #1' });
    const res: Response = await request
      .post(`/investments/${inv1.id}/operations`)
      .send({ type: 'buy', quantity: 10, unitPrice: 1000 });
    expect(res.status).toBe(201);
    const opId1 = res.body.id;

    const res2: Response = await request.get(`/wallets/${campId1}`);
    expect(res2.status).toEqual(200);
    expect(res2.body.investments).toHaveLength(1);
    expect(res2.body.investments[0].operations).toContainEqual(expect.objectContaining({ id: opId1 }));

    const res3: Response = await request.delete(`/investments/${inv1.id}/operations/${opId1}`);
    expect(res3.status).toBe(204);

    const res4: Response = await request.get(`/wallets/${campId1}`);
    expect(res4.status).toEqual(200);
    expect(res4.body.investments).toHaveLength(1);
    expect(res4.body.investments[0].operations).toHaveLength(0);
  });
  it('Remove operation from the investment which would cause violation of the unit state respond with 400', async () => {
    await authorizeAsNewUser();
    const campId1 = '1';
    const inv1 = await createInvestment(campId1, { name: 'Investment #1' });
    const res: Response = await request
      .post(`/investments/${inv1.id}/operations`)
      .send({ type: 'buy', quantity: 10, unitPrice: 1000 });
    expect(res.status).toBe(201);
    const opId1 = res.body.id;
    const res2: Response = await request
      .post(`/investments/${inv1.id}/operations`)
      .send({ type: 'sell', quantity: 10, unitPrice: 1000 });
    expect(res2.status).toBe(201);

    const res3: Response = await request.delete(`/investments/${inv1.id}/operations/${opId1}`);
    expect(res3.status).toBe(400);
    expect(res3.body).toContainEqual(expect.objectContaining({ code: '082' }));
  });
  it("Remove operation from other user's investment respond with 403", async () => {
    await authorizeAsNewUser();
    const campId1 = '1';
    const inv1 = await createInvestment(campId1, { name: 'Investment #1' });
    const res: Response = await request
      .post(`/investments/${inv1.id}/operations`)
      .send({ type: 'buy', quantity: 10, unitPrice: 1000 });
    expect(res.status).toBe(201);
    const opId1 = res.body.id;

    await authorizeAsNewUser();
    const res3: Response = await request.delete(`/investments/${inv1.id}/operations/${opId1}`);
    expect(res3.status).toBe(403);
  });
  it('Remove operation a not existing investment respond with 404', async () => {
    await authorizeAsNewUser();
    const res3: Response = await request.delete('/investments/qqq-www-eee-rrr/operations/1');
    expect(res3.status).toBe(404);
  });
});

afterEach(() => {
  request.removeToken();
});
