import { Response } from 'supertest';
import { request } from '../Request';
import { createUser, getToken } from '../Helpers';
describe('Register User', () => {
  it('Register User', async () => {
    const res: Response = await request.post('/users').send({
      name: 'John',
      login: Math.random().toString(),
      password: 'qweasdzxc',
    });
    expect(res.status).toEqual(201);
    expect(res.body).toHaveProperty('id');
  });
  it('with too short password fails', async () => {
    const res: Response = await request.post('/users').send({
      name: 'John',
      login: Math.random().toString(),
      password: 'qw',
    });
    expect(res.status).toEqual(400);
  });
  it('with occupied login fails', async () => {
    const login = Math.random().toString();
    const res: Response = await request.post('/users').send({
      name: 'John',
      login,
      password: 'qweasdzxc',
    });
    expect(res.status).toEqual(201);
    const res2: Response = await request.post('/users').send({
      name: 'John',
      login,
      password: 'qweasdzxc',
    });
    expect(res2.status).toEqual(400);
  });
});

describe('Log in', () => {
  it('Log in', async () => {
    const user = {
      name: 'John',
      login: Math.random().toString(),
      password: 'qweasdzxc',
    };
    const res: Response = await request.post('/users').send(user);
    expect(res.status).toEqual(201);
    const res2: Response = await request.post('/tokens').send({ login: user.login, password: user.password });
    expect(res2.status).toEqual(201);
    expect(res2.body).toHaveProperty('token');
  });
  it('with lacking data fails', async () => {
    const res: Response = await request.post('/tokens').send({ login: 'qwe' });
    expect(res.status).toEqual(400);
  });
  it('with not existing user fails', async () => {
    const res: Response = await request
      .post('/tokens')
      .send({ login: Math.random().toString(), password: 'qweasdzxc' });
    expect(res.status).toEqual(400);
    expect(res.body).toContainEqual(expect.objectContaining({ code: '003' }));
  });
  it("with incorrect user's password", async () => {
    const user = {
      name: 'John',
      login: Math.random().toString(),
      password: 'qweasdzxc',
    };
    const res: Response = await request.post('/users').send(user);
    expect(res.status).toEqual(201);
    const res2: Response = await request.post('/tokens').send({ login: user.login, password: `${user.password}qwe` });
    expect(res2.status).toEqual(400);
    expect(res2.body).toContainEqual(expect.objectContaining({ code: '004' }));
  });
});

describe('Get User details', () => {
  it('Get User details', async () => {
    const user = await createUser();
    const token = await getToken(user.login, user.password);
    const res: Response = await request.setToken(token).get(`/users/${user.id}`);
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('id');
    expect(res.body).toHaveProperty('login');
    expect(res.body).toHaveProperty('name');
    expect(res.body).not.toHaveProperty('password');
  });
  it('of not existing User fails', async () => {
    const user = await createUser();
    const token = await getToken(user.login, user.password);
    const res: Response = await request.setToken(token).get('/users/1234');
    expect(res.status).toEqual(404);
  });
  it('without sending a token fails', async () => {
    const user = await createUser();
    // console.log(request);
    const res: Response = await request.get(`/users/${user.id}`);
    expect(res.status).toEqual(401);
  });
  it("with other user's token fails", async () => {
    const user = await createUser();
    const user2 = await createUser();
    const token = await getToken(user.login, user.password);
    // request.delete
    const res: Response = await request.setToken(token).get(`/users/${user2.id}`);
    expect(res.status).toEqual(403);
  });
});

describe('Change User password', () => {
  it('Change User password', async () => {
    const user = await createUser();
    const token = await getToken(user.login, user.password);
    const newPassword = 'zxcasdqwe';
    const res: Response = await request
      .setToken(token)
      .put(`/users/${user.id}/password`)
      .send({ oldPassword: user.password, newPassword });
    expect(res.status).toEqual(204);
    const token2 = await getToken(user.login, newPassword); // if the token is returned the password was changed
  });
  it('of not existing User fails', async () => {
    const user = await createUser();
    const token = await getToken(user.login, user.password);
    const newPassword = 'zxcasdqwe';
    const res: Response = await request
      .setToken(token)
      .put(`/users/${Math.random()}/password`)
      .send({ oldPassword: user.password, newPassword });
    expect(res.status).toEqual(404);
  });
  it("with other user's token fails", async () => {
    const user = await createUser();
    const user2 = await createUser();
    const token = await getToken(user.login, user.password);
    const newPassword = 'zxcasdqwe';
    const res: Response = await request
      .setToken(token)
      .put(`/users/${user2.id}/password`)
      .send({ oldPassword: user.password, newPassword });
    expect(res.status).toEqual(403);
  });
  it('with not matching old password', async () => {
    const user = await createUser();
    const token = await getToken(user.login, user.password);
    const newPassword = 'zxcasdqwe';
    const res: Response = await request
      .setToken(token)
      .put(`/users/${user.id}/password`)
      .send({ oldPassword: `${user.password}xxx`, newPassword });
    expect(res.status).toEqual(400);
    expect(res.body).toContainEqual(expect.objectContaining({ code: '001' }));
  });
});

describe('Update User details', () => {
  it('Update User details', async () => {
    const user = await createUser({ name: 'Alfons' });
    const token = await getToken(user.login, user.password);
    const newData = {
      name: 'Ziutek',
    };
    const res: Response = await request
      .setToken(token)
      .put(`/users/${user.id}`)
      .send(newData);
    expect(res.status).toEqual(204);
    const res2: Response = await request.get(`/users/${user.id}`);
    expect(res2.status).toEqual(200);
    expect(res2.body).toMatchObject(newData);
  });
  it('of not existing User fails', async () => {
    const user = await createUser({ name: 'Alfons' });
    const token = await getToken(user.login, user.password);
    const newData = {
      name: 'Ziutek',
    };
    const res: Response = await request
      .setToken(token)
      .put(`/users/${Math.random()}`)
      .send(newData);
    expect(res.status).toEqual(404);
  });
  it("with other user's token fails", async () => {
    const user = await createUser({ name: 'Alfons' });
    const user2 = await createUser();
    const token = await getToken(user2.login, user2.password);
    const newData = {
      name: 'Ziutek',
    };
    const res: Response = await request
      .setToken(token)
      .put(`/users/${user.id}`)
      .send(newData);
    expect(res.status).toEqual(403);
  });
});

afterEach(() => {
  request.removeToken();
});
