import { agent, SuperTest, Test, Response } from 'supertest';
import { env } from '../src/Lib/Infrastructure/Env';

// @ts-ignore // extension is added in the code below
export const request: SuperTest<Test> & ExtensionInterface = agent(env('APP_URL'));
type setTokenFn = (token: string) => SuperTest<Test>;
type removeTokenFn = () => SuperTest<Test>;
interface ExtensionInterface {
  setToken: setTokenFn;
  removeToken: removeTokenFn;
}
const extension: ExtensionInterface = {
  setToken(token: string) {
    this.auth(token, { type: 'bearer' });
    return this;
  },
  removeToken() {
    this.set('Authorization', null);
    return this;
  },
};
Object.assign(request, extension);
