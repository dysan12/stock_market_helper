module.exports = {
  apps: [
    {
      name: 'API',
      script: 'src/App.ts',
      autorestart: true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
      watch: true,
      ignore_watch: ['logs', 'node_modules', 'dist'],
      error_file: 'dev/null',
      out_file: 'dev/null',
      log_file: 'logs/combined.log',
    },
  ],
};
