import { VoterInterface } from '../../../../Lib/Application/VoterRegistry';
import { Investment } from '../Investment';
import { TokenPayload } from '../../../../Lib/Common/Token';
import { WalletRepository } from '../../Infrastructure/WalletRepository';
import { Logger } from '../../../../Lib/Infrastructure/Logger';

class Voter implements VoterInterface {
  supports(obj: any): obj is Investment {
    return obj instanceof Investment;
  }

  async vote(obj: any, action: string, token: TokenPayload): Promise<boolean> {
    if (this.supports(obj) === false) {
      return false;
    }
    const investment: Investment = obj;
    const wallet = await WalletRepository.findOneBy({ investments: { $all: [investment.getId()] } });
    if (wallet === null) {
      Logger.warn(`There is investment which is not assigned to any of wallet! ${investment.getId()}`);
      return false;
    }
    return wallet.ownerId === token.user.id;
  }
}

export const InvestmentVoter = new Voter();
