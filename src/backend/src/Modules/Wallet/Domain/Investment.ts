import { Operation } from './Operation';
import { SellOverState } from './Exceptions';
import { Entity } from '../../../Lib/Domain/Entity';

export class Investment extends Entity {
  private _name: string;
  private _totalValue: number;
  private _totalUnits: number;
  private _operations: Operation[];
  private _createdAt: number;

  constructor(name: string) {
    super();
    this._name = name;
    this._operations = [];
    this._totalValue = 0;
    this._totalUnits = 0;
    this._createdAt = Math.round(new Date().getTime() / 1000);
  }

  get name(): string {
    return this._name;
  }

  get totalValue(): number {
    return this._totalValue;
  }

  get totalUnits(): number {
    return this._totalUnits;
  }

  get operations(): Operation[] {
    return this._operations;
  }

  public addOperation(operation: Operation): void {
    operation.id = this.getNextOperationId();
    const operations = [...this._operations, operation];
    const summary = this.calculateSummary(operations);
    if (summary.totalUnits < 0) {
      throw new SellOverState('Cannot add the operation because it would cause a negative total number of units');
    }
    this._operations = operations;
    this.applySummary(summary);
  }

  public removeOperation(id: number): void {
    const operations = this._operations.filter(op => op.id !== id);
    const summary = this.calculateSummary(operations);
    if (summary.totalUnits < 0) {
      throw new SellOverState('Cannot remove the operation because it would cause a negative total number of units');
    }
    this._operations = this._operations.filter(op => op.id !== id);
    this.applySummary(this.calculateSummary(this._operations));
  }

  public getProfitableFrom(): number {
    if (this._totalValue >= 0) {
      return 0;
    }
    if (this._totalUnits === 0) {
      return Infinity;
    }
    return Math.round(-1 * (this._totalValue / this._totalUnits) * 10) / 10;
  }

  private calculateSummary(operations: Operation[]): Summary {
    return {
      totalValue: operations.reduce((val, op) => val + op.getRealValue(), 0),
      totalUnits: operations.reduce((val, op) => val + op.getRealQuantity(), 0),
    };
  }

  private applySummary(sum: Summary): void {
    this._totalUnits = sum.totalUnits;
    this._totalValue = sum.totalValue;
  }

  private getNextOperationId(): number {
    if (this.operations.length === 0) {
      return 1;
    }
    let newId = this.operations[this.operations.length - 1].id + 1;
    const isIdOccupied = (id: number): boolean => this.operations.filter(o => o.id === id).length > 0;
    if (isIdOccupied(newId)) {
      newId = Math.floor(Math.random() * 1000);
    }
    if (isIdOccupied(newId)) {
      return this.getNextOperationId();
    }
    return newId;
  }
}

interface Summary {
  totalValue: number;
  totalUnits: number;
}
