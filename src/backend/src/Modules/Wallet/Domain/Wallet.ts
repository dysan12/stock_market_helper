import { Investment } from './Investment';
import { Entity } from '../../../Lib/Domain/Entity';

export class Wallet extends Entity {
  private _companyId: string;
  private _ownerId: string;
  private _investments: string[];
  private _createdAt: number;

  constructor(companyId: string, ownerId: string) {
    super();
    this._companyId = companyId;
    this._ownerId = ownerId;
    this._investments = [];
    this._createdAt = Math.round(new Date().getTime() / 1000);
  }

  get ownerId(): string {
    return this._ownerId;
  }

  public addInvestment(investment: Investment): void {
    if (this._investments.includes(investment.getId()) === true) {
      return;
    }
    this._investments.push(investment.getId());
  }

  get investments(): string[] {
    return this._investments;
  }

  get companyId(): string {
    return this._companyId;
  }
}
