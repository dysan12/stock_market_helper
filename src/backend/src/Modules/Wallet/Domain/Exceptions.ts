import { Exception } from '../../../Lib/Infrastructure/Exceptions';

export class InvalidPositiveNumber extends Exception {}
export class SellOverState extends Exception {}
