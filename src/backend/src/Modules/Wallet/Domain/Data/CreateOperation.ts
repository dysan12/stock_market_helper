export type OperationType = 'buy' | 'sell';
export namespace CreateOperation {
  export interface Interface {
    type: OperationType;
    unitPrice: number;
    quantity: number;
  }

  const within: OperationType[] = ['buy', 'sell'];
  export const constraints = {
    type: {
      type: 'string',
      presence: true,
      inclusion: {
        within,
        message: `An operation type can be one of ${JSON.stringify(within)}`,
      },
    },
    unitPrice: { type: 'number', presence: true, numericality: { onlyInteger: true, greaterThan: 0 } },
    quantity: { type: 'number', presence: true, numericality: { onlyInteger: true, greaterThan: 0 } },
  };
}
