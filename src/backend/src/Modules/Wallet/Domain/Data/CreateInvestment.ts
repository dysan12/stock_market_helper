export namespace CreateInvestmentData {
  export interface Interface {
    name: string;
  }

  export const constraints = {
    name: { length: { minimum: 3 }, presence: true },
  };
}
