import { PositiveInteger } from './VO/PositiveInteger';
import { RuntimeException } from '../../../Lib/Infrastructure/Exceptions';
import { CreateOperation, OperationType } from './Data/CreateOperation';
import { Nested } from '../../../Lib/Domain/Nested';

export class Operation extends Nested {
  private _id: number;
  private _type: OperationType;
  private _unitPrice: number;
  private _quantity: number;
  private _madeAt: number;

  constructor(type: OperationType, unitPrice: PositiveInteger, quantity: PositiveInteger) {
    super();
    this._quantity = quantity.number;
    this._unitPrice = unitPrice.number;
    this._type = type;
    this._madeAt = Math.round(new Date().getTime() / 1000);
  }

  get id(): number | undefined {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  public static create(data: CreateOperation.Interface): Operation {
    return new Operation(data.type, new PositiveInteger(data.unitPrice), new PositiveInteger(data.quantity));
  }

  get type(): OperationType {
    return this._type;
  }

  get unitPrice(): PositiveInteger {
    return new PositiveInteger(this._unitPrice);
  }

  get quantity(): PositiveInteger {
    return new PositiveInteger(this._quantity);
  }

  get madeAt(): number {
    return this._madeAt;
  }

  public getRealValue(): number {
    return this.getRealQuantity() * this._unitPrice * -1;
  }

  public getRealQuantity(): number {
    if (this._type === 'sell') {
      return -1 * this._quantity;
    }
    if (this._type === 'buy') {
      return this._quantity;
    }
    throw new RuntimeException(`Implement support for an operation type "${this._type}"`);
  }
}
