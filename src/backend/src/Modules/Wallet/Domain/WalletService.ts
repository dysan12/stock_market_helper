import { Investment } from './Investment';
import { InvestmentPersistence, InvestmentRepository } from '../Infrastructure/InvestmentRepository';
import { Logger } from '../../../Lib/Infrastructure/Logger';
import { ErrorCodes, Result, ResultInterface } from '../../../Lib/Application/Result';
import { Wallet } from './Wallet';
import { WalletPersistence, WalletRepository } from '../Infrastructure/WalletRepository';
import { CreateInvestmentData } from './Data/CreateInvestment';
import { Operation } from './Operation';
import { CreateOperation } from './Data/CreateOperation';
import { SellOverState } from './Exceptions';
import { validate } from '../../../Lib/UI/Validator';

class Service {
  public async createInvestment(
    companyId: string,
    data: CreateInvestmentData.Interface,
    userId: string,
  ): Promise<ResultInterface<{ id: string }>> {
    const result: Result = new Result();

    try {
      const investment = new Investment(data.name);
      result.data = { id: await InvestmentPersistence.save(investment) };

      let wallet = await WalletRepository.findOneBy({ ownerId: userId, companyId });
      if (wallet === null) {
        wallet = new Wallet(companyId, userId);
        await WalletPersistence.save(wallet);
      }
      wallet.addInvestment(investment);
      await WalletPersistence.save(wallet);
    } catch (e) {
      Logger.warn(e); // if recognized, add a proper handling
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: 'Unknown error',
      });
      return result;
    }

    result.isSuccessful = true;
    return result;
  }

  public async getWallet(companyId: string, userId: string): Promise<Result<Wallet>> {
    const result: Result = new Result();

    try {
      let wallet = await WalletRepository.findOneBy({ ownerId: userId, companyId });
      if (wallet === null) {
        wallet = new Wallet(companyId, userId);
        await WalletPersistence.save(wallet);
      }
      result.isSuccessful = true;
      result.data = wallet;
    } catch (e) {
      Logger.warn(e); // if recognized, add a proper handling
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: 'Unknown error',
      });
    }

    return result;
  }

  public async addInvestmentOperation(
    investmentId: string,
    data: CreateOperation.Interface,
  ): Promise<ResultInterface<{ id: number }>> {
    const violations = validate(data, CreateOperation.constraints);
    if (violations.length > 0) {
      return new Result(false, violations);
    }
    const result = new Result();

    const investment = await InvestmentRepository.find(investmentId);
    if (investment === null) {
      result.addError({
        code: ErrorCodes.INVESTMENT_NOT_FOUND,
        message: `Cannot find an Investment with ID ${investmentId}`,
      });
      return result;
    }

    const operation = Operation.create(data);
    try {
      investment.addOperation(operation);
    } catch (e) {
      if (e instanceof SellOverState) {
        result.addError({
          code: ErrorCodes.OPERATION_VIOLATES_STATE,
          message: 'Cannot sell units over acquired state',
        });
        return result;
      }
    }

    try {
      await InvestmentPersistence.save(investment);
      result.isSuccessful = true;
      result.data = { id: operation.id };
    } catch (e) {
      Logger.warn(e); // if recognized, add a proper handling
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: 'Unknown error',
      });
      return result;
    }

    return result;
  }

  public async removeInvestmentOperation(investmentId: string, operationId: number): Promise<ResultInterface> {
    const result = new Result();

    const investment = await InvestmentRepository.find(investmentId);
    if (investment === null) {
      result.addError({
        code: ErrorCodes.INVESTMENT_NOT_FOUND,
        message: `Cannot find an Investment with ID ${investmentId}`,
      });
      return result;
    }

    try {
      investment.removeOperation(operationId);
    } catch (e) {
      if (e instanceof SellOverState) {
        result.addError({
          code: ErrorCodes.OPERATION_VIOLATES_STATE,
          message: 'Cannot remove an operation that would violate the acquired state',
        });
        return result;
      }
    }

    try {
      await InvestmentPersistence.save(investment);
      result.isSuccessful = true;
    } catch (e) {
      Logger.warn(e); // if recognized, add a proper handling
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: 'Unknown error',
      });
      return result;
    }

    return result;
  }
}

export const WalletService = new Service();
