import { InvalidPositiveNumber } from '../Exceptions';

export class PositiveInteger {
  private _number: number;
  constructor(number: number) {
    if (number <= 0 || Number.isInteger(number) === false) {
      throw new InvalidPositiveNumber(`Number: "${number}" must be positive integer`);
    }
    this._number = number;
  }

  get number(): number {
    return this._number;
  }
}
