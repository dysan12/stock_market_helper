import {
  AbstractRepository,
  PersistenceInterface,
  RepositoryInterface,
} from '../../../Lib/Infrastructure/MongoDB/Repository';
import { Wallet } from '../Domain/Wallet';

class Repository extends AbstractRepository<Wallet> {
  protected getEntityClass(): { prototype: any; name: string } {
    return Wallet;
  }
}

const repo = new Repository();

export const WalletRepository: RepositoryInterface<Wallet> = repo;
export const WalletPersistence: PersistenceInterface<Wallet> = repo;
