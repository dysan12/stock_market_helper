import {
  AbstractRepository,
  PersistenceInterface,
  RepositoryInterface,
} from '../../../Lib/Infrastructure/MongoDB/Repository';
import { Investment } from '../Domain/Investment';

class Repository extends AbstractRepository<Investment> {
  protected getEntityClass(): { prototype: any; name: string } {
    return Investment;
  }
}

const repo = new Repository();

export const InvestmentRepository: RepositoryInterface<Investment> = repo;
export const InvestmentPersistence: PersistenceInterface<Investment> = repo;
