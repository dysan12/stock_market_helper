import { Wallet } from '../Domain/Wallet';
import { Investment } from '../Domain/Investment';

export function walletRespond(wallet: Wallet, investments: Investment[]): { [key: string]: any } {
  return {
    companyId: wallet.companyId,
    investments: investments.map(i => ({
      id: i.getId(),
      name: i.name,
      totalValue: i.totalValue,
      totalUnits: i.totalUnits,
      profitableFrom: i.getProfitableFrom(),
      operations: i.operations.map(o => ({
        id: o.id,
        type: o.type,
        unitPrice: o.unitPrice.number,
        quantity: o.quantity.number,
        madeAt: o.madeAt,
      })),
    })),
  };
}
export function walletsRespond(wallets: Wallet[]): { data: { [key: string]: any } } {
  return {
    data: wallets.map(w => ({
      companyId: w.companyId,
      investments: w.investments,
    })),
  };
}
