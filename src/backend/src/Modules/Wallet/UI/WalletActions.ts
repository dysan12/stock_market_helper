import { Express, Response } from 'express';
import { validateBody } from '../../../Lib/UI/Validator';
import { authorize, AuthorizedRequest } from '../../../Lib/UI/Authorization';
import { WalletService } from '../Domain/WalletService';
import { CreateInvestmentData } from '../Domain/Data/CreateInvestment';
import { WalletRepository } from '../Infrastructure/WalletRepository';
import { walletRespond, walletsRespond } from './Responders';
import { InvestmentRepository } from '../Infrastructure/InvestmentRepository';
import { CreateOperation } from '../Domain/Data/CreateOperation';
import { VoterRegistry } from '../../../Lib/Application/VoterRegistry';

export namespace WalletActions {
  export function registerRoutes(App: Express): void {
    App.post(
      '/wallets/:companyId/investments',
      [authorize, validateBody(CreateInvestmentData.constraints)],
      Handlers.createInvestment,
    );
    App.get('/wallets', [authorize], Handlers.getAllWallets);
    App.get('/wallets/:companyId', [authorize], Handlers.getWallet);
    App.post(
      '/investments/:investmentId/operations',
      [authorize, validateBody(CreateOperation.constraints)],
      Handlers.addOperation,
    );
    App.delete('/investments/:investmentId/operations/:operationId', [authorize], Handlers.removeOperation);
  }

  namespace Handlers {
    export async function createInvestment(
      req: AuthorizedRequest<{ companyId: string }>,
      res: Response,
    ): Promise<void> {
      const { companyId } = req.params;
      const { body } = req;

      const result = await WalletService.createInvestment(companyId, body, req.tokenPayload.user.id);
      if (result.isSuccessful === true) {
        res.status(201).json({ id: result.data.id });
      } else {
        res.status(400).json(result.errors);
      }
    }
    export async function getAllWallets(req: AuthorizedRequest, res: Response): Promise<void> {
      const userId = req.tokenPayload.user.id;

      const wallets = await WalletRepository.findBy({ ownerId: userId });
      res.json(walletsRespond(wallets));
    }

    export async function getWallet(req: AuthorizedRequest<{ companyId: string }>, res: Response): Promise<void> {
      const { companyId } = req.params;
      const userId = req.tokenPayload.user.id;

      const result = await WalletService.getWallet(companyId, userId);
      if (result.isSuccessful === false) {
        res.status(400).json();
        return;
      }

      const wallet = result.data;
      const investments = await InvestmentRepository.findByIds(wallet.investments);
      res.json(walletRespond(wallet, investments));
    }

    export async function addOperation(req: AuthorizedRequest<{ investmentId: string }>, res: Response): Promise<void> {
      const { investmentId } = req.params;
      const { body } = req;

      const investment = await InvestmentRepository.find(investmentId);
      if (investment === null) {
        res.status(404).json();
        return;
      }
      if ((await VoterRegistry.isAllowed(investment, 'addOperation', req.tokenPayload)) === false) {
        res.status(403).json();
        return;
      }
      const result = await WalletService.addInvestmentOperation(investmentId, body);
      if (result.isSuccessful === true) {
        res.status(201).json({ id: result.data.id });
      } else {
        res.status(400).json(result.errors);
      }
    }

    export async function removeOperation(
      req: AuthorizedRequest<{ investmentId: string; operationId: string }>,
      res: Response,
    ): Promise<void> {
      const { investmentId, operationId } = req.params;

      const investment = await InvestmentRepository.find(investmentId);
      if (investment === null) {
        res.status(404).json();
        return;
      }
      if ((await VoterRegistry.isAllowed(investment, 'removeOperation', req.tokenPayload)) === false) {
        res.status(403).json();
        return;
      }
      const result = await WalletService.removeInvestmentOperation(investmentId, Number.parseInt(operationId, 10));
      if (result.isSuccessful === true) {
        res.status(204).json({});
      } else {
        res.status(400).json(result.errors);
      }
    }
  }
}
