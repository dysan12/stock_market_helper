import { Entity } from '../../../../Lib/Domain/Entity';

export class User extends Entity {
  private _password: string;
  private _name: string;
  private _login: string;

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }
}
