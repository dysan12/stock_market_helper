import { UserPersistence, UserRepository } from '../Infrastructure/UserRepository';
import { User } from './Entity/User';
import { ErrorCodes, Result, ResultInterface } from '../../../Lib/Application/Result';
import { Logger } from '../../../Lib/Infrastructure/Logger';
import { UserChangePasswordData } from './Data/ChangePassword';
import { UserRegisterData } from './Data/Register';
import { UserUpdateDetailsData } from './Data/UpdateDetails';

class Service {
  public async register(data: UserRegisterData.Interface): Promise<ResultInterface<{ id: string }>> {
    const result: Result = new Result();

    const isOccupied = await UserRepository.findOneBy({ login: data.login }) !== null;
    if (isOccupied) {
      result.addError({
        code: ErrorCodes.USER_LOGIN_OCCUPIED,
        message: `User with a login '${data.login}' already exists`,
      });
      return result;
    }

    try {
      const user = new User();
      user.login = data.login;
      user.password = data.password;
      user.name = data.name;

      result.data = { id: await UserPersistence.save(user) };
      result.isSuccessful = true;
    } catch (e) {
      Logger.warn(e); // if recognized, add a proper handling
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: 'Unknown error',
      });
    }
    return result;
  }

  public async changePassword(user: User, data: UserChangePasswordData.Interface): Promise<ResultInterface> {
    const result: Result = new Result();

    if (user.password !== data.oldPassword) {
      result.addError({
        code: ErrorCodes.USER_OLD_PASSWORD_NOT_MATCH,
        message: "Provided oldPassword doesn't match to the actual User password",
      });
      return result;
    }

    try {
      user.password = data.newPassword;
      await UserPersistence.save(user);
      result.isSuccessful = true;
    } catch (e) {
      Logger.warn(e); // if recognized, add a proper handling
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: 'Unknown error',
      });
    }
    return result;
  }

  public async updateDetails(user: User, data: UserUpdateDetailsData.Interface): Promise<ResultInterface> {
    const result: Result = new Result();

    try {
      user.name = data.name;
      await UserPersistence.save(user);
      result.isSuccessful = true;
    } catch (e) {
      Logger.warn(e); // if recognized, add a proper handling
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: 'Unknown error',
      });
    }
    return result;
  }
}
export const UserService = new Service();
