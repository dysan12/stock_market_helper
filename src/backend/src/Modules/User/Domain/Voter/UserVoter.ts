import { User } from '../Entity/User';
import { VoterInterface } from '../../../../Lib/Application/VoterRegistry';
import { TokenPayload } from '../../../../Lib/Common/Token';

class Voter implements VoterInterface {
  supports(obj: any): obj is User {
    return obj instanceof User;
  }

  async vote(obj: any, action: string, token: TokenPayload): Promise<boolean> {
    let result = false;
    if (this.supports(obj)) {
      result = obj.getId() === token.user.id;
    }
    return result;
  }
}

export const UserVoter = new Voter();
