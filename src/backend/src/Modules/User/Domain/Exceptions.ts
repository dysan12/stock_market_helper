import { Exception } from '../../../Lib/Infrastructure/Exceptions';

export class UserNotFound extends Exception {}
