export namespace UserUpdateDetailsData {
  export interface Interface {
    name: string;
  }

  export const constraints = {
    name: { length: { minimum: 3 }, presence: true },
  };
}
