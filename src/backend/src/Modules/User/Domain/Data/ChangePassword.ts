export namespace UserChangePasswordData {
  export interface Interface {
    oldPassword: string;
    newPassword: string;
  }

  export const constraints = {
    oldPassword: { length: { minimum: 3 }, presence: true },
    newPassword: { length: { minimum: 3 }, presence: true },
  };
}
