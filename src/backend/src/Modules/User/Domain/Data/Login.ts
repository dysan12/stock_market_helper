export namespace LoginData {
  export interface Interface {
    login: string;
    password: string;
  }

  export const constraints = {
    login: { length: { minimum: 3 }, presence: true },
    password: { length: { minimum: 3 }, presence: true },
  };
}
