export namespace UserRegisterData {
  export interface Interface {
    name: string;
    login: string;
    password: string;
  }

  export const constraints = {
    name: { length: { minimum: 3 }, presence: true },
    login: { length: { minimum: 3 }, presence: true },
    password: { length: { minimum: 3 }, presence: true },
  };
}
