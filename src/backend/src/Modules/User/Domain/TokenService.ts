import * as jwt from 'jsonwebtoken';
import { LoginData } from './Data/Login';
import { UserRepository } from '../Infrastructure/UserRepository';
import { ErrorCodes, Result } from '../../../Lib/Application/Result';
import { TokenPayload } from '../../../Lib/Common/Token';

class Service {
  private passphrase = '8984618487981844894849818786';
  public async login(data: LoginData.Interface): Promise<Result<string>> {
    const result: Result = new Result<string>();

    const user = await UserRepository.findOneBy({ login: data.login });
    if (user === null) {
      result.addError({
        code: ErrorCodes.USER_NOT_FOUND,
        message: `User with a login '${data.login}' doesn't exist`,
      });
      return result;
    }

    if (user.password !== data.password) {
      result.addError({
        code: ErrorCodes.USER_PASSWORD_NOT_MATCH,
        message: 'Provided password is incorrect',
      });
      return result;
    }

    const payload: TokenPayload = { user: { id: user.getId(), login: user.login } };
    try {
      result.data = jwt.sign(payload, this.passphrase, { expiresIn: '2h' });
      result.isSuccessful = true;
    } catch (e) {
      result.addError({
        code: ErrorCodes.UNKNOWN,
        message: e,
      });
    }
    return result;
  }

  public decode(token: string): TokenPayload {
    return jwt.verify(token, this.passphrase) as TokenPayload;
  }
}

export const TokenService = new Service();
