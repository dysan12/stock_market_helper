import {
  AbstractRepository,
  PersistenceInterface,
  RepositoryInterface,
} from '../../../Lib/Infrastructure/MongoDB/Repository';
import { User } from '../Domain/Entity/User';

class Repository extends AbstractRepository<User> {
  protected getEntityClass(): { prototype: any; name: string } {
    return User;
  }
}

const repo = new Repository();

export const UserRepository: RepositoryInterface<User> = repo;
export const UserPersistence: PersistenceInterface<User> = repo;
