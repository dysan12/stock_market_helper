import { User } from '../Domain/Entity/User';

export function userRespond(user: User): any {
  return {
    id: user.getId(),
    login: user.login,
    name: user.name,
  };
}
