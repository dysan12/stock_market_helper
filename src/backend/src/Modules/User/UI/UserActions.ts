import { Express, Request, Response } from 'express';
import { validateBody } from '../../../Lib/UI/Validator';
import { UserRepository } from '../Infrastructure/UserRepository';
import { UserService } from '../Domain/UserService';
import { UserRegisterData } from '../Domain/Data/Register';
import { UserChangePasswordData } from '../Domain/Data/ChangePassword';
import { LoginData } from '../Domain/Data/Login';
import { TokenService } from '../Domain/TokenService';
import { authorize, AuthorizedRequest } from '../../../Lib/UI/Authorization';
import { VoterRegistry } from '../../../Lib/Application/VoterRegistry';
import { userRespond } from './Responders';
import { UserUpdateDetailsData } from '../Domain/Data/UpdateDetails';

export namespace UserActions {
  export function registerRoutes(App: Express): void {
    App.post('/users', [validateBody(UserRegisterData.constraints)], Handlers.register);
    App.get('/users/:userId', [authorize], Handlers.getOne);
    App.put(
      '/users/:userId/password',
      [authorize, validateBody(UserChangePasswordData.constraints)],
      Handlers.changePassword,
    );
    App.put('/users/:userId', [authorize, validateBody(UserUpdateDetailsData.constraints)], Handlers.updateDetails);
    App.post('/tokens', [validateBody(LoginData.constraints)], Handlers.login);
    App.get('/tokens', [authorize], Handlers.checkToken);
  }

  namespace Handlers {
    export async function login(req: Request, res: Response): Promise<void> {
      const { body } = req;
      const result = await TokenService.login(body);

      if (result.isSuccessful === true) {
        res.status(201).json({ token: result.data });
      } else {
        res.status(400).json(result.errors);
      }
    }

    // todo contract tests
    export async function checkToken(req: Request, res: Response): Promise<void> {
      res.status(200).json();
    }

    export async function register(req: Request, res: Response): Promise<void> {
      const { body } = req;
      const result = await UserService.register(body);

      if (result.isSuccessful === true) {
        res.status(201).json({ id: result.data.id });
      } else {
        res.status(400).json(result.errors);
      }
    }

    export async function getOne(req: AuthorizedRequest<{ userId: string }>, res: Response): Promise<void> {
      const { userId } = req.params;
      const user = await UserRepository.find(userId);
      if (user === null) {
        res.status(404).json();
        return;
      }

      if ((await VoterRegistry.isAllowed(user, 'view', req.tokenPayload)) === false) {
        res.status(403).json();
        return;
      }

      res.send(userRespond(user));
    }

    export async function changePassword(req: AuthorizedRequest<{ userId: string }>, res: Response): Promise<void> {
      const { userId } = req.params;
      const { body } = req;
      const user = await UserRepository.find(userId);
      if (user === null) {
        res.status(404).json();
        return;
      }
      if ((await VoterRegistry.isAllowed(user, 'changePassword', req.tokenPayload)) === false) {
        res.status(403).json();
        return;
      }

      const result = await UserService.changePassword(user, body);
      if (result.isSuccessful === true) {
        res.status(204).json();
      } else {
        res.status(400).json(result.errors);
      }
    }

    export async function updateDetails(req: AuthorizedRequest<{ userId: string }>, res: Response): Promise<void> {
      const { userId } = req.params;
      const { body } = req;

      const user = await UserRepository.find(userId);
      if (user === null) {
        res.status(404).json();
        return;
      }
      if ((await VoterRegistry.isAllowed(user, 'updateDetails', req.tokenPayload)) === false) {
        res.status(403).json();
        return;
      }

      const result = await UserService.updateDetails(user, body);
      if (result.isSuccessful === true) {
        res.status(204).json();
      } else {
        res.status(400).json(result.errors);
      }
    }
  }
}
