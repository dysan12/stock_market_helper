export namespace IndexesData {
  export class Data {
    WIG: string = 'PL9999999995';
    WIG20: string = 'PL9999999987';
    WIG30: string = 'PL9999999375';
  }
}
