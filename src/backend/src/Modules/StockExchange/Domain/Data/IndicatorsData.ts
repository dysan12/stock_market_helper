export namespace IndicatorsData {
  export interface Interface {
    companyUid: string;
  }

  export const constraints = {
    companyUid: { presence: true },
  };
}
