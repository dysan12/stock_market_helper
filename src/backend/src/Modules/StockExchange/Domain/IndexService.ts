import { Logger } from '../../../Lib/Infrastructure/Logger';
import { ErrorCodes, Result, ResultInterface } from '../../../Lib/Application/Result';
import { IndexRepository } from '../Infrastructure/IndexRepository';
import { Indexes } from './Entity/Indexes';
import { IndexesData } from './Data/IndexData';

class Service {
  public async getCurrentIndexes(data: IndexesData.Data): Promise<ResultInterface<Indexes>> {
    const result: Result = new Result<Indexes>();

    try {
      result.data = {
        wig: await IndexRepository.findOneBy({ uid: data.WIG }, { sortBy: 'timestamp', sortType: -1 }),
        wig20: await IndexRepository.findOneBy({ uid: data.WIG20 }, { sortBy: 'timestamp', sortType: -1 }),
        wig30: await IndexRepository.findOneBy({ uid: data.WIG30 }, { sortBy: 'timestamp', sortType: -1 }),
      };
      result.isSuccessful = true;
    } catch (e) {
      Logger.warn(e);
      result.addError({
        code: ErrorCodes.INDEX_COLLECTION_FIND_ERROR,
        message: e,
      });
    }
    return result;
  }
}
export const IndexService = new Service();
