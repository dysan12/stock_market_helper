import { Index } from './Index';

export class Indexes {
  private _wig: Index;
  private _wig20: Index;
  private _wig30: Index;

  get wig(): Index {
    return this._wig;
  }

  get wig20(): Index {
    return this._wig20;
  }

  get wig30(): Index {
    return this._wig30;
  }
}
