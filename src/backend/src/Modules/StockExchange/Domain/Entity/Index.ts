import { CompanyShare } from './CompanyShare';
import { Entity } from '../../../../Lib/Domain/Entity';

export class Index extends Entity {
  private _uid: string;
  private _name: string;
  private _rate: number;
  private _rateChange: number;
  private _openingRate: number;
  private _maxRate: number;
  private _minRate: number;
  private _closingRate: number;
  private _timestamp: number;
  private _companies: Array<CompanyShare>;

  get uid(): string {
    return this._uid;
  }

  get name(): string {
    return this._name;
  }

  get rate(): number {
    return this._rate;
  }

  get rateChange(): number {
    return this._rateChange;
  }

  get openingRate(): number {
    return this._openingRate;
  }

  get maxRate(): number {
    return this._maxRate;
  }

  get minRate(): number {
    return this._minRate;
  }

  get closingRate(): number {
    return this._closingRate;
  }

  get timestamp(): number {
    return this._timestamp;
  }

  get companies(): Array<CompanyShare> {
    return this._companies;
  }
}
