export class CompanyShare {
  private _uid: string;
  private _name: string;
  private _packet: number;
  private _packetPln: number;
  private _walletSharePercentage: number;
  private _sessionStockExchangeSharePercentage: number;
  private _averageSessionSpread: number;
  private _timestamp: number;

  get uid(): string {
    return this._uid;
  }

  get name(): string {
    return this._name;
  }

  get packet(): number {
    return this._packet;
  }

  get packetPln(): number {
    return this._packetPln;
  }

  get walletSharePercentage(): number {
    return this._walletSharePercentage;
  }

  get sessionStockExchangeSharePercentage(): number {
    return this._sessionStockExchangeSharePercentage;
  }

  get averageSessionSpread(): number {
    return this._averageSessionSpread;
  }

  get timestamp(): number {
    return this._timestamp;
  }
}
