import { Entity } from '../../../../Lib/Domain/Entity';

export class Company extends Entity {
  private _uid: string;
  private _name: string;
  private _rateChange: number;
  private _buyOffer: number;
  private _sellOffer: number;
  private _minRate: number;
  private _maxRate: number;
  private _exchangeVolume: number;
  private _exchangeValue: number;
  private _timestamp: number;

  get uid(): string {
    return this._uid;
  }

  get name(): string {
    return this._name;
  }

  get rateChange(): number {
    return this._rateChange;
  }

  get buyOffer(): number {
    return this._buyOffer;
  }

  get sellOffer(): number {
    return this._sellOffer;
  }

  get minRate(): number {
    return this._minRate;
  }

  get maxRate(): number {
    return this._maxRate;
  }

  get exchangeVolume(): number {
    return this._exchangeVolume;
  }

  get exchangeValue(): number {
    return this._exchangeValue;
  }

  get timestamp(): number {
    return this._timestamp;
  }
}
