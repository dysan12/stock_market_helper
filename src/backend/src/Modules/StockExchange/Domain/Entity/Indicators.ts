export namespace Indicators {
  export interface Interface {
    rsi: number;
    slowSts: number;
    fastSts: number;
    macd: number;
    trix: number;
    sma5: number;
    sma15: number;
    sma30: number;
    sma60: number;
    ema5: number;
    ema15: number;
    ema30: number;
    ema60: number;
  }

  export const constraints = {
    rsi: { presence: true },
    slowSts: { presence: true },
    fastSts: { presence: true },
    macd: { presence: true },
    trix: { presence: true },
    sma5: { presence: true },
    sma15: { presence: true },
    sma30: { presence: true },
    sma60: { presence: true },
    ema5: { presence: true },
    ema15: { presence: true },
    ema30: { presence: true },
    ema60: { presence: true },
  };
}
