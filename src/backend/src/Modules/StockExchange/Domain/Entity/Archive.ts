import { Entity } from '../../../../Lib/Domain/Entity';

export class Archive extends Entity {
  private _uid: string;
  private _name: string;
  private _currency: string;
  private _openingRate: number;
  private _rateChange: number;
  private _maxRate: number;
  private _minRate: number;
  private _closingRate: number;
  private _exchangeVolume: number;
  private _transactionsAmount: number;
  private _exchangeValue: number;
  private _date: string;
  private _timestamp: number;

  get uid(): string {
    return this._uid;
  }

  get name(): string {
    return this._name;
  }

  get currency(): string {
    return this._currency;
  }

  get openingRate(): number {
    return this._openingRate;
  }

  get rateChange(): number {
    return this._rateChange;
  }

  get maxRate(): number {
    return this._maxRate;
  }

  get minRate(): number {
    return this._minRate;
  }

  get closingRate(): number {
    return this._closingRate;
  }

  get exchangeVolume(): number {
    return this._exchangeVolume;
  }

  get transactionsAmount(): number {
    return this._transactionsAmount;
  }

  get exchangeValue(): number {
    return this._exchangeValue;
  }

  get date(): string {
    return this._date;
  }

  get timestamp(): number {
    return this._timestamp;
  }
}
