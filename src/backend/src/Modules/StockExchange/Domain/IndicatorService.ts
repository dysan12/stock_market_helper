import { Logger } from '../../../Lib/Infrastructure/Logger';
import { ErrorCodes, Result, ResultInterface } from '../../../Lib/Application/Result';
import { ArchiveRepository } from '../Infrastructure/ArchiveRepository';
import { IndicatorOperations } from './IndicatorOperations';
import { IndicatorsData } from './Data/IndicatorsData';
import { Archive } from './Entity/Archive';
import { Indicators } from './Entity/Indicators';

class Service {
  public async getIndicators(data: IndicatorsData.Interface): Promise<ResultInterface<Indicators.Interface>> {
    const result: Result = new Result<Indicators.Interface>();

    const archives = await ArchiveRepository.findBy(
      { uid: data.companyUid },
      { sortBy: 'date', sortType: -1, limit: 60 },
    );
    if (!archives) {
      result.addError({
        code: ErrorCodes.ARCHIVE_COLLECTION_FIND_ERROR,
        message: 'Archives collection find error.',
      });
      return result;
    }
    if (archives.length < 60) {
      result.addError({
        code: ErrorCodes.ARCHIVE_COLLECTION_LENGTH_ERROR,
        message: 'Archives collection is less than 60.',
      });
      return result;
    }

    try {
      result.data = {
        rsi: this.getRelativeStrengthIndex(archives.slice(0, 14)),
        slowSts: this.getSlowStochasticOscillator(archives.slice(0, 14)),
        fastSts: this.getFastStochasticOscillator(archives.slice(0, 16)),
        macd: this.getMovingAverageConvergenceDivergence(archives.slice(0, 26)),
        trix: this.getTripleExponentialAverage(archives.slice(0, 15)),
        sma5: this.getSimpleMovingAverage(archives.slice(0, 5)),
        sma15: this.getSimpleMovingAverage(archives.slice(0, 15)),
        sma30: this.getSimpleMovingAverage(archives.slice(0, 30)),
        sma60: this.getSimpleMovingAverage(archives.slice(0, 60)),
        ema5: this.getExponentialMovingAverage(archives.slice(0, 5)),
        ema15: this.getExponentialMovingAverage(archives.slice(0, 15)),
        ema30: this.getExponentialMovingAverage(archives.slice(0, 30)),
        ema60: this.getExponentialMovingAverage(archives.slice(0, 60)),
      };
      result.isSuccessful = true;
      for (var indicator in result.data) {
        if (result.data[indicator] === null) {
          Logger.warn('Indicator null value error: ' + indicator);
          result.addError({
            code: ErrorCodes.INDICATOR_NULL_VALUE_ERROR,
            message: 'Indicator null value error: ' + indicator,
          });
          result.isSuccessful = false;
        }
      }
    } catch (e) {
      Logger.warn(e);
      result.addError({
        code: ErrorCodes.INDICATOR_OPERATION_ERROR,
        message: e,
      });
    }
    return result;
  }

  private getRelativeStrengthIndex(archives: Array<Archive>): number {
    try {
      const gains = archives.filter(a => a.rateChange > 0.0).map(a => a.rateChange);
      const losses = archives.filter(a => a.rateChange < 0.0).map(a => a.rateChange);
      const rsi = IndicatorOperations.getRelativeStrengthIndex(gains, losses);
      return rsi;
    } catch (e) {
      Logger.warn(e);
      throw e;
    }
  }

  private getSlowStochasticOscillator(archives: Array<Archive>): number {
    try {
      const l14 = Math.min(...archives.map(a => a.minRate));
      const h14 = Math.max(...archives.map(a => a.maxRate));
      const closingRate = archives[0].closingRate;

      const slowSts = ((closingRate - l14) / (h14 - l14)) * 100;
      return slowSts;
    } catch (e) {
      Logger.warn(e);
      throw e;
    }
  }

  private getFastStochasticOscillator(archives: Array<Archive>): number {
    try {
      const l14 = Math.min(...archives.slice(0, 13).map(a => a.minRate));
      const h14 = Math.max(...archives.slice(0, 13).map(a => a.maxRate));
      const l15 = Math.min(...archives.slice(0, 14).map(a => a.minRate));
      const h15 = Math.max(...archives.slice(0, 14).map(a => a.maxRate));
      const l16 = Math.min(...archives.map(a => a.minRate));
      const h16 = Math.max(...archives.map(a => a.maxRate));
      const closingRate = archives[0].closingRate;

      const fastSts = IndicatorOperations.getFastStochasticOscillator(l14, l15, l16, h14, h15, h16, closingRate);
      return fastSts;
    } catch (e) {
      Logger.warn(e);
      throw e;
    }
  }

  private getMovingAverageConvergenceDivergence(archives: Array<Archive>): number {
    try {
      const closingRates = archives.map(a => a.closingRate);

      const macd = IndicatorOperations.getMovingAverageConvergenceDivergence(closingRates);
      return macd;
    } catch (e) {
      Logger.warn(e);
      throw e;
    }
  }

  private getTripleExponentialAverage(archives: Array<Archive>): number {
    try {
      const closingRates = archives.map(a => a.closingRate);

      const trix = IndicatorOperations.getTripleExponentialAverage(closingRates);
      return trix;
    } catch (e) {
      Logger.warn(e);
      throw e;
    }
  }

  private getSimpleMovingAverage(archives: Array<Archive>): number {
    try {
      const closingRates = archives.map(a => a.closingRate);

      const sma = IndicatorOperations.getSimpleMovingAverage(...closingRates);
      return sma;
    } catch (e) {
      Logger.warn(e);
      throw e;
    }
  }

  private getExponentialMovingAverage(archives: Array<Archive>): number {
    try {
      const closingRates = archives.map(a => a.closingRate);

      const ema = IndicatorOperations.getExponentialMovingAverage(closingRates);
      return ema;
    } catch (e) {
      Logger.warn(e);
      throw e;
    }
  }
}
export const IndicatorService = new Service();
