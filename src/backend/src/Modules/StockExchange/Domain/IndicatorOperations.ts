class Operations {
  //TODO: Check indicators values reliability
  public getRelativeStrengthIndex(gains: Array<number>, losses: Array<number>): number {
    const gainsAvg = gains.reduce((element, sum) => element + sum, 0) / gains.length;
    const lossesAvg = (losses.reduce((element, sum) => element + sum, 0) / losses.length) * -1;
    /* RSI */
    const rsi = 100 - 100 / (1 + gainsAvg / lossesAvg);
    return rsi;
  }

  public getSlowStochasticOscillator(l14: number, h14: number, latestClosingRate: number): number {
    /* %K */
    const slowSts = ((latestClosingRate - l14) / (h14 - l14)) * 100;
    return slowSts;
  }

  public getFastStochasticOscillator(
    l14: number,
    l15: number,
    l16: number,
    h14: number,
    h15: number,
    h16: number,
    latestClosingRate: number,
  ): number {
    const firstPeriod = this.getSlowStochasticOscillator(l14, h14, latestClosingRate);
    const secondPeriod = this.getSlowStochasticOscillator(l15, h15, latestClosingRate);
    const thirdPeriod = this.getSlowStochasticOscillator(l16, h16, latestClosingRate);
    /* %D -> 3-period of %K - SMA */
    const fastSts = this.getSimpleMovingAverage(firstPeriod, secondPeriod, thirdPeriod);
    return fastSts;
  }

  public getMovingAverageConvergenceDivergence(closingRates: Array<number>): number {
    /* Note: closingRates must be sorted from the NEWEST (at 0 index) to the OLDEST (last index) */
    if (closingRates.length < 26) {
      return null;
    }
    const ema12 = this.getExponentialMovingAverage(closingRates.slice(0, 11).reverse());
    const ema26 = this.getExponentialMovingAverage(closingRates.slice(0, 25).reverse());
    /* MACD */
    const macd = ema12 - ema26;

    return macd;
  }

  public getTripleExponentialAverage(closingRates: Array<number>): number {
    /* Note: closingRates must be sorted from the NEWEST (at 0 index) to the OLDEST (last index) */
    if (closingRates.length < 15) {
      return null;
    }
    const ema1Series = this.getgetExponentialMovingAverageSeries(closingRates.slice(0, closingRates.length - 1).reverse());
    const ema2Series = this.getgetExponentialMovingAverageSeries(ema1Series);
    const ema3Series = this.getgetExponentialMovingAverageSeries(ema2Series);
    /* TRIX */
    const trix = (ema3Series[ema3Series.length - 1] - ema3Series[ema3Series.length - 2])/(ema3Series[ema3Series.length - 2]);

    return trix;
  }

  public getSimpleMovingAverage(...params: number[]): number {
    /* SMA */
    const sma = params.reduce((sum, current) => sum + current, 0) / params.length;
    return sma;
  }

  public getExponentialMovingAverage(closingRates: Array<number>): number {
    //Note: closingRates must be sorted from the OLDEST (at 0 index) to the NEWEST (last index)
    const emaSeries = this.getgetExponentialMovingAverageSeries(closingRates);
    return emaSeries[emaSeries.length - 1];
  }

  private getgetExponentialMovingAverageSeries(closingRates: Array<number>): number[] {
    let emaSeries = [];
    let emaSeriesLen = 0;
    const multiplier = 2 / (closingRates.length + 1);
    emaSeriesLen = emaSeries.push(this.getSimpleMovingAverage(...closingRates));
    for (let closingRate of closingRates) {
      /* EMA */
      emaSeriesLen = emaSeries.push((closingRate - emaSeries[emaSeriesLen - 1]) * multiplier + emaSeries[emaSeriesLen - 1]);
    }
    return emaSeries;
  }
}
export const IndicatorOperations = new Operations();
