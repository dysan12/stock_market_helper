import { AbstractRepository, RepositoryInterface } from '../../../Lib/Infrastructure/MongoDB/Repository';
import { Index } from '../Domain/Entity/Index';

class Repository extends AbstractRepository<Index> {
  protected getEntityClass(): { prototype: any; name: string } {
    return Index;
  }
}

const repo = new Repository();

export const IndexRepository: RepositoryInterface<Index> = repo;
