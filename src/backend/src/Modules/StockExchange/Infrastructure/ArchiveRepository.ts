import { AbstractRepository, RepositoryInterface } from '../../../Lib/Infrastructure/MongoDB/Repository';
import { Archive } from '../Domain/Entity/Archive';

class Repository extends AbstractRepository<Archive> {
  protected getEntityClass(): { prototype: any; name: string } {
    return Archive;
  }
}

const repo = new Repository();

export const ArchiveRepository: RepositoryInterface<Archive> = repo;
