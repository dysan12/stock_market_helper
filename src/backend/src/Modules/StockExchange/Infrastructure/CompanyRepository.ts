import { AbstractRepository, RepositoryInterface } from '../../../Lib/Infrastructure/MongoDB/Repository';
import { Company } from '../Domain/Entity/Company';

class Repository extends AbstractRepository<Company> {
  protected getEntityClass(): { prototype: any; name: string } {
    return Company;
  }
}

const repo = new Repository();

export const CompanyRepository: RepositoryInterface<Company> = repo;
