import { Express, Response } from 'express';
import { AuthorizedRequest } from '../../../Lib/UI/Authorization';
import { CompanyRepository } from '../Infrastructure/CompanyRepository';
import { companyRespond, companiesRespond, indicatorsRespond } from './Responders';
import { IndicatorService } from '../Domain/IndicatorService';

export namespace CompanyActions {
  export function registerRoutes(App: Express): void {
    App.get('/companies/:companyUid', Handlers.getOne);
    App.get('/companies/:companyUid/indicators', Handlers.getIndicators);
  }

  namespace Handlers {
    export async function getOne(req: AuthorizedRequest<{ companyUid: string }>, res: Response): Promise<void> {
      const { companyUid } = req.params;
      const company = await CompanyRepository.findOneBy({ uid: companyUid }, { sortBy: 'timestamp', sortType: -1 });
      if (company === null) {
        res.status(404).json();
        return;
      }

      res.send(companyRespond(company));
    }

    export async function getIndicators(req: AuthorizedRequest<{ companyUid: string }>, res: Response): Promise<void> {
      const { companyUid } = req.params;
      const result = await IndicatorService.getIndicators({ companyUid: companyUid });

      if (result.isSuccessful === true) {
        res.send(indicatorsRespond(result.data));
      } else {
        res.status(400).json(result.errors);
      }
    }
  }
}
