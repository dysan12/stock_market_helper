import { Express, Response } from 'express';
import { AuthorizedRequest } from '../../../Lib/UI/Authorization';
import { IndexRepository } from '../Infrastructure/IndexRepository';
import { indexRespond, indexesRespond } from './Responders';
import { IndexService } from '../Domain/IndexService';
import { IndexesData } from '../Domain/Data/IndexData';

export namespace IndexActions {
  export function registerRoutes(App: Express): void {
    App.get('/indexes', Handlers.getCurrentIndexes);
    App.get('/indexes/:indexUid', Handlers.getOne);
  }

  namespace Handlers {
    export async function getCurrentIndexes(req: AuthorizedRequest<{}>, res: Response): Promise<void> {
      const result = await IndexService.getCurrentIndexes(new IndexesData.Data());
      if (result.isSuccessful === true) {
        res.send(indexesRespond(result.data));
      } else {
        res.status(400).json(result.errors);
      }
    }

    export async function getOne(req: AuthorizedRequest<{ indexUid: string }>, res: Response): Promise<void> {
      const { indexUid } = req.params;
      const index = await IndexRepository.findOneBy({ uid: indexUid }, { sortBy: 'timestamp', sortType: -1 });
      if (index === null) {
        res.status(404).json();
        return;
      }

      res.send(indexRespond(index));
    }
  }
}
