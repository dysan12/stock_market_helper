import { Company } from '../Domain/Entity/Company';
import { Index } from '../Domain/Entity/Index';
import { Indicators } from '../Domain/Entity/Indicators';
import { Indexes } from '../Domain/Entity/Indexes';

export function companyRespond(company: Company): any {
  return {
    uid: company.uid,
    name: company.name,
    rateChange: company.rateChange,
    buyOffer: company.buyOffer,
    sellOffer: company.sellOffer,
    minRate: company.minRate,
    maxRate: company.maxRate,
    exchangeVolume: company.exchangeVolume,
    exchangeValue: company.exchangeValue,
    timestamp: company.timestamp,
  };
}

export function companiesRespond(companies: Array<Company>): any {
  return {
    data: companies.map(c => ({
      uid: c.uid,
      name: c.name,
      rateChange: c.rateChange,
      buyOffer: c.buyOffer,
      sellOffer: c.sellOffer,
      minRate: c.minRate,
      maxRate: c.maxRate,
      exchangeVolume: c.exchangeVolume,
      exchangeValue: c.exchangeValue,
      timestamp: c.timestamp,
    })),
  };
}

export function indexRespond(index: Index): any {
  return {
    uid: index.uid,
    name: index.name,
    rate: index.rate,
    rateChange: index.rateChange,
    openingRate: index.openingRate,
    maxRate: index.maxRate,
    minRate: index.minRate,
    closingRate: index.closingRate,
    timestamp: index.timestamp,
    companies: index.companies.map(cs => ({
      uid: cs.uid,
      name: cs.name,
      packet: cs.packet,
      packetPln: cs.packetPln,
      walletSharePercentage: cs.walletSharePercentage,
      sessionStockExchangeSharePercentage: cs.sessionStockExchangeSharePercentage,
      averageSessionSpread: cs.averageSessionSpread,
      timestamp: cs.timestamp,
    })),
  };
}

export function indexesRespond(indexes: Indexes): any {
  return {
    data: [indexRespond(indexes.wig), indexRespond(indexes.wig20), indexRespond(indexes.wig30)],
  };
}

export function indicatorsRespond(indicators: Indicators.Interface): any {
  return {
    rsi: indicators.rsi,
    slowSts: indicators.slowSts,
    fastSts: indicators.fastSts,
    macd: indicators.macd,
    trix: indicators.trix,
    sma5: indicators.sma5,
    sma15: indicators.sma15,
    sma30: indicators.sma30,
    sma60: indicators.sma60,
    ema5: indicators.ema5,
    ema15: indicators.ema15,
    ema30: indicators.ema30,
    ema60: indicators.ema60,
  };
}
