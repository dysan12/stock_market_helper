import time
import calendar
import pymongo
import requests
from datetime import datetime, timedelta
from bs4 import BeautifulSoup as bs
from pymongo import MongoClient

# Scraper headers
user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.92"
headers = {'User-Agent': user_agent}

# Connection
client = MongoClient('mongodb://root:qwerty@mongo:27017')
db = client.smh
collection = db.archive


# Methods
def getCompanyArchiveDataRequestUrl(date):
    return "https://www.gpw.pl/archiwum-notowan-full?fetch=0&type=10&instrument=&date=" + str(date)


def getCompaniesArchiveData(startDate, days):
    print("Starting scraping - archive companies data " +
          ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))
    # Collect archive data from X days starting from startDate (counting startDate as 1)
    startDateTime = datetime.strptime(startDate, "%d-%m-%Y")
    for days in range(0, days):
        date = startDateTime - timedelta(days=days)
        print("Start - archive data for " + date.strftime("%Y-%m-%dT%H:%M:%SZ") +
              ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))
        response = requests.get(getCompanyArchiveDataRequestUrl(
            date.strftime("%d-%m-%Y")), headers=headers)
        soup = bs(response.text, "html.parser")
        data = soup.find("th", text="Nazwa")
        if data is not None:
            circulation_data = data.find_next("td")
            while circulation_data is not None:
                name = circulation_data
                if name is not None:
                    uid = name.find_next("td")
                    currency = uid.find_next("td")
                    opening_rate = currency.find_next("td")
                    max_rate = opening_rate.find_next("td")
                    min_rate = max_rate.find_next("td")
                    closing_rate = min_rate.find_next("td")
                    rate_change = closing_rate.find_next("td")
                    exchange_volume = rate_change.find_next("td")
                    transactions_amount = exchange_volume.find_next("td")
                    exchange_value = transactions_amount.find_next("td")
                    entity = {
                        'uid': uid.text.strip(),
                        'name': name.text.strip(),
                        'currency': currency.text.strip(),
                        'openingRate': float(opening_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                        'maxRate': float(max_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                        'minRate': float(min_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                        'closingRate': float(closing_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                        'rateChange': float(rate_change.text.strip().replace('\xa0', '').replace(',','.')),
                        'exchangeVolume': float(exchange_volume.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                        'transactionsAmount': float(transactions_amount.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                        'exchangeValue': float(exchange_value.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                        'date': date.strftime("%Y-%m-%dT%H:%M:%SZ"),
                        'timestamp': int(time.time())
                    }
                    print("Save - archive data with UID: " +
                          uid.text.strip() + " for date " + date.strftime("%Y-%m-%dT%H:%M:%SZ") +
                          ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))
                    inserted_id = collection.insert_one(
                        entity).inserted_id
                    print("Inserted - archive data with ID: " + str(inserted_id) + " for UID: " +
                          uid.text.strip() + " for date " + date.strftime("%Y-%m-%dT%H:%M:%SZ") +
                          ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))

                    circulation_data = exchange_value.find_next("td")

# Launch method


def getArchives():
    getCompaniesArchiveData(datetime.today().strftime("%d-%m-%Y"), 100)
    client.close()

# Launch


def main():
    getArchives()


if __name__ == "__main__":
    main()
