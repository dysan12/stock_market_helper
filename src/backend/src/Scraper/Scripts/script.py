import time
import companies_archive
import companies_current
import indexes_current

time.sleep(10)
print("Indexes scraping started...")
indexes_current.main()
print("Indexes scraping ended..")
time.sleep(10)
print("Companies scraping started....")
companies_current.main()
print("Companies scraping ended.")
time.sleep(10)
print("Archives scraping started...")
companies_archive.main()
print("Archives scraping ended.")
time.sleep(5)
print("Scrapping ended generally.")
# print("Scrapper disabled in code - commented out")
