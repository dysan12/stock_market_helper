import time
import calendar
import pymongo
import requests
from datetime import datetime, timedelta
from bs4 import BeautifulSoup as bs
from pymongo import MongoClient

# Scraper headers
user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.92"
headers = {'User-Agent': user_agent}

# Connection
client = MongoClient('mongodb://root:qwerty@mongo:27017')
db = client.smh
collection = db.index

# Methods


def getWigDataRequestUrl(indexId):
    return "https://gpwbenchmark.pl/karta-indeksu?isin=" + indexId


def getWigCompaniesRequestUrl(indexId):
    return "https://www.gpw.pl/ajaxindex.php?action=GPWIndexes&start=ajaxPortfolio&format=html&lang=PL&isin=" + indexId + "&cmng_id=1010&time=" + str(calendar.timegm(time.gmtime()))


def getWigCurrentData(indexName, indexId):
    print("Starting scraping - index data with UID: " + indexId +
          ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))
    # Data
    response = requests.get(getWigDataRequestUrl(
        indexId), headers=headers)
    soup = bs(response.text, "html.parser")
    companies = []
    data_rate = soup.find("span", class_="summary")
    if data_rate is not None:
        rate_profit = soup.find("b", class_="profit")
        rate_loss = soup.find("b", class_="loss")
        opening_rate = data_rate.find_next(
            "td", text="otwarcie").find_next("td")
        min_rate = opening_rate.find_next("td").find_next("td")
        max_rate = min_rate.find_next("td").find_next("td")
        closing_rate = max_rate.find_next("td").find_next("td")
    else:
        print("Error no data - company data with UID: " + indexId +
              ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))
    # Companies
    response = requests.get(getWigCompaniesRequestUrl(
        indexId), headers=headers)
    soup = bs(response.text, "html.parser")
    companies_data = soup.find("th", text="Instrument")
    if companies_data is not None:
        circulation_data = companies_data.find_next("a")
        while circulation_data is not None:
            name = circulation_data
            if name is not None:
                company_uid = name.find_next("td")
                packet = company_uid.find_next("td")
                packet_pln = packet.find_next("td")
                wallet_share_percentage = packet_pln.find_next("td")
                session_stock_exchange_share_percentage = wallet_share_percentage.find_next(
                    "td")
                average_session_spread = session_stock_exchange_share_percentage.find_next(
                    "td")
                entity = {
                    'uid': company_uid.text.strip(),
                    'name': name.text.strip(),
                    'packet': float(packet.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                    'packetPln': float(packet_pln.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                    'walletSharePercentage': float(wallet_share_percentage.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                    'sessionStockExchangeSharePercentage': float(session_stock_exchange_share_percentage.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                    'averageSessionSpread': float(average_session_spread.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
                    'timestamp': int(time.time())
                }
                companies.append(entity)
                circulation_data = average_session_spread.find_next("a")
    mongo_entity = {
        'uid': indexId,
        'name': indexName,
        'rate': float(data_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
        'rateChange': float(rate_profit.text.strip().replace('+', '').replace('%', '').replace(',','.')) if rate_profit is not None else float(rate_loss.text.strip().replace('%', '').replace(',','.')),
        'openingRate': float(opening_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
        'maxRate': float(max_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
        'minRate': float(min_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
        'closingRate': float(closing_rate.text.strip().replace('\xa0', '').replace(' ', '').replace(',','.')),
        'timestamp': int(time.time()),
        'companies': companies
    }
    print("Save - index data with UID: " + indexId +
          ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))
    inserted_id = collection.insert_one(mongo_entity).inserted_id
    print("Inserted - index with ID: " + str(inserted_id) + " for UID: " +
          indexId + ": " + datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"))


# Launch data
indexes = {
    "WIG": "PL9999999995",
    "WIG20": "PL9999999987",
    "WIG30": "PL9999999375"
}

# Launch method


def getIndexesData():
    for key, value in indexes.items():
        getWigCurrentData(key, value)
    client.close()

# Launch


def main():
    getIndexesData()


if __name__ == "__main__":
    main()
