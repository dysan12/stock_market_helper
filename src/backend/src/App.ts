import bodyParser from 'body-parser';
import express from 'express';
import lusca from 'lusca';
import morgan from 'morgan';
import cors from 'cors';
import { UserActions } from './Modules/User/UI/UserActions';
import { bootConfig } from './Lib/Infrastructure/Config';
import { bootServices } from './Lib/ServiceLoader';
import { CompanyActions } from './Modules/StockExchange/UI/CompanyActions';
import { IndexActions } from './Modules/StockExchange/UI/IndexActions';
import { WalletActions } from './Modules/Wallet/UI/WalletActions';

const App = express();
bootConfig();
bootServices();

// todo add log on which port app is listening
App.set('port', process.env.PORT || 3000);
App.use(cors());
App.use(bodyParser.json());
App.use(bodyParser.urlencoded({ extended: true }));
App.use(lusca.xframe('SAMEORIGIN'));
App.use(lusca.xssProtection(true));
App.use(morgan('combined'));
App.listen(App.get('port'));
UserActions.registerRoutes(App);
CompanyActions.registerRoutes(App);
IndexActions.registerRoutes(App);
WalletActions.registerRoutes(App);
