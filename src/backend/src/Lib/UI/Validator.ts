import { Request, Response } from 'express-serve-static-core';
import { NextFunction } from 'express';
import * as validator from 'validate.js';
import { ErrorCodes, FieldError } from '../Application/Result';

export function validateBody(constraints: any) {
  return (req: Request, res: Response, next: NextFunction) => {
    const violations = validate(req.body, constraints);
    if (violations.length > 0) {
      res.status(400).json(violations);
    } else {
      next();
    }
  };
}

export function validate(obj: any, constraints: any): FieldError[] {
  return (
    validator.validate(obj, constraints, {
      format: 'custom',
    }) || []
  );
}

validator.formatters.custom = (errors: any[]) => {
  return errors.map(
    (e): FieldError => ({
      field: e.attribute,
      message: e.error,
      // @ts-ignore
      code: ErrorCodes.validator[e.validator] || e.validator,
    }),
  );
};
