import { NextFunction, Request, Response } from 'express';
// eslint-disable-next-line import/no-unresolved
import * as core from 'express-serve-static-core'; // requiring this package causes errors in action handlers
import { TokenService } from '../../Modules/User/Domain/TokenService';
import { TokenPayload } from '../Common/Token';

export function authorize(req: AuthorizedRequest, res: Response, next: NextFunction): void {
  const authHeader = req.header('authorization');
  if (authHeader === undefined) {
    res.status(401).json({});
    return;
  }

  const token = authHeader.split(' ')[1];
  try {
    req.tokenPayload = TokenService.decode(token);
    next();
  } catch (e) {
    res.status(401).json({});
  }
}

/**
 * Use authorize middleware before handling a request, otherwise tokenPayload will be undefined!
 */
export interface AuthorizedRequest<P extends core.Params = core.ParamsDictionary> extends Request<P> {
  tokenPayload: TokenPayload;
}
