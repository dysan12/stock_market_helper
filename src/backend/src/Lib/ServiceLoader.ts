import { VoterRegistry } from './Application/VoterRegistry';
import { UserVoter } from '../Modules/User/Domain/Voter/UserVoter';
import { InvestmentVoter } from '../Modules/Wallet/Domain/Voter/InvestmentVoter';

export function bootServices(): void {
  VoterRegistry.register(UserVoter);
  VoterRegistry.register(InvestmentVoter);
}
