export class Result<DataType = any> implements ResultInterface<DataType> {
  constructor(public isSuccessful = false, public errors: Error[] = [], public data: DataType = null) {}

  addError(error: Error): void {
    this.errors.push(error);
  }
}

export interface ResultInterface<T = undefined> {
  readonly isSuccessful: boolean;
  readonly errors: Error[];
  readonly data: T;
}

export interface Error {
  code: string;
  message: string;
}

export interface FieldError extends Error {
  field: string;
}

export const ErrorCodes = {
  USER_OLD_PASSWORD_NOT_MATCH: '001',
  USER_LOGIN_OCCUPIED: '002',
  USER_NOT_FOUND: '003',
  USER_PASSWORD_NOT_MATCH: '004',
  INDEX_COLLECTION_FIND_ERROR: '021',
  ARCHIVE_COLLECTION_FIND_ERROR: '041',
  ARCHIVE_COLLECTION_LENGTH_ERROR: '042',
  INDICATOR_NULL_VALUE_ERROR: '061',
  INDICATOR_OPERATION_ERROR: '062',
  CREATE_OPERATION_VALIDATION_FAILURE: '080',
  INVESTMENT_NOT_FOUND: '081',
  OPERATION_VIOLATES_STATE: '082',
  UNKNOWN: '666',

  // validators mapper
  validator: {
    length: '101',
    presence: '102',
    inclusion: '103',
    type: '104',
    numericality: '105',
  },
};
