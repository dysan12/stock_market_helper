import { TokenPayload } from '../Common/Token';

class Registry {
  private voters: VoterInterface[] = [];

  public register(voter: VoterInterface): void {
    this.voters.push(voter);
  }

  /**
   * @param obj
   * @param action - for eg. EDIT, SHOW, DELETE
   * @param token
   */
  public async isAllowed(obj: any, action: string, token: TokenPayload): Promise<boolean> {
    // eslint-disable-next-line no-restricted-syntax
    for (const voter of this.voters) {
      if (voter.supports(obj)) {
        return voter.vote(obj, action, token);
      }
    }
    return true;
  }
}

export interface VoterInterface {
  /**
   * Called before vote()
   */
  supports(obj: any): boolean;
  vote(obj: any, action: string, token: TokenPayload): Promise<boolean>;
}

export const VoterRegistry = new Registry();
