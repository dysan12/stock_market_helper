import dotenv from 'dotenv';
import fs from 'fs';
import { Logger } from './Logger';

let booted = false;
export function bootConfig(): void {
  if (fs.existsSync('.env')) {
    Logger.debug('Loading .env file');
    dotenv.config({ path: '.env' });
  }

  if (fs.existsSync('.env.local')) {
    Logger.debug('Loading .env.local file');
    dotenv.config({ path: '.env.local' });
  }
  booted = true;
}

export function isBooted(): boolean {
  return booted;
}
