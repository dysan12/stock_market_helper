import { Operation } from '../../../Modules/Wallet/Domain/Operation';

export const ProtoMap = {
  // ugly solution but works
  // todo maybe moving that info to the Repository of the parent would be a better idea
  [Operation.name]: Operation.prototype,
};
