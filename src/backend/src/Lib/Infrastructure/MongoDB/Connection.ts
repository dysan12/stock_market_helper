import { Collection, MongoClient } from 'mongodb';
import { CollectionNotFound, ConnectionError } from './Exceptions';
import { env } from '../Env';

export type PerformCallback<T> = (col: Collection) => Promise<T>;

export async function performInDb<T>(collectionName: string, callback: PerformCallback<T>): Promise<T> {
  let client: MongoClient;
  try {
    client = await MongoClient.connect(env('MONGO_URL'), {
      useUnifiedTopology: true,
    });
  } catch (e) {
    throw new ConnectionError('Cannot connect to the MongoDB');
  }

  let collection: Collection;
  try {
    const db = client.db(env('MONGO_DB_NAME'));
    collection = await db.collection(collectionName);
  } catch (e) {
    await client.close();
    throw new CollectionNotFound(`Cannot find a collection with the name ${collectionName}`);
  }

  let result;
  try {
    result = await callback(collection);
  } catch (e) {
    throw e;
  } finally {
    await client.close();
  }
  return result;
}
