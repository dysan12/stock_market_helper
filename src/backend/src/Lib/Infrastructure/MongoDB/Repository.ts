import { ObjectID } from 'mongodb';
import { PerformCallback, performInDb } from './Connection';
import { Logger } from '../Logger';
import { CollectionNotFound, ConnectionError } from './Exceptions';
import { Entity } from '../../Domain/Entity';
import { ProtoMap } from './ProtoMap';

type SortType = 1 | -1;
type ConditionValue = string | number | boolean | null | ObjectID | Conditions | ConditionValue[];
type RawObject = { [key: string]: any };
interface Conditions {
  [key: string]: ConditionValue;
}
interface Filters {
  sortBy: string;
  sortType: SortType;
  limit?: number;
}
export abstract class AbstractRepository<EntityType extends Entity>
  implements RepositoryInterface<EntityType>, PersistenceInterface<EntityType> {
  public async find(id: string): Promise<EntityType | null> {
    try {
      const idObj = new ObjectID(id);
      return (await this.findBy({ _id: idObj }))[0];
    } catch (e) {
      return null;
    }
  }

  public async findByIds(ids: Array<string>): Promise<EntityType[]> {
    if (ids.length === 0) {
      return [] as EntityType[];
    }
    return this.findBy({
      $or: ids.map(id => ({
        _id: new ObjectID(id),
      })),
    });
  }

  public async findAll(): Promise<EntityType[] | null> {
    const result: RawObject[] | null = await this.perform(col => {
      return col.find().toArray();
    });
    if (result === null) {
      // todo afaik it's not possible to receive null from find() - to check
      return null;
    }
    return this.deserializeArr(result, this.getEntityClass().prototype);
  }

  public async findBy(conditions: Conditions, filters?: Filters): Promise<EntityType[] | null> {
    const result = await this.perform(col => {
      if (filters) {
        return col
          .find(conditions)
          .sort({ [filters.sortBy]: filters.sortType })
          .limit(filters.limit || 1)
          .toArray();
      } else {
        return col.find(conditions).toArray();
      }
    });
    if (result === null) {
      return null;
    }
    return this.deserializeArr(result, this.getEntityClass().prototype);
  }

  public async findOneBy(conditions: Conditions, filters?: Filters): Promise<EntityType | null> {
    const result = await this.perform(col => {
      if (filters) {
        return col
          .find(conditions)
          .sort({ [filters.sortBy]: filters.sortType })
          .limit(1)
          .toArray();
      } else {
        return col.find(conditions).toArray();
      }
    });
    if (result === null || result.length === 0 || result[0] === null) {
      return null;
    }
    return this.deserializeObj(result[0], this.getEntityClass().prototype);
  }

  public async save(obj: EntityType): Promise<string> {
    if (obj.getId() === null) {
      const result = await this.perform(col => {
        return col.insertOne(getObjForSave(obj));
      });
      const id = result.insertedId;
      // @ts-ignore
      obj.setId(id);
      return id.toHexString();
    } else {
      const idObj = new ObjectID(obj.getId());
      await this.perform(col => {
        return col.updateOne({ _id: idObj }, { $set: getObjForUpdate(obj) });
      });
      return obj.getId();
    }
  }
  protected abstract getEntityClass(): { prototype: any; name: string };

  protected async perform<T>(callback: PerformCallback<T>): Promise<T> {
    try {
      return await performInDb(this.getCollectionName(), callback);
    } catch (e) {
      if (e instanceof ConnectionError || e instanceof CollectionNotFound) {
        Logger.warn(e.msg);
      }
      throw e;
    }
  }

  protected deserializeObj(toDeser: RawObject, proto: any): EntityType {
    const deser: any = { __proto__: proto };
    Object.keys(toDeser).forEach(key => {
      const fieldName = key.replace(/^_/, ''); // _id to id
      const value = toDeser[key];
      if (Array.isArray(value)) {
        deser[`_${fieldName}`] = value.map(v => {
          if (v.__type__ && ProtoMap[v.__type__] !== undefined) {
            return this.deserializeObj(v, ProtoMap[v.__type__]);
          } else {
            return v;
          }
        });
      } else if (value.__type__ && ProtoMap[value.__value__]) {
        deser[`_${fieldName}`] = this.deserializeObj(value, ProtoMap[value.__type__]);
      } else {
        deser[`_${fieldName}`] = toDeser[key];
      }
    });
    // @ts-ignore
    return deser;
  }

  protected deserializeArr(array: RawObject[], proto: any): EntityType[] {
    // @ts-ignore
    return array.map(obj => {
      return this.deserializeObj(obj, proto);
    });
  }

  private getCollectionName(): string {
    return `${this.getEntityClass().name.toLowerCase()}`;
  }
}
export interface RepositoryInterface<T> {
  find: (id: string) => Promise<T | null>;
  findByIds: (ids: Array<string>) => Promise<T[]>;
  findAll: () => Promise<T[] | null>;
  findBy: (conditions: Conditions, filters?: Filters) => Promise<T[] | null>;
  findOneBy: (conditions: Conditions, filters?: Filters) => Promise<T | null>;
}

export interface PersistenceInterface<T> {
  save: (obj: T) => Promise<string>;
}

function getObjForUpdate(obj: { [k: string]: any }): any {
  return getObj(obj, ['id']);
}
function getObjForSave(obj: { [k: string]: any }): any {
  return getObj(obj, ['id']);
}

function getObj(obj: { [k: string]: any }, ignoredFields: string[] = []): any {
  const values = Object.entries(obj).map(([key, value]) => {
    if (typeof value === 'function') {
      return null;
    }
    const fieldName = key.replace(/^_/, ''); // eg. field_id to id
    if (ignoredFields.find(f => f === fieldName)) {
      return null;
    }
    return [fieldName, value];
  });
  return Object.fromEntries(values.filter(e => e !== null));
}
