import { Exception } from '../Exceptions';

export class ConnectionError extends Exception {}

export class CollectionNotFound extends Exception {};