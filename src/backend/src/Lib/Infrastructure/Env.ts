import { bootConfig, isBooted } from './Config';

export function env(key: string): string {
  if (isBooted() === false) {
    bootConfig();
  }
  return process.env[key] || '';
}
