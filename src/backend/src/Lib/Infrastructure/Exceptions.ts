export class Exception {
  constructor(public msg: string = '') {}
}

export class RuntimeException extends Exception {}
