import * as winston from 'winston';

const enumerateErrorFormat = winston.format(info => {
  // @ts-ignore
  if (info.message instanceof Error || info.message instanceof SyntaxError) {
    // @ts-ignore
    info.message = { message: info.message.message, stack: info.message.stack, ...info.message };
  }

  if (info instanceof Error || info instanceof SyntaxError) {
    return { message: info.message, stack: info.stack, ...info };
  }

  return info;
});

const logger = winston.createLogger({
  levels: winston.config.npm.levels,
  format: winston.format.combine(enumerateErrorFormat(), winston.format.json()),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: 'logs/combined.log' }),
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  ],
});

export const Logger = logger;
