/**
 * Add class to the ProtoMap in order to deserialize objects from the DB
 */
export abstract class Nested {
  protected __type__: any;

  protected constructor() {
    this.__type__ = this.constructor.name;
  }
}
