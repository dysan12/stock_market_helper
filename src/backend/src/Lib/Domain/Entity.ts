interface StringableId {
  toString: () => string;
}
export abstract class Entity {
  protected _id: StringableId | null = null;

  public getId(): string | null {
    return this._id !== null ? this._id.toString() : null;
  }

  private setId(id: StringableId): void {
    this._id = id;
  }
}
