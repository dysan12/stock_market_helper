export interface TokenPayload {
  user: {
    id: string;
    login: string;
  };
}
